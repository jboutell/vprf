unsigned int hashs(int *arr, unsigned int len);
unsigned int hash(unsigned int *arr, unsigned int len);
unsigned int hash_seed();
unsigned int hash_many(int *arr, unsigned int len, unsigned int seed);
unsigned int hashc(unsigned char *str);
