#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "onnxgen.h"
#include "hash.h"

#define STR_MAX 256
#define FAN_MAX 16
#define NODE_MAX 64

#define FLOAT_SPLIT 1
#define FLOAT_JOIN 2

char* upperCase(char *sbuffer, char *str) {
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        sbuffer[i] = toupper(str[i]);
    }
    sbuffer[len] = '\0';
    return sbuffer;
}

void replace_chars(char *name) {
    for (int i = 0; i < strlen(name); i++) {
        if ((name[i] == '/') || (name[i] == ':')){
            name[i] = '%';
        }
    }
}

int file_exists(char *folder, char *buffer) {
    char fn[STR_MAX];
    sprintf(fn, "%s/%s", folder, buffer);
    FILE *f = fopen(fn, "rb");
    if (f != NULL) {
        fclose(f);
        return 1;
    }
    return 0;
}

int tensor_file_exists(struct onnx_tensor_t *t, char *folder) {
    char tmp_name[STR_MAX-11], buffer[STR_MAX];
    strcpy(tmp_name, t->name);
    replace_chars(tmp_name);
    sprintf(buffer, "%s/data/%s.bin", folder, tmp_name);
    FILE *f = fopen(buffer, "rb");
    if (f != NULL) {
        fclose(f);
        return 1;
    }
    return 0;
}

void prune_export_tensor(char *name, struct onnx_tensor_t *t, char *folder) {
    char buffer[STR_MAX], tmp_name[STR_MAX-11];
    strcpy(tmp_name, name);
    replace_chars(tmp_name);
    sprintf(buffer, "%s/data/%s.bin", folder, tmp_name);
    FILE *tf = NULL;
    tf = fopen(buffer, "wb");
    if (tf != NULL) {
        fwrite(&(t->ndata), sizeof(size_t), 1, tf);
        fwrite(t->datas, onnx_tensor_type_sizeof(t->type), t->ndata, tf);
        fclose(tf);
        #ifdef DEBUG
        printf("Exported tensor file %s of size %lu with checksum %u\n", tmp_name, t->ndata, hashCharArray((char *)t->datas, t->ndata));
        #endif
    } else {
        printf("Error: could not open file %s for writing\n", buffer);
    }
}

void writePnHeader(FILE *xml) {
    fprintf(xml, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    fprintf(xml, "<processnetwork xmlns=\"http://www.tik.ee.ethz.ch/~euretile/schema/PROCESSNETWORK\"\n");
    fprintf(xml, "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
    fprintf(xml, "xsi:schemaLocation=\"http://www.tik.ee.ethz.ch/~euretile/schema/PROCESSNETWORK\n");
    fprintf(xml, "http://www.tik.ee.ethz.ch/~euretile/schema/processnetwork.xsd\" name=\"onnx\">\n\n");
}

void writeProcess(FILE *xml, struct onnx_node_t *n, int ind, int *extInputs, int *extOutputs, char *folder) {
    char actorName[STR_MAX];
    sprintf(actorName, "%s_%i", n->proto->op_type, ind);

    fprintf(xml, "<process name=\"%s\" type=\"local\">\n", actorName);
    for (int i = 0; i < n->ninput; i++) {
        if (extInputs[i] == 0) {
            if(!tensor_file_exists(n->inputs[i], folder)) {
                fprintf(xml, "\t<port type=\"input\" name=\"in_%i\"/>\n", i);
            }
        }
    }
    for (int i = 0; i < n->noutput; i++) {
        if (extOutputs[i] == 0) {
            fprintf(xml, "\t<port type=\"output\" name=\"out_%i\"/>\n", i);
        }
    }
    fprintf(xml, "\t<source type=\"c\" location=\"%s.c\"/>\n", actorName);
}

void writeSwChannel(FILE *xml, char *name, int size) {
    fprintf(xml, "<sw_channel type=\"fifo\" initialtokens=\"0\" tokensize=\"%i\" size=\"1\" name=\"%s\">\n", size, name);
    fprintf(xml, "\t<port type=\"input\" name=\"0\"/>\n");
    fprintf(xml, "\t<port type=\"output\" name=\"1\"/>\n");
    fprintf(xml, "</sw_channel>\n");
    fprintf(xml, "\n");
}

int writeConnection(FILE *xml, char *source, char *sink, char *fifo, int src_p_ind, int snk_p_ind, int conn_index) {
    fprintf(xml, "<connection name=\"connection_%i\">\n", conn_index++);
    fprintf(xml, "\t<origin name=\"%s\">\n", source);
    fprintf(xml, "\t\t<port name=\"out_%u\"/>\n", src_p_ind);
    fprintf(xml, "\t</origin>\n");
    fprintf(xml, "\t<target name=\"%s\">\n", fifo);
    fprintf(xml, "\t\t<port name=\"0\"/>\n");
    fprintf(xml, "\t</target>\n");
    fprintf(xml, "</connection>\n");
    fprintf(xml, "<connection name=\"connection_%i\">\n", conn_index++);
    fprintf(xml, "\t<origin name=\"%s\">\n", fifo);
    fprintf(xml, "\t\t<port name=\"1\"/>\n");
    fprintf(xml, "\t</origin>\n");
    fprintf(xml, "\t<target name=\"%s\">\n", sink);
    fprintf(xml, "\t\t<port name=\"in_%u\"/>\n", snk_p_ind);
    fprintf(xml, "\t</target>\n");
    fprintf(xml, "</connection>\n");
    fprintf(xml, "\n");
    return conn_index;
}

void writeMapHeader (FILE *src) {
    fprintf(src, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    fprintf(src, "<mapping xmlns=\"http://www.tik.ee.ethz.ch/~euretile/schema/MAPPING\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n");
    fprintf(src, "  xsi:schemaLocation=\"http://www.tik.ee.ethz.ch/~euretile/schema/MAPPING\n");
    fprintf(src, "  http://www.tik.ee.ethz.ch/~euretile/schema/mapping.xsd\" name=\"mapping1\" processnetwork=\"APP1\">\n\n");
}

void writeMappingAndParameters (FILE *map, struct onnx_node_t* nd, int ind) {
    fprintf(map, "\t<binding name=\"%s_%i\">\n", nd->proto->op_type, ind);
    fprintf(map, "\t\t<process name=\"%s_%i\"/>\n", nd->proto->op_type, ind);
    fprintf(map, "\t\t<processor name=\"core_0\"/>\n");
    fprintf(map, "\t</binding>\n\n");
}

void writeProcessCHeader(struct onnx_node_t *nd, int ind, char *folder) {
    char name[STR_MAX-8], filename[STR_MAX];
    sprintf(name, "%s_%i", nd->proto->op_type, ind);
    sprintf(filename, "%s/src/%s.h", folder, name);
    FILE *h = fopen(filename, "w");
    fprintf(h, "#ifndef %s_H\n", name);
    fprintf(h, "#define %s_H\n", name);
    fprintf(h, "\n");
    fprintf(h, "#include \"common.h\"\n");
    fprintf(h, "#include \"dnn.h\"\n");
    fprintf(h, "#include \"onnx_helper.h\"\n");
    fprintf(h, "\n");
    fprintf(h, "extern struct onnx_context_t *onnx_ctx;\n");
    fprintf(h, "\n");
    fprintf(h, "typedef struct {\n");
    fprintf(h, "\tshared_t *shared;\n");
    fprintf(h, "\tstruct onnx_node_t * n;\n");
    fprintf(h, "\tint ind;\n");
    fprintf(h, "} %s_data_t;\n", name);
    fprintf(h, "\n");
    fprintf(h, "int %sInit(%s_data_t *data);\n", name, name);
    fprintf(h, "void *%sFire(void *p);\n", name);
    fprintf(h, "void %sFinish(%s_data_t *data);\n", name, name);
    fprintf(h, "#endif\n\n");
    fclose(h);
}

void printDynamicActorInputs(FILE *h, struct onnx_node_t *nd, int ind, int controlInput, int dynamicActorType, char *folder) {
    char name[256], nameup[256];
    sprintf(name, "%s_%i", nd->proto->op_type, ind);
    upperCase(nameup, name);
    for (int i = 0; i < nd->ninput; i++) {
        if (i == controlInput) {
            if(!tensor_file_exists(nd->inputs[i], folder)) {
                fprintf(h, "\tcl_float *fifo_in_%i = (cl_float *) fifoReadStart(data->shared->inputs[%s_IN_%i]);\n", i, nameup, i);
                fprintf(h, "\t%s control_value;\n", onnx_tensor_type_tostring(nd->inputs[i]->type));
                fprintf(h, "\tmemcpy(data->n->inputs[%i]->datas, fifo_in_%i, %lu);\n", i, i, nd->inputs[i]->ndata*onnx_tensor_type_sizeof(nd->inputs[i]->type));
                fprintf(h, "\tcontrol_value = fifo_in_%i[0];\n", i);
            }
        }
    }
    for (int i = 0; i < nd->ninput; i++) {
        if (i != controlInput) {
            if(!tensor_file_exists(nd->inputs[i], folder)) {
                fprintf(h, "\tcl_float *fifo_in_%i;\n", i);
                if ((dynamicActorType == FLOAT_JOIN)) {
                    fprintf(h, "\tif(control_value > 0.5) {\n");
                }
                fprintf(h, "\tfifo_in_%i = (cl_float *) fifoReadStart(data->shared->inputs[%s_IN_%i]);\n", i, nameup, i);
                fprintf(h, "\tmemcpy(data->n->inputs[%i]->datas, fifo_in_%i, %lu);\n", i, i, nd->inputs[i]->ndata*onnx_tensor_type_sizeof(nd->inputs[i]->type));
                   if ((dynamicActorType == FLOAT_JOIN)) {
                    fprintf(h, "\t}\n");
                }
            }
        }
    }
}

void writeProcessCSource(struct onnx_node_t *nd, int ind, int *extInputs, int *extOutputs, char *folder, int fanOut, int controlInput, int dynamicActorType) {
    char name[STR_MAX-8], filename[STR_MAX], nameup[STR_MAX];
    int pi, pi2;
    sprintf(name, "%s_%i", nd->proto->op_type, ind);
    upperCase(nameup, name);
    sprintf(filename, "%s/src/%s.c", folder, name);
    FILE *h = fopen(filename, "w");
    fprintf(h, "#include \"dnn.h\"\n");
    fprintf(h, "#include \"ports.h\"\n");
    fprintf(h, "#include \"%s.h\"\n", name);
    fprintf(h, "#ifdef DBGPRINT\n");
    fprintf(h, "\t#include <stdio.h>\n");
    fprintf(h, "#endif\n");
    fprintf(h, "#include <string.h>\n");
    fprintf(h, "\n");
    fprintf(h, "int %sInit(%s_data_t *data) {\n", name, name);
    fprintf(h, "\tchar fileName[256];\n");
    fprintf(h, "\tdata->n = NULL;\n");
    fprintf(h, "\tdata->n = (struct onnx_node_t *) malloc(sizeof(struct onnx_node_t));\n");
    fprintf(h, "\tonnx_node_init(data->n, %i, %i, %i, onnx_ctx->model->graph->node[%i]);\n", nd->opset, nd->ninput, nd->noutput, ind);
    for (int i = 0; i < nd->ninput; i++) {
        fprintf(h, "\tonnx_tensor_init(data->n->inputs[%i], \"%s\", %i, %i);\n", i, nd->inputs[i]->name, nd->inputs[i]->type, nd->inputs[i]->ndim);
        for (int j = 0; j < nd->inputs[i]->ndim; j++) {
            fprintf(h, "\t data->n->inputs[%i]->dims[%i] = %i;\n", i, j, nd->inputs[i]->dims[j]);
            fprintf(h, "\t data->n->inputs[%i]->strides[%i] = %i;\n", i, j, nd->inputs[i]->strides[j]);
        }
        fprintf(h, "\tonnx_tensor_allocate(data->n->inputs[%i]);\n", i);
    }
    for (int i = 0; i < nd->noutput; i++) {
        fprintf(h, "\tonnx_tensor_init(data->n->outputs[%i], \"%s\", %i, %i);\n", i, nd->outputs[i]->name, nd->outputs[i]->type, nd->outputs[i]->ndim);
        for (int j = 0; j < nd->outputs[i]->ndim; j++) {
            fprintf(h, "\t data->n->outputs[%i]->dims[%i] = %i;\n", i, j, nd->outputs[i]->dims[j]);
            fprintf(h, "\t data->n->outputs[%i]->strides[%i] = %i;\n", i, j, nd->outputs[i]->strides[j]);
        }
        fprintf(h, "\tonnx_tensor_allocate(data->n->outputs[%i]);\n", i);
        if (extOutputs[i] == 1) {
            fprintf(h, "\tdata->ind = sink_init(data->n->outputs[%i]);\n", i);
        }
    }
    fprintf(h, "\tonnx_node_resolve(data->n);\n");
    for (int i = 0; i < nd->ninput; i++) {
        if (extInputs[i] == 1) {
            fprintf(h, "\tdata->ind = source_init(data->n->inputs[%i]);\n", i);
        } else {
            char tmp_name[STR_MAX-10], buffer[STR_MAX];
            strcpy(tmp_name, nd->inputs[i]->name);
            replace_chars(tmp_name);
            sprintf(buffer, "data/%s.bin", tmp_name);
            if (file_exists(folder, buffer)) {
                fprintf(h, "\tonnx_tensor_load(\"%s\", data->n->inputs[%i]);\n", buffer, i);
            }
        }
    }
    fprintf(h, "\treturn 0;\n");
    fprintf(h, "}\n");
    fprintf(h, "\n");
    fprintf(h, "void %sFinish(%s_data_t *data) {\n", name, name);
    for (int i = 0; i < nd->ninput; i++) {
        if (extInputs[i] == 1) {
            fprintf(h, "\tsource_close(data->ind, data->n->inputs[%i]);\n", i);
        }
    }
    for (int i = 0; i < nd->noutput; i++) {
        if (extOutputs[i] == 1) {
            fprintf(h, "\tsink_close(data->ind, data->n->outputs[%i]);\n", i);
        }
    }
    fprintf(h, "\tonnx_node_free(data->n);\n");
    fprintf(h, "}\n");
    fprintf(h, "\n");
    fprintf(h, "void* %sFire (void *p) {\n", name);
    fprintf(h, "\t%s_data_t *data = (%s_data_t *) p;\n", name, name);
    pi = 0;
    if (dynamicActorType == 0) {
        for (int i = 0; i < nd->ninput; i++) {
            if (extInputs[i] == 1) {
                fprintf(h, "\tsource_read(data->ind, data->n->inputs[%i]);\n", i);
            } else {
                if(!tensor_file_exists(nd->inputs[i], folder)) {
                    fprintf(h, "\tcl_float *fifo_in_%i = (cl_float *) fifoReadStart(data->shared->inputs[%s_IN_%i]);\n", pi, nameup, pi);
                    fprintf(h, "\tmemcpy(data->n->inputs[%i]->datas, fifo_in_%i, %lu);\n", i, pi, nd->inputs[i]->ndata*onnx_tensor_type_sizeof(nd->inputs[i]->type));
                    pi++;
                }
            }
        }
    } else {
        printDynamicActorInputs(h, nd, ind, controlInput, dynamicActorType, folder);
    }
    pi = 0;
    for (int i = 0; i < nd->noutput; i++) {
        if (extOutputs[i] == 0) {
            fprintf(h, "\tcl_float *fifo_out_%i;\n", pi);
        }
        pi++;
    }
       if (dynamicActorType == FLOAT_SPLIT) {
        fprintf(h, "\tif(control_value > 0.5) {\n");
    }
    pi = 0;
    for (int i = 0; i < nd->noutput; i++) {
        if (extOutputs[i] == 0) {
            fprintf(h, "\tfifo_out_%i = (cl_float *) fifoWriteStart(data->shared->outputs[%i]);\n", pi, pi);
        }
        pi++;
    }
       if (dynamicActorType == FLOAT_JOIN) {
        fprintf(h, "\tif(control_value > 0.5) {\n");
    }
    fprintf(h, "\tif(data->n->reshape(data->n)){\n");
    fprintf(h, "\t\t#ifdef DEBUG\n");
    fprintf(h, "\t\tprintf(\"actor %s fires\\n\");\n", name);
    fprintf(h, "\t\t#endif\n");
    fprintf(h, "\t\tdata->n->operator(data->n);\n");
    fprintf(h, "\t}\n");
    if (dynamicActorType > 0) {
        fprintf(h, "\t}\n");
    }
    pi = 0;
    for (int i = 0; i < nd->ninput; i++) {
        if (extInputs[i] == 0) {
            if(!tensor_file_exists(nd->inputs[i], folder)) {
                if ((dynamicActorType == FLOAT_JOIN) && (i != controlInput)) {
                    fprintf(h, "\tif(control_value > 0.5) {\n");
                }
                fprintf(h, "\tfifoReadEnd(data->shared->inputs[%s_IN_%i]);\n", nameup, pi);
                   if ((dynamicActorType == FLOAT_JOIN) && (i != controlInput)) {
                    fprintf(h, "\t}\n");
                }
                pi++;
            }
        }
    }
    pi = 0;
    pi2 = 0;
    for (int i = 0; i < nd->noutput; i++) {
        if ((dynamicActorType == FLOAT_SPLIT) || (dynamicActorType == FLOAT_JOIN)){
            fprintf(h, "\tif(control_value > 0.5) {\n");
        }
        long unsigned int tokenSize = nd->outputs[i]->ndata*onnx_tensor_type_sizeof(nd->outputs[i]->type);
        if (extOutputs[i] == 1) {
            fprintf(h, "\tsink_write(data->ind, data->n->outputs[%i]);\n", i);
        } else {
            fprintf(h, "\tmemcpy(fifo_out_%i, data->n->outputs[%i]->datas, %lu);\n", pi, i, tokenSize);
            for(int j = 1; j < fanOut; j++) {
                fprintf(h, "\tcl_float *fifo_out_%i = (cl_float *) fifoWriteStart(data->shared->outputs[%i]);\n", pi+j, pi+j);
                fprintf(h, "\tmemcpy(fifo_out_%i, fifo_out_0, %lu);\n", pi+j, tokenSize);
                fprintf(h, "\tfifoWriteEnd(data->shared->outputs[%i]);\n", pi+j);
            }
            fprintf(h, "\tfifoWriteEnd(data->shared->outputs[%i]);\n", pi);
            pi++;
        }
        if (dynamicActorType == FLOAT_JOIN) {
            fprintf(h, "\t} else {\n");
            fprintf(h, "\t\tmemset(fifo_out_%i, 0, %lu);\n", pi2, tokenSize);
            for(int j = 1; j < fanOut; j++) {
                fprintf(h, "\t\tcl_float *fifo_out_%i = (cl_float *) fifoWriteStart(data->shared->outputs[%i]);\n", pi2+j, pi2+j);
                fprintf(h, "\t\tmemset(fifo_out_%i, 0, %lu);\n", pi2+j, tokenSize);
                fprintf(h, "\t\tfifoWriteEnd(data->shared->outputs[%i]);\n", pi2+j);
            }
            fprintf(h, "\t\tfifoWriteEnd(data->shared->outputs[%i]);\n", pi2);
            fprintf(h, "\t}\n");
            pi2++;
        } else if (dynamicActorType == FLOAT_SPLIT) {
            fprintf(h, "\t} else {\n");
            fprintf(h, "\t}\n");
        }
    }
    fprintf(h, "\treturn p;\n");
    fprintf(h, "}\n\n");
    fclose(h);
}

void prune_export_parameters(struct onnx_context_t * ctx, char *folder) {
    for(int i = 0; i < ctx->g->nlen; i++) {
        for(int j = 0; j < ctx->g->nodes[i].ninput; j++){
            int found = 0;
            for(int k = 0; k < ctx->model->graph->n_initializer; k++) {
                if(strcmp(ctx->model->graph->initializer[k]->name, ctx->g->nodes[i].inputs[j]->name) == 0) {
                    found = 1;
                }
            }
            if (found) {
                if(onnx_tensor_search(ctx, ctx->g->nodes[i].inputs[j]->name)) {
                    prune_export_tensor(ctx->g->nodes[i].inputs[j]->name, ctx->g->nodes[i].inputs[j], folder);
                }
            }
        }
    }
}

int find_sink(struct onnx_graph_t * g, int j, int i, int *sinkNode, int *sinkPort) {
    int count = 0;
    char *tensor_name = g->nodes[j].outputs[i]->name;
    for (int k = 0; k < g->nlen; k++) {
        struct onnx_node_t *n = &g->nodes[k];
        for (int l = 0; l < n->ninput; l++) {
            if (!strcmp(tensor_name, n->inputs[l]->name)) {
                sinkNode[count] = k;
                sinkPort[count] = l;
                count ++;
            }
        }
    }
    return count;
}

int findActorAndPort(struct onnx_graph_t *g, char *dynamicActorName, char *dynamicActorPort, int *portIndex) {
    for (int j = 0; j < g->nlen; j++) {
        struct onnx_node_t *n = &g->nodes[j];
        char currentActorName[STR_MAX];
        sprintf(currentActorName, "%s_%i", n->proto->op_type, j);
        if (!strcmp(currentActorName, dynamicActorName)) {
            for (int l = 0; l < n->ninput; l++) {
                if (!strcmp(dynamicActorPort, n->inputs[l]->name)) {
                    portIndex[0] = l;
                    return j;
                }
            }
        }
    }
    return -1;
}

int readDynamicActorList(struct onnx_graph_t *g, const char *fn, int *dynamicActorList, int *dynamicActorCtlPort, int *dynamicActorType) {
    int count = 0;
    int done = 0;
    char actorName[STR_MAX], actorPort[STR_MAX], actorType[STR_MAX];
    FILE *f = fopen(fn, "r");
    if (f == NULL) {
        return 0;
    }
    printf("Processing %s\n", fn);
    while(!done) {
        int actorIndex, portIndex;
        int ret = fscanf(f, "%s %s %s\n", actorName, actorPort, actorType);
        if (ret == 3) {
            actorIndex = findActorAndPort(g, actorName, actorPort, &portIndex);
            if (actorIndex >= 0) {
                dynamicActorList[count] = actorIndex;
                dynamicActorCtlPort[count] = portIndex;
                if (!strcmp(actorType, "float_join")) {
                    dynamicActorType[count] = FLOAT_JOIN;
                } else if (!strcmp(actorType, "float_split")) {
                    dynamicActorType[count] = FLOAT_SPLIT;
                } else {
                    printf("Unknown dynamic actor type %s on line %i in %s\n", actorType, count, fn);
                    dynamicActorType[count] = 0;
                    done = 1;
                }
            } else {
                printf("Unknown dynamic actor name %s or port %s\n", actorName, actorPort);
                done = 1;
            }
            count++;
        } else {
            done = 1;
        }
    }
    fclose(f);
    return count;
}

void prune_generate_graph(struct onnx_graph_t *g, char *folder) {
    int *fanOut;
    char buffer[STR_MAX];
    sprintf(buffer, "%s/xml/pn.xml", folder);
    FILE *pn = fopen(buffer, "w");
    sprintf(buffer, "%s/xml/mapping.xml", folder);
    FILE *map = fopen(buffer, "w");
    writePnHeader(pn);
    writeMapHeader(map);
    int conn_index = 0;
    int sinkPort[FAN_MAX];
    int sinkNode[FAN_MAX];

    int dynamicActorList[NODE_MAX];
    int dynamicActorCtlPort[NODE_MAX];
    int dynamicActorType[NODE_MAX];
    int dynamicActorCount = 0;

    fanOut = (int*) malloc(sizeof(int)*g->nlen);

    for (int j = 0; j < g->nlen; j++) {
        struct onnx_node_t *n = &g->nodes[j];
        for (int i = 0; i < n->noutput; i++) {
            int sink_count = find_sink(g, j, i, sinkNode, sinkPort);
            fanOut[j] = sink_count;
            for (int k = 0; k < sink_count; k++) {
                int node = sinkNode[k];
                int port = sinkPort[k];
                char *src = n->outputs[i]->name;
                struct onnx_node_t *snk = &g->nodes[node];
                struct onnx_tensor_t *tensor = snk->inputs[port];
                if (tensor != NULL) {
                    char fifoStr[STR_MAX], fifoName[STR_MAX];
                    sprintf(fifoStr, "%s_%i_%s_%s_%i", n->proto->op_type, j, tensor->name, snk->proto->op_type, node);
                    sprintf(fifoName, "fifo_%u", hashc(fifoStr));
                    int tokenSize = n->outputs[i]->ndata*onnx_tensor_type_sizeof(n->outputs[i]->type);
                    writeSwChannel(pn, fifoName, tokenSize);
                    if (tokenSize == 0) {
                        if (n->outputs[i]->ndata == 0) {
                            printf("warning: FIFO %s ndata is zero\n", fifoStr);
                        }
                        if (onnx_tensor_type_sizeof(n->outputs[i]->type) == 0) {
                            printf("warning: FIFO %s data type (%i) has size zero\n", fifoStr, n->outputs[i]->type);
                        }
                    }
                } else {
                    printf("error: sink port is NULL\n");
                }
            }
        }
    }

    dynamicActorCount = readDynamicActorList(g, "dynamic_actors.txt", dynamicActorList, dynamicActorCtlPort, dynamicActorType);

    for (int j = 0; j < g->nlen; j++) {
        struct onnx_node_t *n1 = &g->nodes[j];
        char actorName[STR_MAX];
        int extInputs[FAN_MAX] = {0};
        int extOutputs[FAN_MAX] = {0};
        sprintf(actorName, "%s", n1->proto->op_type);

        for (int i = 0; i < n1->ninput; i++) {
            int src_found = 0;

            for (int k = 0; k < g->nlen; k++) {
                struct onnx_node_t *n2 = &g->nodes[k];
                for (int l = 0; l < n2->noutput; l++) {
                    if (!strcmp(n2->outputs[l]->name, n1->inputs[i]->name)) {
                        char fifoStr[STR_MAX], fifoName[STR_MAX];
                        sprintf(fifoStr, "%s_%i_%s_%s_%i", n2->proto->op_type, k, n2->outputs[l]->name, n1->proto->op_type, j);
                        sprintf(fifoName, "fifo_%u", hashc(fifoStr));
                        char node1Name[STR_MAX];
                        sprintf(node1Name, "%s_%i", n2->proto->op_type, k);
                        char node2Name[STR_MAX];
                        sprintf(node2Name, "%s_%i", n1->proto->op_type, j);
                        conn_index = writeConnection(pn, node1Name, node2Name, fifoName, l, i, conn_index);
                        src_found = 1;
                    }
                }
            }
            if (src_found == 0) {
                if (!tensor_file_exists(n1->inputs[i], folder)) {
                    extInputs[i] = 1;
                }
            }
        }

        for (int i = 0; i < n1->noutput; i++) {
            int snk_found = 0;
            for (int k = 0; k < g->nlen; k++) {
                struct onnx_node_t *n2 = &g->nodes[k];
                for (int l = 0; l < n2->ninput; l++) {
                    if (!strcmp(n1->outputs[i]->name, n2->inputs[l]->name)) {
                        snk_found = 1;
                    }
                }
            }
            if (snk_found == 0) {
                printf("Graph output at %s_%i:%s\n", n1->proto->op_type, j, n1->outputs[i]->name);
                extOutputs[i] = 1;
            }
        }

        int source_written = 0;
        writeProcessCHeader(n1, j, folder);
        for (int i = 0; i < dynamicActorCount; i++) {
            if (j == dynamicActorList[i]) {
                source_written = 1;
                printf("%s_%i with control port %s is dynamic actor of type %i\n", n1->proto->op_type, j, n1->inputs[dynamicActorCtlPort[i]]->name, dynamicActorType[i]);
                   writeProcessCSource(n1, j, extInputs, extOutputs, folder, fanOut[j], dynamicActorCtlPort[i], dynamicActorType[i]);
                   break;
            }
        }
        if (!source_written) {
            writeProcessCSource(n1, j, extInputs, extOutputs, folder, fanOut[j], -1, 0);
        }
        writeProcess(pn, n1, j, extInputs, extOutputs, folder);

        writeMappingAndParameters(map, n1, j);
        fprintf(pn, "</process>\n\n");
    }

    free(fanOut);
    fprintf(pn, "</processnetwork>\n\n");
    fprintf(map, "</mapping>\n\n");
    fclose(pn);
    fclose(map);
}
