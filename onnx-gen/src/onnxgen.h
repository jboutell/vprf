#include <onnx.h>

void prune_generate_graph(struct onnx_graph_t * g, char *folder);
void prune_export_tensor(char *name, struct onnx_tensor_t *t, char *folder);
unsigned int hashCharArray(char *arr, unsigned int len);
unsigned int onnx_tensor_checksum(struct onnx_tensor_t *t);
void prune_export_parameters(struct onnx_context_t * ctx, char *folder);

