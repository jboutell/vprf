#include <onnx.h>
#include <stdio.h>
#include "onnxgen.h"

int main(int argc, char** argv) {

    if(argc != 3) {
        printf("usage: onnxgen <onnx-file-name> <dest-folder>\n");
        return 0;
    }

    struct onnx_context_t * ctx = onnx_context_alloc_from_file(argv[1], NULL, 0);

    if(!ctx) {
        printf("onnx_context_alloc_from_file failed.\n");
	return -1;
    } else {
        printf("onnx_context_allocation succeeded.\n");
    }

    printf("Starting to generate PRUNE files.\n");
    prune_export_parameters(ctx, argv[2]);
    prune_generate_graph(ctx->g, argv[2]);
    printf("PRUNE generation completed.\n");

    onnx_context_free(ctx);
    return 0;
}
