unsigned int hashs(int *arr, unsigned int len) {
    unsigned int hash = 5381;
    int i;

    for(i = 0; i < len; i++)
    {
        int sval = arr[i] + 1048576;
        unsigned int uval = sval;
        hash = ((hash << 5) + hash) + uval;
    }

    return hash;
}

unsigned int hash(unsigned int *arr, unsigned int len) {
    unsigned int hash = 5381;
    int i;

    for(i = 0; i < len; i++)
        hash = ((hash << 5) + hash) + arr[i];

    return hash;
}

unsigned int hash_seed() {
    return 5381;
}

unsigned int hash_many(int *arr, unsigned int len, unsigned int seed) {
    unsigned int hash = seed;
    int i;

    for(i = 0; i < len; i++)
    {
        unsigned int uval;
        int sval = arr[i] + 1048576;
        uval = sval;
        hash = ((hash << 5) + hash) + uval;
    }

    return hash;
}

unsigned int hashc(unsigned char *str) {
    unsigned int hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c;

    return hash;
}
