#include "dnn.h"

__kernel void mulL3 (
	__global float* restrict B,
	__global int* restrict select_in,
	__global float* restrict c_mul,
	__constant float* restrict wgt) {

	const int Mdim = MAGIC;
	const int Kdim = 1;
	const int Ndim = PATCH3SQ * FM_COUNT;

    int r = get_global_id(0);
    int i = get_global_id(1);

	if (select_in[r] < REPEAT) {
		float acc = 0.0;
		for (int z = 0; z < Ndim; z++) {
			acc += wgt[Ndim*i + z] * B[TOKEN3SIZE*r + z];
		}
		c_mul[TOKEN4SIZE*r + i] = acc;
	}
}

