#include "dnn.h"
#include "select.h"
#include "ports.h"
#include <stdlib.h>
#include <string.h>

void selectInit(select_data_t *data) {
	data->iteration = 0;
}

void *selectFire(void *p) {
	select_data_t *data = (select_data_t *) p;
	#ifdef DBGPRINT
	printf("actor select fires\n");
	#endif
	// read image and add PAD_NUM padding on each side of image
	int writeOffset = 0;
	int readOffset = 0;

	int *select = (int *) fifoReadStart(data->shared->inputs[SELECTL1_IN2]);

	int enaTable[REPEAT];
	int enaCount = 0;
	for (int i = 0; i < REPEAT; i++) {
		if(select[i] < REPEAT) {
			enaTable[enaCount] = select[i];
			enaCount ++;
		}
	}
	fifoReadEnd(data->shared->inputs[SELECTL1_IN2]);

	float *rbufin1 = (float *) fifoReadStart(data->shared->inputs[SELECTL1_IN1]);
	float *wbufout1 = (float *) fifoWriteStartN(data->shared->outputs[SELECTL1_OUT1], enaCount);
	//float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[SELECTL1_OUT1]);
	for (int r = 0; r < enaCount; r++) {
		int enable = enaTable[r];
		readOffset = enaTable[r]*PATCH1SQ*CHANNELS;
		writeOffset = r*TOKEN1SIZE;
		if (enable < REPEAT) {
			for (int c = 0; c < CHANNELS; c++) {
				cl_float *fifop = &wbufout1[writeOffset + c*PATCH1SQPAD];
				memset(fifop, 0, 2*(PATCH1 + 2*PAD_NUM)*sizeof(cl_float));
				for (int i = PAD_NUM; i < (PATCH1 + PAD_NUM); i++) {
					memset(&fifop[i * (PATCH1+2*PAD_NUM)], 0, PAD_NUM*sizeof(cl_float));
					memcpy(&fifop[i * (PATCH1+2*PAD_NUM) + PAD_NUM], &rbufin1[readOffset], sizeof(cl_float)*PATCH1);
					memset(&fifop[i * (PATCH1+2*PAD_NUM) + PAD_NUM + PATCH1], 0, PAD_NUM*sizeof(cl_float));
					readOffset += PATCH1;
				}
				memset(&fifop[(PATCH1 + PAD_NUM) * (PATCH1+2*PAD_NUM)], 0, 2*(PATCH1 + 2*PAD_NUM)*sizeof(cl_float));
			}
		}
	}
	fifoReadEnd(data->shared->inputs[SELECTL1_IN1]);
	fifoWriteEnd(data->shared->outputs[SELECTL1_OUT1]);

	return p;
}

void selectFinish(select_data_t *data) {
}

