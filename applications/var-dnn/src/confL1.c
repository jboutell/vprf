#include "dnn.h"
#include "confL1.h"
#include <stdlib.h>
#include <string.h>
#include "ports.h"

void confL1Init(confL1_data_t *data) {
	char confL1FileName[256];
	data->file = NULL;
	sprintf(confL1FileName, "%s.bin", data->fn);
	data->iteration = 0;
	data->file = fopen(confL1FileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", confL1FileName);
		return;
	}
}

void *confL1Fire(void *p) {
	confL1_data_t *data = (confL1_data_t *) p;
	#ifdef DBGPRINT
	printf("actor confL1 fires\n");
	#endif

	int enOffset = 0;
	int enCount = 0;
	char enaTable[REPEAT];
	fread(enaTable, sizeof(char), REPEAT, data->file);
	for (int r = 0; r < REPEAT; r++) {
		enCount += (int) enaTable[r];
	}
	int *wbufout0 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_0]);
	int *wbufout1 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_1]);
	int *wbufout2 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_2]);
	int *wbufout3 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_3]);
	int *wbufout4 = (int *) fifoWriteStart(data->shared->outputs[CONFL1_OUT1_4]);

	for (int r = 0; r < REPEAT; r++) {
		int enable, enVar;
		enVar = enaTable[r];
		if (enVar == 0) {
			enable = REPEAT;
		} else {
			enable = r;
		}
		if (enable < REPEAT) {
			wbufout0[enOffset] = enable;
			wbufout1[enOffset] = enable;
			wbufout2[enOffset] = enable;
			wbufout3[enOffset] = enable;
			wbufout4[enOffset] = enable;
			enOffset++;
		}
	}
	for (; enOffset < REPEAT; enOffset++) {
		wbufout0[enOffset] = REPEAT;
		wbufout1[enOffset] = REPEAT;
		wbufout2[enOffset] = REPEAT;
		wbufout3[enOffset] = REPEAT;
		wbufout4[enOffset] = REPEAT;
	}	

	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_0]);
	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_1]);
	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_2]);
	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_3]);
	fifoWriteEnd(data->shared->outputs[CONFL1_OUT1_4]);

	return p;
}

void confL1Finish(confL1_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

