#include "dnn.h"
#include "source.h"
#include <stdlib.h>
#include <string.h>

extern volatile int globalRepetitions;

void sourceInit(source_data_t *data) {
	char sourceFileName[256];
	data->file = NULL;
	sprintf(sourceFileName, "%s.bin", data->fn);
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
	globalRepetitions = getFileSize(data->file) / (CHANNELS * PATCH1SQ * sizeof(float));
	data->iteration = 0;
	//printf("Running for %i images\n", globalRepetitions);
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		//printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[0]);
		#ifdef DBGPRINT
		printf("actor source fires\n");
		#endif
		// read image and add PAD_NUM padding on each side of image
		int retval = fread(wbufout1, sizeof(cl_float), REPEAT*PATCH1SQ*CHANNELS, data->file);
		if (retval != REPEAT*PATCH1SQ*CHANNELS) {
			printf("Source %s depleted\n", data->fn);
		}
		fifoWriteEnd(data->shared->outputs[0]);
		data->iteration += REPEAT;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

