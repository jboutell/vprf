#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	shared_t *shared;
	char const *fn;
	int iteration;
} confL1_data_t;

void confL1Init(confL1_data_t *data);
void *confL1Fire(void *p);
void confL1Finish(confL1_data_t *data);


