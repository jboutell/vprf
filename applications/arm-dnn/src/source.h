#include <stdio.h>
#include "common.h"

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
#else
  #include "IntelCL/CLTensor.h"
  #include "IntelCL/CLFunctions.h"
#endif

using namespace arm_compute;

typedef struct {
	FILE *file;
	shared_t *shared;
	int iteration;
	char const *fn;
} source_data_t;

void sourceInit(source_data_t *data);
void *sourceFire(void *p);
void sourceFinish(source_data_t *data);


