#include "dnn.h"
#include "source.h"
#include <string.h>

#include "CLHelper.h"

extern volatile int globalRepetitions;

void sourceInit(source_data_t *data) {
	data->file = NULL;
	char sourceFileName[256];
	sprintf(sourceFileName, "data/c384.bin");
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
	globalRepetitions = getFileSize(data->file) / (CHANNELS * PATCH1SQ * sizeof(float));
	data->iteration = 0;
	printf("Running for %i images\n", globalRepetitions);
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		CLTensor *out_l1mid = ((CLTensor**) fifoWriteStart(data->shared->outputs[0]))[0];
		#ifdef DBGPRINT
		printf("actor source fires\n");
		#endif
		StreamTensor(out_l1mid, data->file, 96, 96, 3, true);
		fifoWriteEnd(data->shared->outputs[0]);
		data->iteration ++;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

