#include "mtx_mulf_l3.h"
#include <string.h>

#include "CLHelper.h"

int getFileSize (FILE *fp);

int mtx_mulf_l3Init(mtx_mulf_l3_data_t *data) {
	char fileName[256];
	sprintf(fileName, "data/ip3.bin");
	CLTensor* out_mxpool2;
	CLTensor* out_dense1;

	for(int i = 0; i < FIFO_SIZE; i++) {
		data->weights3[i] = CreateTensor2D(32*24*24,100);
		out_mxpool2 = ((CLTensor**) fifoTokenGet(data->shared->inputs[0], i))[0];
		out_dense1 = ((CLTensor**) fifoTokenGet(data->shared->outputs[0], i))[0];
		#ifdef ARM_API
		data->dense1[i].configure(out_mxpool2, data->weights3[i], NULL, out_dense1);
		#else
		data->dense1[i].configure(out_mxpool2->data, data->weights3[i]->data, NULL, out_dense1->data, MAGIC, PATCH3SQ*FM_COUNT, 1);
		#endif
		LoadTensor2D(data->weights3[i], fileName, 32*24*24, 100, true);
	}
	data->iteration = 0;
	return 0;
}

void mtx_mulf_l3Finish(mtx_mulf_l3_data_t *data) {
	for(int i = 0; i < FIFO_SIZE; i++) {
		#ifdef ARM_API
		data->weights3[i]->allocator()->free();
		#else
		free (data->weights3[i]->data);
		#endif
		delete data->weights3[i];
	}
}

void *mtx_mulf_l3Fire(void *p) {
	mtx_mulf_l3_data_t *data = (mtx_mulf_l3_data_t *) p;

	fifoReadStart(data->shared->inputs[0]);
	fifoWriteStart(data->shared->outputs[0]);

	data->dense1[data->iteration%FIFO_SIZE].run();
	data->iteration++;

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
