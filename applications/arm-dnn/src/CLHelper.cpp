/*************************************************************************\
* Minimal ARMCL emulation library for general purpose Linux architectures *
* Authors: Jani Boutellier, jani.boutellier@uwasa.fi                      *
*          Mir Khan, mir.khan@tuni.fi                                     *
\*************************************************************************/

#include <stdio.h>
#include <string.h>
#include "CLHelper.h"

CLTensor* CreateTensor1D(unsigned int x){
#ifdef ARM_API
	CLTensor *X=new CLTensor;
	const TensorShape x_shape(x);
	X->allocator()->init(TensorInfo(x_shape, 1, DataType::F32));
	return X;
#else
	CLTensor *X = new CLTensor;
	X->allocate(x);
	return X;
#endif
}

CLTensor* CreateTensor2D(unsigned int x, unsigned int y){
#ifdef ARM_API
	CLTensor *X=new CLTensor;
	const TensorShape x_shape(x,y);
	X->allocator()->init(TensorInfo(x_shape, 1, DataType::F32));
	return X;
#else
	CLTensor *X = new CLTensor;
	X->allocate(x*y);
	return X;
#endif
}

CLTensor* CreateTensor3D(unsigned int x, unsigned int y, unsigned int z){
#ifdef ARM_API
	CLTensor *X=new CLTensor;
	const TensorShape x_shape(x,y,z);
	X->allocator()->init(TensorInfo(x_shape, 1, DataType::F32));
	return X;
#else
	CLTensor *X = new CLTensor;
	X->allocate(x*y*z);
	return X;
#endif
}

CLTensor* CreateTensor4D(unsigned int x, unsigned int y, unsigned int z, unsigned int w){
#ifdef ARM_API
	CLTensor *X=new CLTensor;
	const TensorShape x_shape(x,y,z,w);
	X->allocator()->init(TensorInfo(x_shape, 1, DataType::F32));
	return X;
#else
	CLTensor *X = new CLTensor;
	X->allocate(x*y*z*w);
	return X;
#endif
}

void FillTensor(CLTensor* X) {
	#ifdef ARM_API
	X->allocator()->allocate();
	#endif
}

int LoadTensor4D(CLTensor* X, char *fn, int width, int height, int batch, int fm, bool map) {
	FILE *file = fopen(fn, "rb");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return 0;
	}
	int len = getFileSize(file) / sizeof(float);
	#ifdef ARM_API
	X->allocator()->allocate();
	if (map) {
		X->map();
	}
	if (X->buffer() == NULL) {
		printf("Buffer is NULL in LoadTensor\n");
		return 0;
	}
	int cnt = 0;
	for(unsigned int f = 0; f < fm; ++f) {
		for(unsigned int z = 0; z < batch; ++z) {
			for(unsigned int y = 0; y < height; ++y) {
				cnt += fread(reinterpret_cast<float*>(X->buffer() + X->info()->offset_element_in_bytes(Coordinates(0,y,z,f))),
					sizeof(float), width, file);
			}
		}
	}
	if (map) {
		X->unmap();
	}
	#else
	int cnt = fread(X->data, sizeof(float), len, file);
	#endif
	if (cnt != len) {
		printf("error reading %s: %i of %i samples acquired\n", fn, cnt, len);
	}
	fclose(file);
	return len;
}

int LoadTensor3D(CLTensor* X, char *fn, int width, int height, int batch, bool map) {
	FILE *file = fopen(fn, "rb");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return 0;
	}
	int len = getFileSize(file) / sizeof(float);
	#ifdef ARM_API
	X->allocator()->allocate();
	if (map) {
		X->map();
	}
	if (X->buffer() == NULL) {
		printf("Buffer is NULL in LoadTensor\n");
		return 0;
	}
	int cnt = 0;
	for( unsigned int z = 0; z < batch; ++z) {
		for( unsigned int y = 0; y < height; ++y) {
			cnt += fread(reinterpret_cast<float*>(X->buffer() + X->info()->offset_element_in_bytes(Coordinates(0,y,z))),
				sizeof(float), width, file);
		}
	}
	if (map) {
		X->unmap();
	}
	#else
	int cnt = fread(X->data, sizeof(float), len, file);
	#endif
	if (cnt != len) {
		printf("error reading %s: %i of %i samples acquired\n", fn, cnt, len);
	}
	fclose(file);
	return len;
}

int LoadTensor2D(CLTensor* X, char *fn, int width, int height, bool map) {
	FILE *file = fopen(fn, "rb");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return 0;
	}
	int len = getFileSize(file) / sizeof(float);
	#ifdef ARM_API
	X->allocator()->allocate();
	if (map) {
		X->map();
	}
	if (X->buffer() == NULL) {
		printf("Buffer is NULL in LoadTensor\n");
		return 0;
	}
	int cnt = 0;
	for( unsigned int y = 0; y < height; ++y) {
		cnt += fread(reinterpret_cast<float*>(X->buffer() + X->info()->offset_element_in_bytes(Coordinates(0,y))),
			sizeof(float), width, file);
	}
	if (map) {
		X->unmap();
	}
	#else
	int cnt = fread(X->data, sizeof(float), len, file);
	#endif
	if (cnt != len) {
		printf("error reading %s: %i of %i samples acquired\n", fn, cnt, len);
	}
	fclose(file);
	return len;
}

int StreamTensor(CLTensor* X, FILE *file, int width, int height, int batch, bool map) {
	int cnt = 0;
	int len = width*height*batch;
	#ifdef ARM_API
	if (map) {
		X->map();
	}
	if (X->buffer() == NULL) {
		printf("Buffer is NULL in StreamTensor\n");
		return 0;
	}
	for( unsigned int z = 0; z < batch; ++z) {
		for( unsigned int y = 0; y < height; ++y) {
			cnt += fread(reinterpret_cast<float*>(X->buffer() + X->info()->offset_element_in_bytes(Coordinates(0,y,z))),
				sizeof(float), width, file);
		}
	}
	if (map) {
		X->unmap();
	}
	#else
	cnt = fread(X->data, sizeof(float), len, file);
	#endif
	if (cnt != len) {
		printf("error reading: %i of %i samples acquired\n", cnt, len);
	}
	return cnt;
}

void StreamOutput(FILE* file, CLTensor* X, int sz, bool map){
	#ifdef ARM_API
	if (map) {
		X->map();
	}
	if (X->buffer() == NULL) {
		printf("Buffer is NULL in StreamOutput\n");
	}
	float *p = reinterpret_cast<float*>(X->buffer()) + X->info()->offset_element_in_bytes(Coordinates(0, 0));
        for (int i = 0; i < sz; i ++) {
                fprintf(file, "%f\n", p[i]);
        }
	if (map) {
		X->unmap();
	}
	#else
	for (int i = 0; i < sz; i ++) {
		fprintf(file, "%f\n", X->data[i]);
	}
	#endif
}

