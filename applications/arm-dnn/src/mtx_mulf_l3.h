#ifndef MULL3_H
#define MULL3_H

#include "common.h"
#include "dnn.h"
#include <stdio.h>

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
#else
  #include "IntelCL/CLTensor.h"
  #include "IntelCL/CLFunctions.h"
#endif

using namespace arm_compute;

typedef struct {
	shared_t *shared;
	int iteration;
	CLTensor* weights3[FIFO_SIZE];
	CLFullyConnectedLayer dense1[FIFO_SIZE];
} mtx_mulf_l3_data_t;

int mtx_mulf_l3Init(mtx_mulf_l3_data_t *data);
void *mtx_mulf_l3Fire(void *p);
void mtx_mulf_l3Finish(mtx_mulf_l3_data_t *data);

#endif
