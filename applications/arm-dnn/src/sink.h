#include <stdio.h>
#include "common.h"

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
#else
  #include "IntelCL/CLTensor.h"
  #include "IntelCL/CLFunctions.h"
#endif

using namespace arm_compute;

typedef struct {
	FILE *file;
	int count;
	int length;
	shared_t *shared;
	char const *fn;
	int iteration;
	int repetitions;
	CLTensor *weights4[FIFO_SIZE];
	CLTensor *weights5[FIFO_SIZE];;
	CLTensor *out_relu3[FIFO_SIZE];;
	CLTensor *out_dense2[FIFO_SIZE];;
	CLTensor *out_relu4[FIFO_SIZE];;
	CLTensor *out_dense3[FIFO_SIZE];;
	CLTensor *out_softmax[FIFO_SIZE];;
	CLActivationLayer relu3[FIFO_SIZE];;
	CLActivationLayer relu4[FIFO_SIZE];;
	CLFullyConnectedLayer dense2[FIFO_SIZE];;
	CLFullyConnectedLayer dense3[FIFO_SIZE];;
	CLSoftmaxLayer softmax[FIFO_SIZE];;
} sink_data_t;

int sinkInit(sink_data_t *data);
void *sinkFire(void *p);
void sinkFinish(sink_data_t *data);
