#include <math.h>
#include <string.h>
#include "dnn.h"
#include "sink.h"

#include "CLHelper.h"

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

void openSink(sink_data_t *data) {
	char sinkFileName[256];
	data->file = NULL;
	data->length = CLASSES;
	sprintf(sinkFileName, "data/sink384.txt");
	data->file = fopen (sinkFileName, "w");
	if (data->file == NULL) {
		printf("Could not open %s\n", sinkFileName);
		return;
	}
	if (globalRepetitions == 0) {
		printf("Warning: number of repetitions uninitialized\n");
	}
	data->repetitions = globalRepetitions;
	data->iteration = 0;
}

int sinkInit(sink_data_t *data) {
	char fileName[256];
	openSink (data);
	for(int i = 0; i < FIFO_SIZE; i++) {
		data->weights4[i] = CreateTensor2D(100,100);
		data->weights5[i] = CreateTensor2D(100,4);

		data->out_relu3[i] = CreateTensor2D(100,1);
		data->out_dense2[i] = CreateTensor2D(100,1);
		data->out_relu4[i] = CreateTensor2D(100,1);
		data->out_dense3[i] = CreateTensor2D(4,1);
		data->out_softmax[i] = CreateTensor2D(4,1);
		CLTensor* out_dense1 = ((CLTensor**) fifoTokenGet(data->shared->inputs[0], i))[0];

		#ifdef ARM_API
		data->relu3[i].configure(out_dense1, data->out_relu3[i], ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
		data->dense2[i].configure(data->out_relu3[i], data->weights4[i], NULL, data->out_dense2[i]);
		data->relu4[i].configure(data->out_dense2[i], data->out_relu4[i], ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
		data->dense3[i].configure(data->out_relu4[i], data->weights5[i], NULL, data->out_dense3[i]);
		data->softmax[i].configure(data->out_dense3[i], data->out_softmax[i]);
		#else
		data->relu3[i].configure(out_dense1->data, data->out_relu3[i]->data, NULL, MAGIC, 1);
		data->dense2[i].configure(data->out_relu3[i]->data, data->weights4[i]->data, NULL, data->out_dense2[i]->data, MAGIC, MAGIC, 1);
		data->relu4[i].configure(data->out_dense2[i]->data, data->out_relu4[i]->data, NULL, MAGIC, 1);
		data->dense3[i].configure(data->out_relu4[i]->data, data->weights5[i]->data, NULL, data->out_dense3[i]->data, CLASSES, MAGIC, 1);
		data->softmax[i].configure(data->out_dense3[i]->data, data->out_softmax[i]->data, 4, 1);
		#endif
		sprintf(fileName, "data/ip4.bin");
		LoadTensor2D(data->weights4[i], fileName, 100, 100, true);
		sprintf(fileName, "data/ip_last.bin");
		LoadTensor2D(data->weights5[i], fileName, 100, 4, true);
		FillTensor(data->out_relu3[i]);
		FillTensor(data->out_dense2[i]);
		FillTensor(data->out_relu4[i]);
		FillTensor(data->out_dense3[i]);
		FillTensor(data->out_softmax[i]);
	}
	return 0;
}

void *sinkFire(void *p) {
	sink_data_t *data = (sink_data_t *) p;
	if(data->iteration >= data->repetitions) {
		printf("Sink finished\n");
		globalApplicationFinished = 1;
	} else {
		if (data->file != NULL) {
			fifoReadStart(data->shared->inputs[0]);

			data->relu3[data->iteration%FIFO_SIZE].run();
			data->dense2[data->iteration%FIFO_SIZE].run();
			data->relu4[data->iteration%FIFO_SIZE].run();
			data->dense3[data->iteration%FIFO_SIZE].run();
			data->softmax[data->iteration%FIFO_SIZE].run();

			#ifdef ARM_API
			CLScheduler::get().sync();
			data->out_softmax[data->iteration%FIFO_SIZE]->map();
			StreamOutput(data->file, data->out_softmax[data->iteration%FIFO_SIZE], 4, true);
			data->out_softmax[data->iteration%FIFO_SIZE]->unmap();
			#else
			StreamOutput(data->file, data->out_softmax[data->iteration%FIFO_SIZE], 4, true);
			#endif
			fifoReadEnd(data->shared->inputs[0]);
		}
		data->iteration ++;
	}

	return p;
}

void sinkFinish(sink_data_t *data) {
	for(int i = 0; i < FIFO_SIZE; i++) {
		#ifdef ARM_API
		data->out_relu3[i]->allocator()->free();
		data->out_relu4[i]->allocator()->free();
		data->out_dense2[i]->allocator()->free();
		data->out_dense3[i]->allocator()->free();
		data->out_softmax[i]->allocator()->free();
		data->weights4[i]->allocator()->free();
		data->weights5[i]->allocator()->free();
		#else
		free (data->out_relu3[i]->data);
		free (data->out_dense2[i]->data);
		free (data->out_relu4[i]->data);
		free (data->out_dense3[i]->data);
		free (data->out_softmax[i]->data);
		free (data->weights4[i]->data);
		free (data->weights5[i]->data);
		#endif
		delete data->weights4[i];
		delete data->weights5[i];
		delete data->out_dense3[i];
		delete data->out_softmax[i];
		delete data->out_relu4[i];
		delete data->out_relu3[i];
		delete data->out_dense2[i];
	}
	if (data->file != NULL) {
		fflush (data->file);
		fclose (data->file);
		data->file = NULL;
	}
}
