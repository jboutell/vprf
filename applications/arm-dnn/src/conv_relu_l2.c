/****************************************************************************\

    OpenCL kernels for combined convolution+relu operations
    author: Jani Boutellier, Tampere Univ. of Technology

    Adapted from LIDE-C implementation written by Renjie Xie
    Described in "Resource-Constrained Implementation and Optimization
      of a Deep Neural Network for Vehicle Classification"
    by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
    EUSIPCO 2016

    v. 1.1 / Jan 05, 2017

\****************************************************************************/

#include "dnn.h"
#include "conv_relu_l2.h"
#ifdef DBGPRINT
	#include <stdio.h>
#endif

#include "CLHelper.h"

int conv_relu_l2Init(conv_relu_l2_data_t *data) {
	char fileName[256];
	for(int i = 0; i < FIFO_SIZE; i++) {
		data->weights2[i] = CreateTensor4D(5,5,32,32);

		CLTensor *out_mxpool1 = ((CLTensor**) fifoTokenGet(data->shared->inputs[0], i))[0];
		CLTensor *out_mxpool2 = ((CLTensor**) fifoTokenGet(data->shared->outputs[0], i))[0];

		#ifdef ARM_API
		data->out_conv2[i] = CreateTensor3D(48,48,32);
		data->out_relu2[i] = CreateTensor3D(48,48,32);
		#endif

		#ifdef ARM_API
		data->conv2[i].configure(out_mxpool1, data->weights2[i], NULL, data->out_conv2[i], PadStrideInfo(1,1,2,2));
		data->relu2[i].configure(data->out_conv2[i], data->out_relu2[i], ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
		data->mxpool2[i].configure(data->out_relu2[i], out_mxpool2, PoolingLayerInfo(PoolingType::MAX, 2,PadStrideInfo(2,2,0,0)));
		#else
		data->conv2[i].configure(out_mxpool1->data, data->weights2[i]->data, NULL, out_mxpool2->data, false);
		#endif

		sprintf(fileName, "data/conv2_flipped.bin");
		LoadTensor4D(data->weights2[i], fileName, 5, 5, 32, 32, true);
		#ifdef ARM_API
		FillTensor(data->out_conv2[i]);
		FillTensor(data->out_relu2[i]);
		#endif
	}
	data->iteration = 0;
	return 0;
}
void conv_relu_l2Finish(conv_relu_l2_data_t *data) {
	for(int i = 0; i < FIFO_SIZE; i++) {
		#ifdef ARM_API
		data->out_conv2[i]->allocator()->free();
		data->out_relu2[i]->allocator()->free();
		data->weights2[i]->allocator()->free();
		delete data->out_conv2[i];
		delete data->out_relu2[i];
		#else
		free (data->weights2[i]->data);
		#endif
		delete data->weights2[i];
	}
}


void* conv_relu_l2Fire (void *p) {
	conv_relu_l2_data_t *data = (conv_relu_l2_data_t *) p;
	fifoReadStart(data->shared->inputs[0]);
	fifoWriteStart(data->shared->outputs[0]);

	data->conv2[data->iteration%FIFO_SIZE].run();
	#ifdef ARM_API
	data->relu2[data->iteration%FIFO_SIZE].run();
	data->mxpool2[data->iteration%FIFO_SIZE].run();
	#endif
	data->iteration++;

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
