/****************************************************************************\

    OpenCL kernels for combined convolution+relu operations
    author: Jani Boutellier, Tampere Univ. of Technology

    Adapted from LIDE-C implementation written by Renjie Xie
    Described in "Resource-Constrained Implementation and Optimization
      of a Deep Neural Network for Vehicle Classification"
    by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
    EUSIPCO 2016

    v. 1.1 / Jan 05, 2017

\****************************************************************************/

#include "dnn.h"
#include "conv_relu_l1.h"
#include <string.h>
#ifdef DBGPRINT
	#include <stdio.h>
#endif

#include "CLHelper.h"

int conv_relu_l1Init(conv_relu_l1_data_t *data) {
	char fileName[256];
	for(int i = 0; i < FIFO_SIZE; i++) {
		data->weights1[i] = CreateTensor4D(5,5,3,32);
		CLTensor *in_l1mid = ((CLTensor**) fifoTokenGet(data->shared->inputs[0], i))[0];
		CLTensor *out_mxpool1 = ((CLTensor**) fifoTokenGet(data->shared->outputs[0], i))[0];
		#ifdef ARM_API
		data->out_conv1[i] = CreateTensor3D(96,96,32);
		data->out_relu1[i] = CreateTensor3D(96,96,32);
		#endif

		#ifdef ARM_API
		data->conv1[i].configure(in_l1mid, data->weights1[i], NULL, data->out_conv1[i], PadStrideInfo(1,1,2,2), WeightsInfo());
		data->relu1[i].configure(data->out_conv1[i], data->out_relu1[i], ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
		data->mxpool1[i].configure(data->out_relu1[i], out_mxpool1, PoolingLayerInfo(PoolingType::MAX, 2,PadStrideInfo(2,2,0,0)));
		#else
		data->conv1[i].configure(in_l1mid->data, data->weights1[i]->data, NULL, out_mxpool1->data, true);
		#endif

		sprintf(fileName, "data/conv1_flipped.bin");
		LoadTensor4D(data->weights1[i], fileName, 5, 5, 3, 32, true);
		#ifdef ARM_API
		FillTensor(data->out_conv1[i]);
		FillTensor(data->out_relu1[i]);
		#endif
	}
	data->iteration = 0;
	return 0;
}

void conv_relu_l1Finish(conv_relu_l1_data_t *data) {
	for(int i = 0; i < FIFO_SIZE; i++) {
		#ifdef ARM_API
		data->weights1[i]->allocator()->free();
		data->out_conv1[i]->allocator()->free();
		data->out_relu1[i]->allocator()->free();
		delete data->out_conv1[i];
		delete data->out_relu1[i];
		#else
		free (data->weights1[i]->data);
		#endif
		delete data->weights1[i];
	}
}

void* conv_relu_l1Fire (void *p) {
	conv_relu_l1_data_t *data = (conv_relu_l1_data_t *) p;
	fifoReadStart(data->shared->inputs[0]);
	fifoWriteStart(data->shared->outputs[0]);

	data->conv1[data->iteration%FIFO_SIZE].run();
	#ifdef ARM_API
	data->relu1[data->iteration%FIFO_SIZE].run();
	data->mxpool1[data->iteration%FIFO_SIZE].run();
	#endif
	data->iteration++;

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}
