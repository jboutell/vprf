#ifndef RELUL1_H
#define RELUL1_H

#include "common.h"
#include "dnn.h"

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
#else
  #include "IntelCL/CLTensor.h"
  #include "IntelCL/CLFunctions.h"
#endif

using namespace arm_compute;

typedef struct {
	shared_t *shared;
	int iteration;
	CLTensor* weights1[FIFO_SIZE];
	CLConvolutionLayer conv1[FIFO_SIZE];
	#ifdef ARM_API
	CLTensor* out_conv1[FIFO_SIZE];
	CLTensor* out_relu1[FIFO_SIZE];
	CLActivationLayer relu1[FIFO_SIZE];
	CLPoolingLayer mxpool1[FIFO_SIZE];
	#endif
} conv_relu_l1_data_t;

int conv_relu_l1Init(conv_relu_l1_data_t *data);
void *conv_relu_l1Fire(void *p);
void conv_relu_l1Finish(conv_relu_l1_data_t *data);

#endif
