#ifndef RELUL2_H
#define RELUL2_H

#include "common.h"
#include "dnn.h"

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
#else
  #include "IntelCL/CLTensor.h"
  #include "IntelCL/CLFunctions.h"
#endif

using namespace arm_compute;

typedef struct {
	shared_t *shared;
	int iteration;
	CLTensor* weights2[FIFO_SIZE];
	CLConvolutionLayer conv2[FIFO_SIZE];
	#ifdef ARM_API
	CLTensor* out_conv2[FIFO_SIZE];
	CLTensor* out_relu2[FIFO_SIZE];
	CLActivationLayer relu2[FIFO_SIZE];
	CLPoolingLayer mxpool2[FIFO_SIZE];
	#endif
} conv_relu_l2_data_t;

int conv_relu_l2Init(conv_relu_l2_data_t *data);
void *conv_relu_l2Fire(void *p);
void conv_relu_l2Finish(conv_relu_l2_data_t *data);

#endif
