#define CHANNELS 3
#define PATCH1 96
#define PATCH1SQ (PATCH1*PATCH1)
#define PATCH2 (PATCH1/2)
#define PATCH2SQ (PATCH2*PATCH2) 
#define PATCH3 (PATCH2/2)
#define PATCH3SQ (PATCH3*PATCH3) 
#define CONV1SIZE 5
#define CONV1SQ (CONV1SIZE*CONV1SIZE)
#define CONV2SIZE 5
#define CONV2SQ (CONV2SIZE*CONV2SIZE)
#define FM_COUNT 32
#define TILE_NUM 2
#define MAGIC 100
#define CLASSES 4
#define SL1SIZE (FM_COUNT*PATCH1SQ)
#define PAD_NUM 2

#define matrix(pointer,length,row,col) pointer[row*length+col]
#define matrix1(pointer,offset,length,row,col) pointer[offset+row*length+col]

#define REPEAT 1

#include <stdlib.h>
#include <stdio.h>

void pad (float *input, float *fifo, int width, int padding, int channels);
void conv_relu_1 (float * fifo_in, float * fifo_out, float * wgt);void conv_relu_2 (float * fifo_in, float * fifo_out, float * wgt);
void conv_relu_2 (float * fifo_in, float * fifo_out, float * wgt);void conv_relu_2 (float * fifo_in, float * fifo_out, float * wgt);
void lide_c_mtx_mulf(float* A, float* B, float* c_mul, int Mdim, int Ndim, int Kdim);
void lide_c_image_relu(float *bef_array, float *aft_array, int row, int column);
void lide_c_softmax_write(float *output_array, float *input_array, int m, int n);

