#include <stdio.h>
#include "common.h"

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

typedef struct {
	shared_t *shared;
	int iteration;
	char const *fn;
	float *imtmp;

	AVFormatContext * pFormatCtx;
	AVCodec * pCodec;
	AVCodecContext * pCodecCtxOrig;
	AVCodecContext * pCodecCtx;
	AVFrame * pFrame;
	AVFrame * pFrameRGB;
	AVPacket * pPacket;
	struct SwsContext * sws_ctx;
	uint8_t * buffer;
	int videoStream;
	int targetW;
	int targetH;
	int nbFrames;
	int currentFrame;
	int status;
} source_data_t;

void sourceInit(source_data_t *data);
void *sourceFire(void *p);
void sourceFinish(source_data_t *data);

int init_videoSource(source_data_t *data, char *fn);
int run_videoSource(source_data_t *data);
void close_videoSource(source_data_t *data);


