#define C1 3
#define BATCH 4
#define KERNEL_DIM 3
#define KERNEL_RADIUS 1
#define FMAPS1 6
#define BLOCK_DIM_X 300
#define BLOCK_DIM_Y 1

#define RELU6(x) min(max(x, 0.0f), 6.0f)

__kernel void kernel_conv_1x1_beta_relu6(__global float * src, __global float * dst, __global float* krn,__global float* gamma, __global float* beta, __global float* m, __global float* v,
int H, int W, int C, int F) {

	const int IMSZ = H*W;
	int px_ind = get_global_id(0) + get_global_id(1)*get_global_size(0);
	int im_ind = get_global_id(2);
	float sum = 0.0;
	for (unsigned int k = 0; k < C; k++) {
		sum += src[k*IMSZ + px_ind] * krn[im_ind*C + k];
	}
	dst[im_ind*IMSZ + px_ind] = RELU6((float)(sum + beta[im_ind]));
/*
	src += get_group_id(1)*get_local_size(1)*W + get_group_id(0)*get_local_size(0) + get_group_id(2)*W*H*C + get_local_id(1)*W + get_local_id(0);
	dst += get_group_id(1)*get_local_size(1)*W + get_group_id(0)*get_local_size(0) + get_group_id(2)*W*H*F;
	for (int f = 0; f < F; f++) {
		float sum_2 = 0.0;
		for (int c = 0; c < C; c++) {
			sum_2 += krn[c + f*C] * src[c*W*H];
		}
		dst[get_local_id(1)*W + get_local_id(0)] =  RELU6(beta[f] + sum_2); 
		dst += W*H; 
	}
*/
}

