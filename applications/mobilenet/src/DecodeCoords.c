#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "dnn.h"
#include "ports.h"
#include "DecodeCoords.h"

static void CHWtoHWC(float* dst, float* src, int H, int W, int C) {
	for (int c = 0; c < C; c++)
		for (int i = 0; i < H; i++)
			for (int j = 0; j < W; j++)
				dst[j*C + i*C*W + c] = (src[c*H*W + i*W + j]);
}

void EncodeCoords(float *ecd_out, float *ecd_in, float *anchors) {
    for(int i = 0; i < NUM_ANCHORS*4; i = i + 4) {
        float yACtr = (anchors[i] + anchors[i+2])/2;
        float xACtr = (anchors[i+1] + anchors[i+3])/2;
        float ha = anchors[i+2] - anchors[i];
        float wa = anchors[i+3] - anchors[i+1];

        float ty = ecd_in[i]/10;
        float tx = ecd_in[i+1]/10;
        float th = ecd_in[i+2]/5;
        float tw = ecd_in[i+3]/5;

        float w = exp(tw) * wa;
        float h = exp(th) * ha;

        float yCtr = ty * ha + yACtr;
        float xCtr = tx * wa + xACtr;

        float ecd0 = yCtr - h/2;
        float ecd1 = xCtr - w/2;
        float ecd2 = yCtr + h/2;
        float ecd3 = xCtr + w/2;

        ecd_out[i] = ecd0;
        ecd_out[i+1] = ecd1;
        ecd_out[i+2] = ecd2;
        ecd_out[i+3] = ecd3;
    }
}

void DecodeCoordsInit(DecodeCoords_data_t *data) {

	data->tmp = (float*) malloc(sizeof(float)*NUM_ANCHORS*4);
	data->anchors = (float*) malloc(sizeof(float)*NUM_ANCHORS*4);
	FILE *fanch = fopen("anchors.txt", "r");
    for(int i = 0; i < NUM_ANCHORS*4; i = i + 4) {
		fscanf(fanch, "%f %f %f %f\n", &data->anchors[i], &data->anchors[i+1],
			&data->anchors[i+2], &data->anchors[i+3]);
    }
	fclose(fanch);
}

void *DecodeCoordsFire(void *p) {
	DecodeCoords_data_t *data = (DecodeCoords_data_t *) p;

	int index = 0;
	cl_float *input23 = (cl_float *) fifoReadStart(data->shared->inputs[DECODECOORDS_IN_23]);
	CHWtoHWC(&data->tmp[index], input23, 19, 19, 12);
	index += 19*19*12;
	fifoReadEnd(data->shared->inputs[DECODECOORDS_IN_23]);

	cl_float *input29 = (cl_float *) fifoReadStart(data->shared->inputs[DECODECOORDS_IN_29]);
	CHWtoHWC(&data->tmp[index], input29, 10, 10, 24);
	index += 10*10*24;
	fifoReadEnd(data->shared->inputs[DECODECOORDS_IN_29]);

	cl_float *input33 = (cl_float *) fifoReadStart(data->shared->inputs[DECODECOORDS_IN_33]);
	CHWtoHWC(&data->tmp[index], input33, 5, 5, 24);
	index += 5*5*24;
	fifoReadEnd(data->shared->inputs[DECODECOORDS_IN_33]);

	cl_float *input37 = (cl_float *) fifoReadStart(data->shared->inputs[DECODECOORDS_IN_37]);
	CHWtoHWC(&data->tmp[index], input37, 3, 3, 24);
	index += 3*3*24;
	fifoReadEnd(data->shared->inputs[DECODECOORDS_IN_37]);

	cl_float *input41 = (cl_float *) fifoReadStart(data->shared->inputs[DECODECOORDS_IN_41]);
	CHWtoHWC(&data->tmp[index], input41, 2, 2, 24);
	index += 2*2*24;
	fifoReadEnd(data->shared->inputs[DECODECOORDS_IN_41]);

	cl_float *input45 = (cl_float *) fifoReadStart(data->shared->inputs[DECODECOORDS_IN_45]);
	CHWtoHWC(&data->tmp[index], input45, 1, 1, 24);
	index += 1*1*24;
	fifoReadEnd(data->shared->inputs[DECODECOORDS_IN_45]);

	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[DECODECOORDS_ECDS]);
	EncodeCoords(output, data->tmp, data->anchors);
	fifoWriteEnd(data->shared->outputs[DECODECOORDS_ECDS]);

	return p;
}

void DecodeCoordsFinish(DecodeCoords_data_t *data) {
	free(data->tmp);
	free(data->anchors);
}



