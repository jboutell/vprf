#include <stdio.h>
#include "common.h"
#include <iostream>

typedef struct {
	FILE *file;
	shared_t *shared;
	int iteration;
	int repetitions;
	char const *fn;
} Sink_data_t;

void SinkInit(Sink_data_t *data);
void *SinkFire(void *p);
void SinkFinish(Sink_data_t *data);
