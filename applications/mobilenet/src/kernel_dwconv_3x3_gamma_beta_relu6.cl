#define C1 3
#define BATCH 4
#define KERNEL_DIM 3
#define KERNEL_RADIUS 1
#define FMAPS1 6
#define BLOCK_DIM_X 300
#define BLOCK_DIM_Y 1

#define RELU6(x) min(max(x, 0.0f), 6.0f)

__kernel void kernel_dwconv_3x3_gamma_beta_relu6(__global float * src, __global float* dst, __global float* krn,__global float* gamma, __global float* beta, __global float* m, __global float* v,
int H, int W, int C, int F) {

	int w = get_global_id(0);
	int h = get_global_id(1);
	int c = get_global_id(2);
	float sum = 0.0;
	for (int k = 0; k < KERNEL_DIM; k++) {
		for (int l = 0; l < KERNEL_DIM; l++) {
			int cond = ((w + l) > 0) && ((w + l) <= W) && ((h + k) > 0) && ((h + k) <= H);
			float srcval = src[(H*c + h + k - 1)*W + w + (l - 1)];
			float krnval = krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM];
			sum += cond ? srcval*krnval : 0.0;
		}
	}
	dst[h*W + w + H*W*c] = RELU6(gamma[c] * sum + beta[c]);
/*
	for (int c = 0; c < C; c++) {
		float sum_2 = 0.0;
		for (int k = 0; k < KERNEL_DIM; k++)
			for (int l = 0; l < KERNEL_DIM; l++) {
				int cond = ((get_group_id(1)*get_local_size(1) + get_local_id(1) + (k - 1)) == H) || ((get_group_id(1)*get_local_size(1) + get_local_id(1) + (k - 1)) == -1) || ((get_group_id(0)*get_local_size(0) + get_local_id(0) + (l - 1)) == W) || ((get_group_id(0)*get_local_size(0) + get_local_id(0) + (l - 1)) == -1);
				sum_2 += krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM] * (cond ? 0.0 : src[((get_group_id(1)*get_local_size(1) + get_local_id(1))*W + (k - 1)*W + get_group_id(0)*get_local_size(0) + get_local_id(0) + (l - 1) + c*W*H)]);
			}
		dst[c*W*H + (get_group_id(1)*get_local_size(1) + get_local_id(1))*W + get_group_id(0)*get_local_size(0) + get_local_id(0)] = RELU6(beta[c] + gamma[c] * sum_2); 
	}
*/
}

