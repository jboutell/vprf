#include "dnn.h"
#include "source.h"
#include "ports.h"
#include <stdlib.h>
#include <string.h>

#define TOKENSIZE 1080000
#define DIM 300
extern volatile int globalRepetitions;

void convertToFloat (float *dst, unsigned char *src, int len) {
	for (int i = 0; i < len; i++) {
		int val = src[i];
		float fval = (float) val;
		fval -= 128.0;
		fval /= 128.0;
		dst[i] = fval;
	}
}

void HWCtoCHW(float* dst, float* src, int H, int W, int C) {
	for (int c = 0; c < C; c++)
		for (int i = 0; i < H; i++)
			for (int j = 0; j < W; j++)
				dst[c*H*W + i*W + j] = (src[j*C + i*C*W + c]);
}

void sourceInit(source_data_t *data) {
	char fileName[256];
	sprintf(fileName, "soccer_4cif.y4m"); //"image1_crop.flt");
	data->iteration = 0;

	data->targetW = DIM;
	data->targetH = DIM;
	data->nbFrames = 100;
	globalRepetitions = data->nbFrames;
	data->currentFrame = 0;
	init_videoSource(data, fileName);

	data->imtmp = (float*) malloc(DIM*DIM*3*sizeof(float));
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		printf("Source finished\n");
		actorTerminate();
	} else if (data->currentFrame < data->nbFrames) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[SOURCE_OUT_SOURCE]);
		#ifdef DBGPRINT
		printf("Node source fires\n");
		#endif

		run_videoSource(data); // new frame is placed to data->pFrameRGB->data[0]
		convertToFloat(data->imtmp, data->pFrameRGB->data[0], DIM*DIM*3);
		HWCtoCHW(wbufout1, data->imtmp, DIM, DIM, 3) ;

		fifoWriteEnd(data->shared->outputs[SOURCE_OUT_SOURCE]);

		float *frame = (float *) fifoWriteStart(data->shared->outputs[SOURCE_FRAME]);
		memcpy(frame, data->imtmp, sizeof(float)*DIM*DIM*3);
		fifoWriteEnd(data->shared->outputs[SOURCE_FRAME]);
		float *width = (float *) fifoWriteStart(data->shared->outputs[SOURCE_WIDTH]);
		width[0] = DIM;
		fifoWriteEnd(data->shared->outputs[SOURCE_WIDTH]);
		float *height = (float *) fifoWriteStart(data->shared->outputs[SOURCE_HEIGHT]);
		height[0] = DIM;
		fifoWriteEnd(data->shared->outputs[SOURCE_HEIGHT]);

		data->iteration ++;
	}
	return p;
}

void sourceFinish(source_data_t *data) {
	close_videoSource(data);
	free(data->imtmp);
}

