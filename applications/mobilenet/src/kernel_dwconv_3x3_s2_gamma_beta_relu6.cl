#define C1 3
#define BATCH 4
#define KERNEL_DIM 3
#define KERNEL_RADIUS 1
#define FMAPS1 6
#define BLOCK_DIM_X 300
#define BLOCK_DIM_Y 1

#define RELU6(x) min(max(x, 0.0f), 6.0f)

__kernel void kernel_dwconv_3x3_s2_gamma_beta_relu6(__global float * src, __global float* dst, __global float* krn,__global float* gamma, __global float* beta, __global float* m, __global float* v,
int H, int W, int C, int F) {

	int even_s2 = (W % 2) == 0;
	int w = get_global_id(0) * 2;
	int h = get_global_id(1) * 2;
	int c = get_global_id(2);

	if (even_s2) {
		int sH = H / 2, sW = W / 2;
		float sum = 0.0;
		for (int k = 0; k < KERNEL_DIM; k++) {
			for (int l = 0; l < KERNEL_DIM; l++) {
				int cond = (((w + l) >= 0) && ((w + l) < W) && ((h + k) >= 0) && ((h + k) < H));
				float srcval = src[(c*H + h + k)*W + w + l];
				float krnval = krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM];
				sum += cond ? srcval*krnval : 0.0;
			}
		}
		dst[(h/2)*sW + (w/2) + (sH*sW*c)] = RELU6(gamma[c] * sum + beta[c]);
	} else {
		int sH = H / 2 + (H % 2 != 0), sW = W / 2 + (W % 2 != 0);
		float sum = 0.0;
		for (int k = 0; k < KERNEL_DIM; k++) {
			for (int l = 0; l < KERNEL_DIM; l++) {
				int cond = (((w + l) > 0) && ((w + l) <= W) && ((h + k) > 0) && ((h + k) <= H));
				float srcval = src[(H*c + h + k - 1)*W + w + (l - 1)];
				float krnval = krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM];
				sum += cond ? srcval * krnval : 0.0;
			}
		}
		dst[(h/2)*sW + (w/2) + (sH*sW*c)] = RELU6(gamma[c] * sum + beta[c]);
	}

/*
	int even_s2 = (W % 2) == 0;
	int gx = get_group_id(0)*get_local_size(0) + get_local_id(0);
	int gy = get_group_id(1)*get_local_size(1) + get_local_id(1);
	int c = get_global_id(2);
	if (even_s2) {
		int sH = H / 2;
		int sW = W / 2;
		float sum_2 = 0.0;
		for (int k = 0; k < KERNEL_DIM; k++) {
			for (int d = 0; d < KERNEL_DIM; d++) {
				int gyt = gy * 2 + 1;
				int gxt = gx * 2 + 1;
				int cond = ((gyt + (k - 1)) == H)
					|| ((gyt + (k - 1)) == -1)
					|| ((gxt + (d - 1)) == W)
					|| ((gxt + (d - 1)) == -1);
				sum_2 += krn[k*KERNEL_DIM + d + c*KERNEL_DIM*KERNEL_DIM]
					* (cond ? 0.0 : src[(gyt*W
					+ (k - 1)*W + gxt + (d - 1) + c*W*H)]);
			}
		}
		dst[c*sW*sH + gy*sW + gx] = RELU6(beta[c] + gamma[c] * sum_2);
	} else {
		int sH = H / 2 + (H % 2);
		int sW = W / 2 + (W % 2);
		float sum_2 = 0.0;
		for (int k = 0; k < KERNEL_DIM; k++) {
			for (int d = 0; d < KERNEL_DIM; d++) {
				int gyt = gy * 2;
				int gxt = gx * 2;
				int cond = ((gyt + (k - 1)) == H)
					|| ((gyt + (k - 1)) == -1)
					|| ((gxt + (d - 1)) == W)
					|| ((gxt + (d - 1)) == -1);
				sum_2 += krn[k*KERNEL_DIM + d + c*KERNEL_DIM*KERNEL_DIM]
					* (cond ? 0.0 : src[(gyt*W
					+ (k - 1)*W + gxt + (d - 1) + c*W*H)]);
			}
		}
		dst[c*sW*sH + gy*sW + gx] = RELU6(beta[c] + gamma[c] * sum_2);
	}
*/
}


