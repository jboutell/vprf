#include <string.h>
#include <stdlib.h>
#include "dnn.h"
#include "ports.h"
#include "CombinePreds.h"

void CombinePredsInit(CombinePreds_data_t *data) {
}

void CombinePredsFinish(CombinePreds_data_t *data) {
}

static void CHWtoHWC(float* dst, float* src, int H, int W, int C) {
	for (int c = 0; c < C; c++)
		for (int i = 0; i < H; i++)
			for (int j = 0; j < W; j++)
				dst[j*C + i*C*W + c] = (src[c*H*W + i*W + j]);
}

void *CombinePredsFire(void *p) {
	CombinePreds_data_t *data = (CombinePreds_data_t *) p;

	cl_float *output = (cl_float *) fifoWriteStart(data->shared->outputs[COMBINEPREDS_PREDS]);

	int index = 0;
	cl_float *input24 = (cl_float *) fifoReadStart(data->shared->inputs[COMBINEPREDS_IN_24]);
	CHWtoHWC(&output[index], input24, 19, 19, 273);
	index += 19*19*273;
	fifoReadEnd(data->shared->inputs[COMBINEPREDS_IN_24]);

	cl_float *input30 = (cl_float *) fifoReadStart(data->shared->inputs[COMBINEPREDS_IN_30]);
	CHWtoHWC(&output[index], input30, 10, 10, 546);
	index += 10*10*546;
	fifoReadEnd(data->shared->inputs[COMBINEPREDS_IN_30]);

	cl_float *input34 = (cl_float *) fifoReadStart(data->shared->inputs[COMBINEPREDS_IN_34]);
	CHWtoHWC(&output[index], input34, 5, 5, 546);
	index += 5*5*546;
	fifoReadEnd(data->shared->inputs[COMBINEPREDS_IN_34]);

	cl_float *input38 = (cl_float *) fifoReadStart(data->shared->inputs[COMBINEPREDS_IN_38]);
	CHWtoHWC(&output[index], input38, 3, 3, 546);
	index += 3*3*546;
	fifoReadEnd(data->shared->inputs[COMBINEPREDS_IN_38]);

	cl_float *input42 = (cl_float *) fifoReadStart(data->shared->inputs[COMBINEPREDS_IN_42]);
	CHWtoHWC(&output[index], input42, 2, 2, 546);
	index += 2*2*546;
	fifoReadEnd(data->shared->inputs[COMBINEPREDS_IN_42]);

	cl_float *input46 = (cl_float *) fifoReadStart(data->shared->inputs[COMBINEPREDS_IN_46]);
	CHWtoHWC(&output[index], input46, 1, 1, 546);
	index += 1*1*546;
	fifoReadEnd(data->shared->inputs[COMBINEPREDS_IN_46]);

	fifoWriteEnd(data->shared->outputs[COMBINEPREDS_PREDS]);
/*
	fpred = fopen("predcheck.txt", "w");
	for (int i = 0; i < NUM_ANCHORS*NUM_CLASSES; i++) {
		fprintf(fpred, "%.2f\n", pred[i]);
	}
	fclose(fpred);
*/
	return p;
}



