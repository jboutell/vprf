#ifndef DecodeCoords_H
#define DecodeCoords_H

#include "common.h"
#include "dnn.h"

typedef struct {
	float* anchors;
	float* tmp;
	shared_t *shared;
} DecodeCoords_data_t;

void DecodeCoordsInit(DecodeCoords_data_t *data);
void *DecodeCoordsFire(void *p);
void DecodeCoordsFinish(DecodeCoords_data_t *data);
#endif

