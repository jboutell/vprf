#include <string.h>
#include <stdlib.h>
#include "dnn.h"
#include "ports.h"
#include "Tracker.h"
#include <math.h>

int SSDTH = 10000;

void TrackerInit(Tracker_data_t *data) {
	data->iteration = 0;
	data->objs = (object_data_t *) malloc (sizeof(object_data_t)*MAX_DETECTIONS);
	data->cobjs = (object_data_t *) malloc (sizeof(object_data_t)*MAX_DETECTIONS);
	data->objcnt = 0;
	for (int i = 0; i < MAX_DETECTIONS; i++) {
		data->objs[i].box[0] = 0;
		data->objs[i].box[1] = 0;
		data->objs[i].box[2] = 0;
		data->objs[i].box[3] = 0;
		data->objs[i].cx = 0;
		data->objs[i].cy = 0;
		data->cobjs[i].box[0] = 0;
		data->cobjs[i].box[1] = 0;
		data->cobjs[i].box[2] = 0;
		data->cobjs[i].box[3] = 0;
		data->cobjs[i].cx = 0;
		data->cobjs[i].cy = 0;
	}

}

unsigned int ssd (int v1, int v2) {
	
	int diff = v1 - v2;
	return diff*diff;
}



unsigned int computeDistance(Tracker_data_t *data, int j, unsigned int *ssdval) {
	unsigned int min_ind = MAX_DETECTIONS, min = 1000000000;
	for	(int i = 0; i < data->objcnt; i++) {
		unsigned int ssd_sum = 0;
		ssd_sum += ssd(data->cobjs[j].box[0], data->objs[i].box[0]);
		ssd_sum += ssd(data->cobjs[j].box[1], data->objs[i].box[1]);
		ssd_sum += ssd(data->cobjs[j].box[2], data->objs[i].box[2]);
		ssd_sum += ssd(data->cobjs[j].box[3], data->objs[i].box[3]);
		if (ssd_sum < min) {
			min_ind = i;
			min = ssd_sum;
		}
	}
	ssdval[0] = min;

	if (min > SSDTH) {
		return MAX_DETECTIONS;
	}

	return min_ind;
}

void decodeCoords(float *boxes, float width, float height) {
}

void *TrackerFire(void *p) {
	Tracker_data_t *data = (Tracker_data_t *) p;

	cl_float *count = (cl_float *) fifoReadStart(data->shared->inputs[TRACKER_COUNT]);
	cl_float *boxes = (cl_float *) fifoReadStart(data->shared->inputs[TRACKER_BOXES]);
	cl_float *scores = (cl_float *) fifoReadStart(data->shared->inputs[TRACKER_SCORES]);
	cl_float *classes = (cl_float *) fifoReadStart(data->shared->inputs[TRACKER_CLASSES]);
	cl_int *tboxes = (cl_int *) fifoWriteStart(data->shared->outputs[TRACKER_TBOXES]);
	cl_int *tscores = (cl_int *) fifoWriteStart(data->shared->outputs[TRACKER_TSCORES]);
	cl_int *tclasses = (cl_int *) fifoWriteStart(data->shared->outputs[TRACKER_TCLASSES]);

	float width = 300.0;
	float height = 300.0;

	for (int i = 0; i < count[0]; i++) {
		data->cobjs[i].box[0] = round(boxes[4*i] * height); // y1
		data->cobjs[i].box[1] = round(boxes[4*i+1] * width); // x1
		data->cobjs[i].box[2] = round(boxes[4*i+2] * height); // y2
		data->cobjs[i].box[3] = round(boxes[4*i+3] * width); // x2
		data->cobjs[i].type = round(classes[i]);
	}

	printf("Frame %i\n", data->iteration);
	for (int j = 0; j < count[0]; j++) {
		unsigned int ssd;
		unsigned int ind = computeDistance(data, j, &ssd); // look for best match between this object (j) and ones from previous iterations (ind)

		// calculate object center
		data->cobjs[j].cx = (data->cobjs[j].box[3] + data->cobjs[j].box[1])/2;
		data->cobjs[j].cy = (data->cobjs[j].box[2] + data->cobjs[j].box[0])/2;
		if (ind == MAX_DETECTIONS) { // no match found
			// transmit zero motion vector
			tscores[j] = 0;
			tclasses[j] = 0;
			printf(" Object %i motion trajectory: %i %i (no match)\n", j, tscores[j], tclasses[j], ssd);
		} else { // match found in object 'ind'
			// calculate motion vector
			int dx = data->objs[ind].cx - data->cobjs[j].cx;
			int dy = data->objs[ind].cy - data->cobjs[j].cy;
			tscores[j] = dx;
			tclasses[j] = dy;
			printf(" Object %i motion trajectory: %i %i (%u)\n", j, tscores[j], tclasses[j], ssd);
		}
	}

	for (int i = 0; i < count[0]; i++) {
		tboxes[4*i] = data->cobjs[i].box[0];
		tboxes[4*i+1] = data->cobjs[i].box[1];
		tboxes[4*i+2] = data->cobjs[i].box[2];
		tboxes[4*i+3] = data->cobjs[i].box[3];

		data->objs[i].box[0] = data->cobjs[i].box[0];
		data->objs[i].box[1] = data->cobjs[i].box[1];
		data->objs[i].box[2] = data->cobjs[i].box[2];
		data->objs[i].box[3] = data->cobjs[i].box[3];
		data->objs[i].cx = data->cobjs[i].cx;
		data->objs[i].cy = data->cobjs[i].cy;
		data->objs[i].type = data->cobjs[i].type;
	}
	data->objcnt = count[0];

	fifoReadEnd(data->shared->inputs[TRACKER_COUNT]);
	fifoReadEnd(data->shared->inputs[TRACKER_BOXES]);
	fifoReadEnd(data->shared->inputs[TRACKER_SCORES]);
	fifoReadEnd(data->shared->inputs[TRACKER_CLASSES]);
	fifoWriteEnd(data->shared->outputs[TRACKER_TBOXES]);
	fifoWriteEnd(data->shared->outputs[TRACKER_TSCORES]);
	fifoWriteEnd(data->shared->outputs[TRACKER_TCLASSES]);
	data->iteration ++;

	return p;
}

void TrackerFinish(Tracker_data_t *data) {
	free(data->objs);
	free(data->cobjs);
}
