/**
 *  Adapted from FFmpeg API tutorial by.
 *  Rambod Rahmani <rambodrahmani@autistici.org>
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "source.h"

int init_videoSource(source_data_t *data, char *fn) {
	data->pFormatCtx = NULL;
	data->pCodec = NULL;
	data->pCodecCtxOrig = NULL;
	data->pCodecCtx = NULL;
	data->pFrame = NULL;
	data->pFrameRGB = NULL;
	data->pPacket = NULL;
	data->sws_ctx = NULL;
	data->buffer = NULL;
	data->videoStream = -1;

	av_register_all();

    int ret = avformat_open_input(&data->pFormatCtx, fn, NULL, NULL);
    if (ret < 0)
    {
		char str[256];
		av_strerror(ret, str, 256);
        printf("Could not open file %s (error %s)\n", fn, str);
        return -1;
    }

    ret = avformat_find_stream_info(data->pFormatCtx, NULL);
    if (ret < 0) {
		printf("Could not find stream information %s\n", fn);
        return -1;
    }

    av_dump_format(data->pFormatCtx, 0, fn, 0);

    for (int i = 0; i < data->pFormatCtx->nb_streams; i++) {
        if (data->pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            data->videoStream = i;
            break;
        }
    }

    if (data->videoStream == -1) {
        printf("No video stream found\n");
        return -1;
    }

    data->pCodec = avcodec_find_decoder(data->pFormatCtx->streams[data->videoStream]->codecpar->codec_id);
    if (data->pCodec == NULL) {
        printf("Unsupported codec!\n");
        return -1;
    }

    data->pCodecCtxOrig = avcodec_alloc_context3(data->pCodec);
    ret = avcodec_parameters_to_context(data->pCodecCtxOrig, data->pFormatCtx->streams[data->videoStream]->codecpar);

    data->pCodecCtx = avcodec_alloc_context3(data->pCodec);
    ret = avcodec_parameters_to_context(data->pCodecCtx, data->pFormatCtx->streams[data->videoStream]->codecpar);
    if (ret != 0) {
        printf("Could not copy codec context.\n");
        return -1;
    }

    ret = avcodec_open2(data->pCodecCtx, data->pCodec, NULL);
    if (ret < 0) {
        printf("Could not open codec.\n");
        return -1;
    }

    data->pFrame = av_frame_alloc();
    if (data->pFrame == NULL) {
        printf("Could not allocate frame.\n");
        return -1;
    }

    data->pFrameRGB = av_frame_alloc();
    if (data->pFrameRGB == NULL) {
        printf("Could not allocate frame.\n");
        return -1;
    }

    int numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGB24, data->pCodecCtx->width, data->pCodecCtx->height, 32);
    data->buffer = (uint8_t *) av_malloc(numBytes * sizeof(uint8_t));

    av_image_fill_arrays(
        data->pFrameRGB->data,
        data->pFrameRGB->linesize,
        data->buffer,
        AV_PIX_FMT_RGB24,
        data->pCodecCtx->width,
        data->pCodecCtx->height,
        32
    );

    data->pPacket = av_packet_alloc();
    if (data->pPacket == NULL) {
        printf("Could not alloc packet,\n");
        return -1;
    }

	if (data->targetW != 0) {
		data->pFrameRGB->linesize[0] = data->targetW*3;
	} else {
		data->targetW = data->pCodecCtx->width;
		data->targetH = data->pCodecCtx->height;
	}

    // initialize SWS context for software scaling
    data->sws_ctx = sws_getContext(
        data->pCodecCtx->width,
        data->pCodecCtx->height,
        data->pCodecCtx->pix_fmt,
        data->targetW,//pCodecCtx->width,
        data->targetH,//pCodecCtx->height,
        AV_PIX_FMT_RGB24,   // sws_scale destination color scheme
        SWS_BILINEAR,
        NULL,
        NULL,
        NULL
    );

	return 0;
}

int run_videoSource(source_data_t *data) {
	
	if (data->currentFrame == 0) {
		printf("Decoding video stream #%i for %i frames\n", data->videoStream, data->nbFrames);
	} 
    while (av_read_frame(data->pFormatCtx, data->pPacket) >= 0) {
        // Is this a packet from the video stream?
        if (data->pPacket->stream_index == data->videoStream) {
            // Decode video frame
            // avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &pPacket);
            // Deprecated: Use avcodec_send_packet() and avcodec_receive_frame().
			data->status = avcodec_send_packet(data->pCodecCtx, data->pPacket);
            if (data->status < 0) {
                // could not send packet for decoding
                printf("Error sending packet for decoding.\n");
                return -1;
            }

            while (data->status >= 0) {
                data->status = avcodec_receive_frame(data->pCodecCtx, data->pFrame);
                if (data->status == AVERROR(EAGAIN) || data->status == AVERROR_EOF) {
                    break; // EOF exit loop
                } else if (data->status < 0) {
                    // could not decode packet
                    printf("Error while decoding.\n");
                    return -1;
                }

                // Convert the image from its native format to RGB
                sws_scale(data->sws_ctx, (uint8_t const * const *)data->pFrame->data, data->pFrame->linesize, 0,
					data->pCodecCtx->height, data->pFrameRGB->data, data->pFrameRGB->linesize);

                // Save the frame to disk
                if (++data->currentFrame <= data->nbFrames) {
					return 1;
					//continueDecode:
                } else {
                    break;
                }
            }

            if (data->currentFrame > data->nbFrames) {
                break;
            }
        }

        // Free the packet that was allocated by av_read_frame
        av_packet_unref(data->pPacket);
    }
    if (data->currentFrame > data->nbFrames)
    	av_packet_unref(data->pPacket);
	return 0;
}

void close_videoSource(source_data_t *data) {
    // Free the RGB image
    av_free(data->buffer);
    av_frame_free(&data->pFrameRGB);
    av_free(data->pFrameRGB);

    // Free the YUV frame
    av_frame_free(&data->pFrame);
    av_free(data->pFrame);

    // Close the codecs
    avcodec_close(data->pCodecCtx);
    avcodec_close(data->pCodecCtxOrig);

    // Close the video file
    avformat_close_input(&data->pFormatCtx);
}

