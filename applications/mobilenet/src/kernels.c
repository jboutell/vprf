#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

// Copied straight out of kernels.h, just src and dst have been interchanged
#define RELU6(x) min(max(x, 0.0), 6.0)
#define KERNEL_DIM 3

#include <stdio.h>

void cpu_dwconv_gamma_beta(float* dst, float * src, float* krn, float* gamma, float* beta, float* _unused_m, float* _unused_v, int H, int W, int C, int _unused_F) {

	for (int h = 0; h < H; h++)
		for (int w = 0; w < W; w++) {

			for (int c = 0; c < C; c++) {

				float sum = 0.0;

				for (int k = 0; k < KERNEL_DIM; k++)
					for (int l = 0; l < KERNEL_DIM; l++)
						sum += (((w + (l - 1)) >= 0 && (w + (l - 1)) < W) && ((h + (k - 1)) >= 0 && (h + (k - 1)) < H)) ? src[H*W*c + h*W + w + (l - 1) + (k - 1)*W] * krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM] : 0.0;
				dst[h*W + w + H*W*c] = RELU6(gamma[c] * sum + beta[c]);
			}
		}
}

void cpu_dwconv_s2_gamma_beta(float* dst, float * src, float* krn, float* gamma, float* beta, float* _unused_m, float* _unused_v, int H, int W, int C, int _unused_F) {

	int even_s2 = (W % 2) == 0;

	if (even_s2) {

		for (int h = 1; h < H; h += 2)
			for (int w = 1; w < W; w += 2) {

				for (int c = 0; c < C; c++) {

					float sum = 0.0;

					for (int k = 0; k < KERNEL_DIM; k++)
						for (int l = 0; l < KERNEL_DIM; l++)
							sum += (((w + (l - 1)) >= 0 && (w + (l - 1)) < W) && ((h + (k - 1)) >= 0 && (h + (k - 1)) < H)) ? src[H*W*c + h*W + w + (l - 1) + (k - 1)*W] * krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM] : 0.0;

					dst[((h - 1)*W / 4) + (w - 1) / 2 + (H*W / 4)*c] = RELU6(gamma[c] * sum + beta[c]);
				}
			}
	}
	else {

		int sH = H / 2 + (H % 2 != 0), sW = W / 2 + (W % 2 != 0);
		for (int h = 0; h < H; h += 2) {
			for (int w = 0; w < W; w += 2) {

				for (int c = 0; c < C; c++) {

					float sum = 0.0;

					for (int k = 0; k < KERNEL_DIM; k++)
						for (int l = 0; l < KERNEL_DIM; l++)
							sum += (((w + (l - 1)) >= 0 && (w + (l - 1)) < W) && ((h + (k - 1)) >= 0 && (h + (k - 1)) < H)) ? src[H*W*c + h*W + w + (l - 1) + (k - 1)*W] * krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM] : 0.0;

					dst[(h*sW / 2) + w / 2 + (sH*sW)*c] = RELU6(gamma[c] * sum + beta[c]);
				}
			}
		}
	}
}

void cpu_matmul_beta_linear(float *dst, float *src, float *wgt, float* _unused_gamma, float*beta, float* _unused_m, float* _unused_v, int H, int W, int C, int F)
{
	int hA = F;
	int wA = C;
	int wB = H*W;

	for (unsigned int i = 0; i < hA; ++i)
		for (unsigned int j = 0; j < wB; ++j)
		{
			float sum = 0;

			for (unsigned int k = 0; k < wA; ++k)
			{
				float a = wgt[i * wA + k];
				float b = src[k * wB + j];
				sum += a * b;
			}

			int addr = i * wB + j;
			dst[addr] = (float)(sum + beta[i]);
		}
}

void cpu_matmul_beta(float *dst, float *src, float *wgt, float* _unused_gamma, float*beta, float* _unused_m, float* _unused_v, int H, int W, int C, int F)
{
	int hA = F;
	int wA = C;
	int wB = H*W;

	for (unsigned int i = 0; i < hA; ++i)
		for (unsigned int j = 0; j < wB; ++j)
		{
			float sum = 0;

			for (unsigned int k = 0; k < wA; ++k)
			{
				float a = wgt[i * wA + k];
				float b = src[k * wB + j];
				sum += a * b;
			}

			dst[i * wB + j] = RELU6((float)(sum + beta[i]));
		}
}

void cpu_conv_s2_beta(float* dst, float * src, float* krn, float* _unused_gamma, float* beta, float* _unused_m, float* _unused_v, int H, int W, int C, int F) {
	int even_s2 = ((W % 2) == 0);
	if (even_s2) {
		for (int f = 0; f < F; f++) {
			for (int h = 1; h < H; h += 2)
				for (int w = 1; w < W; w += 2) {
					float sum2 = 0.0;
					for (int c = 0; c < C; c++) {

						float sum = 0.0;

						for (int k = 0; k < KERNEL_DIM; k++)
							for (int l = 0; l < KERNEL_DIM; l++) {
								sum += (((w + (l - 1)) >= 0 && (w + (l - 1)) < W) && ((h + (k - 1)) >= 0 && (h + (k - 1)) < H)) ? src[H*W*c + h*W + w + (l - 1) + (k - 1)*W] * krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM + f * 3 * 3 * C] : 0.0;
							}
						sum2 += sum;
					}
					int addr = ((h - 1)*W / 4) + ((w - 1) / 2) + (H*W / 4)*f;
					dst[addr] = RELU6(sum2 + beta[f]);
				}
		}
	}
	else {
		int sH = H / 2 + (H % 2 != 0);
		int sW = W / 2 + (W % 2 != 0);

		for (int f = 0; f < F; f++) {
			for (int h = 0; h < H; h += 2)
				for (int w = 0; w < W; w += 2) {

					float sum2 = 0.0;
					for (int c = 0; c < C; c++) {

						for (int k = 0; k < KERNEL_DIM; k++)
							for (int l = 0; l < KERNEL_DIM; l++) {
								sum2 += (((w + (l - 1)) >= 0 && (w + (l - 1)) < W) && ((h + (k - 1)) >= 0 && (h + (k - 1)) < H)) ? src[H*W*c + h*W + w + (l - 1) + (k - 1)*W] * krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM + f * 3 * 3 * C] : 0.0;
							}
					}
					int addr = (h *sW / 2) + ((w) / 2) + (sH*sW)*f;
					dst[addr] = RELU6(sum2 + beta[f]);
				}
		}
	}
}

