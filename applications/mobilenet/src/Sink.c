#include <string.h>
#include <stdlib.h>
#include "dnn.h"
#include "ports.h"
#include "Sink.h"

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

void putPixel(float *image, int w, int x, int y, float color);
void drawDetections(char *fn, float *image, int *boxes, int w, int h, int num_boxes);
void drawLine(float *image, int w, int x0, int y0, int x1, int y1);

static char output_folder[] = "output/";

void SinkInit(Sink_data_t *data) {
	if (globalRepetitions == 0) {
		printf("Warning: number of repetitions uninitialized\n");
	} else {
		printf("Writing output images to folder %s\n", output_folder);
	}
	data->repetitions = globalRepetitions;
	data->iteration = 0;
}

void bigDot(float *frame, int w, int x, int y) {
	putPixel(frame, w, x-1, y-1, 0);
	putPixel(frame, w, x, y-1, 0);
	putPixel(frame, w, x+1, y-1, 0);

	putPixel(frame, w, x-1, y, 0);
	putPixel(frame, w, x, y, 0);
	putPixel(frame, w, x+1, y, 0);

	putPixel(frame, w, x-1, y+1, 0);
	putPixel(frame, w, x, y+1, 0);
	putPixel(frame, w, x+1, y+1, 0);
}

void *SinkFire(void *p) {
	Sink_data_t *data = (Sink_data_t *) p;
	if(data->iteration >= data->repetitions) {
		printf("Sink finished\n");
		globalApplicationFinished = 1;
		actorTerminate();
	} else {
		cl_int *boxes = (cl_int *) fifoReadStart(data->shared->inputs[SINK_TBOXES]);
		cl_int *scores = (cl_int *) fifoReadStart(data->shared->inputs[SINK_TSCORES]);
		cl_int *classes = (cl_int *) fifoReadStart(data->shared->inputs[SINK_TCLASSES]);
		cl_float *width = (cl_float *) fifoReadStart(data->shared->inputs[SINK_WIDTH]);
		cl_float *height = (cl_float *) fifoReadStart(data->shared->inputs[SINK_HEIGHT]);
		cl_float *frame = (cl_float *) fifoReadStart(data->shared->inputs[SINK_FRAME]);
		cl_float *count = (cl_float *) fifoReadStart(data->shared->inputs[SINK_COUNT]);

		for (int j = 0; j < count[0]; j++) {
			int cx = (boxes[4*j+3] + boxes[4*j+1])/2;
			int cy = (boxes[4*j+2] + boxes[4*j])/2;
			int x1 = cx;
			int y1 = cy;
			int x2 = cx + (scores[j]*5);
			int y2 = cy + (classes[j]*5);
			drawLine(frame, width[0], x1, y1, x2, y2);
		}

		char fileName[256];
		sprintf(fileName, "%sframe_%i.png", output_folder, data->iteration);
		drawDetections(fileName, frame, boxes, width[0], height[0], count[0]);	

		fifoReadEnd(data->shared->inputs[SINK_TBOXES]);
		fifoReadEnd(data->shared->inputs[SINK_TSCORES]);
		fifoReadEnd(data->shared->inputs[SINK_TCLASSES]);
		fifoReadEnd(data->shared->inputs[SINK_WIDTH]);
		fifoReadEnd(data->shared->inputs[SINK_HEIGHT]);
		fifoReadEnd(data->shared->inputs[SINK_FRAME]);
		fifoReadEnd(data->shared->inputs[SINK_COUNT]);
		data->iteration ++;
	}
	return p;
}

void SinkFinish(Sink_data_t *data) {
}
