#include <stdio.h>
#include "common.h"
#include <iostream>

typedef struct {
	int box[4];
	int cx;
	int cy;
	int type;
} object_data_t;

typedef struct {
	FILE *file;
	shared_t *shared;
	int iteration;
	int repetitions;
	object_data_t *objs;	
	object_data_t *cobjs;	
	int objcnt;
} Tracker_data_t;

void TrackerInit(Tracker_data_t *data);
void *TrackerFire(void *p);
void TrackerFinish(Tracker_data_t *data);
