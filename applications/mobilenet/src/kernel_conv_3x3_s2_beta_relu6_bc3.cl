#define C1 3
#define BATCH 4
#define KERNEL_DIM 3
#define KERNEL_RADIUS 1
#define FMAPS1 6
#define BLOCK_DIM_X 300
#define BLOCK_DIM_Y 1

#define RELU6(x) min(max(x, 0.0f), 6.0f)

__kernel void kernel_conv_3x3_s2_beta_relu6_bc3(__global float * src, __global float* dst1, __global float* dst2, __global float* dst3, __global float* krn,__global float* gamma, __global float* beta, __global float* m, __global float* v,
int H, int W, int C, int F) {

	int w = get_global_id(0) * 2;
	int h = get_global_id(1) * 2;
	int f = get_global_id(2);

	int even_s2 = ((W % 2) == 0);
	if (even_s2) {
		int sH = H / 2;
		int sW = W / 2;
		float sum = 0.0;
		for (int c = 0; c < C; c++) {
			for (int k = 0; k < KERNEL_DIM; k++)
				for (int l = 0; l < KERNEL_DIM; l++) {
					int cond = (((w + l) >= 0) && ((w + l) < W) && ((h + k) >= 0) && ((h + k) < H));
					float srcval = src[(H*c + h + k)*W + (w + l)];
					float krnval = krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM + f*KERNEL_DIM*KERNEL_DIM*C];
					sum += cond ? srcval*krnval : 0.0;
				}
		}
		float val = RELU6(sum + beta[f]);
		int addr = (h/2)*sW + (w/2) + (f*sW*sH);
		dst1[addr] = val;
		dst2[addr] = val;
		dst3[addr] = val;
	} else {
		int sH = H / 2 + (H % 2 != 0);
		int sW = W / 2 + (W % 2 != 0);
		float sum = 0.0;
		for (int c = 0; c < C; c++) {
			for (int k = 0; k < KERNEL_DIM; k++)
				for (int l = 0; l < KERNEL_DIM; l++) {
					int cond = (((w + l) > 0) && ((w + l) <= W) && ((h + k) > 0) && ((h + k) <= H));
					float srcval = src[(H*c + h + k - 1)*W + w + (l - 1)];
					float krnval = krn[k*KERNEL_DIM + l + c*KERNEL_DIM*KERNEL_DIM + f*KERNEL_DIM*KERNEL_DIM*C];
					sum += cond ? srcval*krnval : 0.0;
				}
		}
		float val = RELU6(sum + beta[f]);
		int addr = (h/2)*sW + (w/2) + (f*sH*sW);
		dst1[addr] = val;
		dst2[addr] = val;
		dst3[addr] = val;
	}
}

