#ifndef NonMaxSuppression_H
#define NonMaxSuppression_H

#include "common.h"
#include "dnn.h"

typedef struct {
	shared_t *shared;
} NonMaxSuppression_data_t;

void NonMaxSuppressionInit(NonMaxSuppression_data_t *data);
void *NonMaxSuppressionFire(void *p);
void NonMaxSuppressionFinish(NonMaxSuppression_data_t *data);
#endif

