#ifndef CombinePreds_H
#define CombinePreds_H

#include "common.h"
#include "dnn.h"

typedef struct {
	shared_t *shared;
} CombinePreds_data_t;

void CombinePredsInit(CombinePreds_data_t *data);
void *CombinePredsFire(void *p);
void CombinePredsFinish(CombinePreds_data_t *data);
#endif

