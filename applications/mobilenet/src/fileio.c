#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define COLOR 0.0f

void putPixel(float *image, int w, int x, int y, float color) {
	image[3*((w*y)+x)+0] = 0.9;
	image[3*((w*y)+x)+1] = 0.9;
	image[3*((w*y)+x)+2] = 0.9;
}

void drawLine0(float *image, int w, int x1, int y1, int x2, int y2) {
    int dx, dy, p, x, y;
 
	dx = x2 - x1;
	dy = y2 - y1;
 
	x = x1;
	y = y1;
 
	p = 2*dy - dx;
 
	while(x < x2) {
		if(p >= 0) {
			putPixel(image, w, x, y, COLOR);
			y = y + 1;
			p = p + 2*dy - 2*dx;
		} else {
			putPixel(image, w, x, y, COLOR);
			p = p + 2*dy;
		}
		x = x + 1;
	}
}

void drawLine(float *image, int w, int x1, int y1, int x2, int y2) {

	if (x1 > x2) {
		if (y1 > y2) {
			drawLine0(image, w, x2, y2, x1, y1);
		} else {
			drawLine0(image, w, x2, y1, x1, y2);
		}
	} else {
		if (y1 > y2) {
			drawLine0(image, w, x1, y2, x2, y1);
		} else {
			drawLine0(image, w, x1, y1, x2, y2);
		}
	}
}

void drawBox(float *image, int x1, int y1, int x2, int y2, int w, int h) {
	if (x1 < 0) x1 = 0;
	if (y1 < 0) y1 = 0;
	if (x2 > (w-1)) x2 = w-1;
	if (y2 > (h-1)) y2 = h-1;

	for (int i = x1; i <= x2; i++) {
		for (int c = 0; c < 3; c++) {
			image[3*((w*y1)+i)+c] = COLOR;
			image[3*((w*y2)+i)+c] = COLOR;
		}
	} 

	for (int j = y1; j <= y2; j++) {
		for (int c = 0; c < 3; c++) {
			image[3*((w*j)+x1)+c] = COLOR;
			image[3*((w*j)+x2)+c] = COLOR;
		}
	} 
}

void writeIntImage (char *dst, float *src, int len) {
	FILE *out = fopen(dst, "wb");
	if (out == NULL) {
		printf("could not open %s\n", dst);
		return;
	}
	for (int i = 0; i < len; i++) {
		float val = src[i];
		val *= 128.0;
		val += 128.0;
		fputc((int) val, out);
	}
	fclose(out);
} 

void drawDetections(char *fn, float *image, int *boxes, int w, int h, int num_boxes) {
	char buffer[256];
    for (int i = 0; i < num_boxes; i++) {
		drawBox(image, boxes[4*i+1], boxes[4*i], boxes[4*i+3], boxes[4*i+2], w, h);
	}
	writeIntImage ("detections.rgb", image, w*h*3);
	sprintf(buffer, "convert -depth 8 -interlace none -size %ix%i detections.rgb %s", w, h, fn);
	system(buffer);
	system("rm detections.rgb");
}

