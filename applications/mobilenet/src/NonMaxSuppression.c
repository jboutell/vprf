#include <math.h>
#include "ports.h"
#include "NonMaxSuppression.h"

#define SCORE_THRESHOLD -0.847297804 // 1/(1+exp(--0.847297804) = 0.300000011921
#define IOU_THRESHOLD 0.600000023842

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

float get_iou(float* a, float* b){

    float x1, x2, y1, y2, width, height;
    float epsilon = 1e-5;
    // COORDINATES OF THE INTERSECTION BOX
    x1 = max(a[0], b[0]);
    y1 = max(a[1], b[1]);
    x2 = min(a[2], b[2]);
    y2 = min(a[3], b[3]);

    // AREA OF OVERLAP - Area where the boxes intersect
    width = (x2 - x1);
    height = (y2 - y1);
    // handle case where there is NO overlap
    if ((width<0) || (height <0))
        return 0;
    float area_overlap = width * height;

    // COMBINED AREA
    float area_a = (a[2] - a[0]) * (a[3] - a[1]);
    float area_b = (b[2] - b[0]) * (b[3] - b[1]);
    float area_combined = area_a + area_b - area_overlap;

    // RATIO OF AREA OF OVERLAP OVER COMBINED AREA
    float iou = area_overlap / (area_combined+epsilon);

    return iou;// > IOU_THRESHOLD ? true : false;

}

void NonMaxSuppressionInit(NonMaxSuppression_data_t *data) {
}

void NonMaxSuppressionFinish(NonMaxSuppression_data_t *data) {
}

void *NonMaxSuppressionFire(void *p) {
	NonMaxSuppression_data_t *data = (NonMaxSuppression_data_t *) p;

	cl_float *preds = (cl_float *) fifoReadStart(data->shared->inputs[NONMAXSUPPRESSION_PREDS]);
	cl_float *ecd = (cl_float *) fifoReadStart(data->shared->inputs[NONMAXSUPPRESSION_ECDS]);

	int fi = 0;
    for (int c = 1; c < NUM_CLASSES; c++) {

		for (int a = 0; a < NUM_ANCHORS; a++) {
			int ia = a*NUM_CLASSES + c;

			if (preds[ia] > SCORE_THRESHOLD) {

				for (int b = 0; b < NUM_ANCHORS; b++) {
					int ib = b*NUM_CLASSES + c;

					if ((a != b) && (preds[ib] > SCORE_THRESHOLD)) {
						//float scorea = 1.0/(1.0+exp(-preds[ia]));
						//float scoreb = 1.0/(1.0+exp(-preds[ib]));

						//printf("Class %i: anchors:%i vs %i; inds:(%i vs %i); scores:[%.2f vs %.2f]\n", c, a, b, ia, ib, scorea, scoreb);
						float abox[] = {ecd[4*a], ecd[4*a+1], ecd[4*a+2], ecd[4*a+3]};
						float bbox[] = {ecd[4*b], ecd[4*b+1], ecd[4*b+2], ecd[4*b+3]};
						float iou = get_iou(abox, bbox);
					    if (iou > IOU_THRESHOLD) {

							// these predictions concern the same object
							if (preds[ia] > preds[ib]) { // object 'a' has a higher confidence than 'b'
								preds[ib] = -10.0;	// purge 'b'
/*
								printf("Class %i (iou:%0.2f): {%i}abox[%.1f,%.1f,%.1f,%.1f]:%.2f {%i}bbox[%.1f,%.1f,%.1f,%.1f]:%.2f -> b removed\n", c, iou,
									a, abox[1], abox[0], abox[3], abox[2], scorea,
									b, bbox[1], bbox[0], bbox[3], bbox[2], scoreb);
*/
							} else { // object 'b' has a higher confidence than 'a'
								preds[ia] = -10.0;	// purge 'a'
/*
								printf("Class %i (iou:%0.2f): {%i}abox[%.1f,%.1f,%.1f,%.1f]:%.2f {%i}bbox[%.1f,%.1f,%.1f,%.1f]:%.2f -> a removed\n", c, iou,
									a, abox[1], abox[0], abox[3], abox[2], scorea,
									b, bbox[1], bbox[0], bbox[3], bbox[2], scoreb);
*/
								break;
							}
						}
					}

				}
			}

		}
	}

	cl_float *boxes = (cl_float *) fifoWriteStart(data->shared->outputs[NONMAXSUPPRESSION_BOXES]);
	cl_float *scores = (cl_float *) fifoWriteStart(data->shared->outputs[NONMAXSUPPRESSION_SCORES]);
	cl_float *classes = (cl_float *) fifoWriteStart(data->shared->outputs[NONMAXSUPPRESSION_CLASSES]);
	cl_float *count_0 = (cl_float *) fifoWriteStart(data->shared->outputs[NONMAXSUPPRESSION_COUNT_0]);
	cl_float *count_1 = (cl_float *) fifoWriteStart(data->shared->outputs[NONMAXSUPPRESSION_COUNT_1]);

    for (int c = 1; c < NUM_CLASSES; c++) {
		for (int a = 0; a < NUM_ANCHORS; a++) {
			int i = a*NUM_CLASSES + c;
			if (preds[i] > SCORE_THRESHOLD) {
				boxes[4*fi] = ecd[4*a];
				boxes[4*fi+1] = ecd[4*a+1];
				boxes[4*fi+2] = ecd[4*a+2];
				boxes[4*fi+3] = ecd[4*a+3];
				scores[fi] = 1.0/(1.0+exp(-preds[i]));
				classes[fi] = c;

				fi++;
				if (fi == MAX_DETECTIONS) {
					goto skip;
				}
			}
		}
	}
	count_0[0] = fi;	
	count_1[0] = fi;	

skip:
	fifoReadEnd(data->shared->inputs[NONMAXSUPPRESSION_ECDS]);
	fifoReadEnd(data->shared->inputs[NONMAXSUPPRESSION_PREDS]);

	fifoWriteEnd(data->shared->outputs[NONMAXSUPPRESSION_BOXES]);
	fifoWriteEnd(data->shared->outputs[NONMAXSUPPRESSION_SCORES]);
	fifoWriteEnd(data->shared->outputs[NONMAXSUPPRESSION_CLASSES]);
	fifoWriteEnd(data->shared->outputs[NONMAXSUPPRESSION_COUNT_0]);
	fifoWriteEnd(data->shared->outputs[NONMAXSUPPRESSION_COUNT_1]);
	return p;
}

