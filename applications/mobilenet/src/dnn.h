#define NUM_ANCHORS 1917
#define NUM_CLASSES 91
#define MAX_DETECTIONS 100

void image_read(float *buffer);
void cpu_dwconv_gamma_beta(float* src, float * dst, float* krn, float* gamma, float* beta, float* _unused_m, float* _unused_v, int H, int W, int C, int _unused_F);
void cpu_dwconv_s2_gamma_beta(float* src, float * dst, float* krn, float* gamma, float* beta, float* _unused_m, float* _unused_v, int H, int W, int C, int _unused_F);
void cpu_matmul_beta_linear(float *src, float *dst, float *wgt, float* _unused_gamma, float*beta, float* _unused_m, float* _unused_v, int H, int W, int C, int F);
void cpu_matmul_beta(float *src, float *dst, float *wgt, float* _unused_gamma, float*beta, float* _unused_m, float* _unused_v, int H, int W, int C, int F);
void cpu_conv_s2_beta(float* src, float * dst, float* krn, float* _unused_gamma, float* beta, float* _unused_m, float* _unused_v, int H, int W, int C, int F);

