#include "poly.h"
#include "ports.h"

int polyInit(poly_data_t *data) {
	return 0;
}

void polyFinish(poly_data_t *data) {
}
	
void *polyFire(void *p) {
	poly_data_t *data = (poly_data_t *) p;

	int *n_c_buffer = (int *) fifoReadStart(data->shared->inputs[POLY_N_C]);
	int n_ind = n_c_buffer[0] >> 8;
	int c_ind = n_c_buffer[0] & 0xFF;
	fifoReadEnd(data->shared->inputs[POLY_N_C]);

	float polytab[NORM][TOKEN_SIZE];
	float abs_sqr_o;

	cl_float *iInput = (cl_float *) fifoReadStart(data->shared->inputs[POLY_I_IN]);
	cl_float *qInput = (cl_float *) fifoReadStart(data->shared->inputs[POLY_Q_IN]);

	for(int j = 0; j < TOKEN_SIZE; j++) {
		abs_sqr_o = iInput[j] * iInput[j] + qInput[j] * qInput[j];
		polytab[0][j] = 1.0;
		for(int i = 1; i < NORM; i ++) {
			polytab[i][j] = polytab[i - 1][j] * abs_sqr_o;
		}
	}

	float *iOutput1 = (float *) fifoWriteStart(data->shared->outputs[POLY_I_OUT1]);
	float *qOutput1 = (float *) fifoWriteStart(data->shared->outputs[POLY_Q_OUT1]);
	if (n_ind == 5) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[4*TOKEN_SIZE + j] = iInput[j] * polytab[4][j];
			qOutput1[4*TOKEN_SIZE + j] = qInput[j] * polytab[4][j];
		}
	}
	if (n_ind >= 4) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[3*TOKEN_SIZE + j] = iInput[j] * polytab[3][j];
			qOutput1[3*TOKEN_SIZE + j] = qInput[j] * polytab[3][j];
		}
	}
	if (n_ind >= 3) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[2*TOKEN_SIZE + j] = iInput[j] * polytab[2][j];
			qOutput1[2*TOKEN_SIZE + j] = qInput[j] * polytab[2][j];
		}
	}
	if (n_ind >= 2) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[1*TOKEN_SIZE + j] = iInput[j] * polytab[1][j];
			qOutput1[1*TOKEN_SIZE + j] = qInput[j] * polytab[1][j];
		}
	}
	if (n_ind >= 1) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[0*TOKEN_SIZE + j] = iInput[j] * polytab[0][j];
			qOutput1[0*TOKEN_SIZE + j] = qInput[j] * polytab[0][j];
		}
	}

	if (c_ind == 5) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[9*TOKEN_SIZE + j] = iInput[j] * polytab[4][j];
			qOutput1[9*TOKEN_SIZE + j] = -(qInput[j] * polytab[4][j]);
		}
	}
	if (c_ind >= 4) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[8*TOKEN_SIZE + j] = iInput[j] * polytab[3][j];
			qOutput1[8*TOKEN_SIZE + j] = -(qInput[j] * polytab[3][j]);
		}
	}
	if (c_ind >= 3) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[7*TOKEN_SIZE + j] = iInput[j] * polytab[2][j];
			qOutput1[7*TOKEN_SIZE + j] = -(qInput[j] * polytab[2][j]);
		}
	}
	if (c_ind >= 2) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[6*TOKEN_SIZE + j] = iInput[j] * polytab[1][j];
			qOutput1[6*TOKEN_SIZE + j] = -(qInput[j] * polytab[1][j]);
		}
	}
	if (c_ind >= 1) {
		for(int j = 0; j < TOKEN_SIZE; j++) {
			iOutput1[5*TOKEN_SIZE + j] = iInput[j] * polytab[0][j];
			qOutput1[5*TOKEN_SIZE + j] = -(qInput[j] * polytab[0][j]);
		}
	}

	fifoWriteEnd(data->shared->outputs[POLY_I_OUT1]);
	fifoWriteEnd(data->shared->outputs[POLY_Q_OUT1]);
	fifoReadEnd(data->shared->inputs[POLY_I_IN]);
	fifoReadEnd(data->shared->inputs[POLY_Q_IN]);

	return p;
}
