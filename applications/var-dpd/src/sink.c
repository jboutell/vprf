#include "sink.h"

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

int sinkInit(sink_data_t *data) {
	data->file = NULL;
	data->count = 0;
	data->length = TOKEN_SIZE - (NUM_TAPS-1);

	char sourceFileName[256];
	sprintf(sourceFileName, "%s.bin", data->fn);
	data->file = fopen(sourceFileName, "wb");
	if (!data->file) {
		printf("Error: could not open output file %s\n", sourceFileName);
	}

	if (globalRepetitions == 0) {
		printf("Warning: number of repetitions is uninitialized\n");
	}
	data->repetitions = globalRepetitions;
	data->iteration = 0;
	// FIXME: what should this function return
	return 0;
}

void *sinkFire(void *p) {
	sink_data_t *data = (sink_data_t *) p;
	if(data->iteration == data->repetitions) {
		//printf("Sink finished\n");
		globalApplicationFinished = 1;
	} else {
		fifo_t *input = data->shared->inputs[0];
		cl_float *token = (cl_float *) fifoReadStart(input);
		fwrite(token, sizeof(cl_float), data->length, data->file);
		fifoReadEnd(input);
		data->iteration ++;
	}
	return p;
}

void sinkFinish(sink_data_t *data) {
	if (data->file != NULL) {
		fflush(data->file);
		fclose(data->file);
		data->file = NULL;
	}
}
