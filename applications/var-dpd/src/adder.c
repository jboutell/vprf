#include "adder.h"
#include "ports.h"

#define DPD_Adder_ic_lo -0.004622196778655052
#define DPD_Adder_qc_lo -0.004829585552215576

int adderInit(adder_data_t *data) {
	return 0;
}

void adderFinish(adder_data_t *data) {
}
	
void* adderFire(void *p) {
	adder_data_t *data = (adder_data_t *) p;

	int *n_c_buffer = (int *) fifoReadStart(data->shared->inputs[ADDER_N_C]);
	int n_ind = n_c_buffer[0] >> 8;
	int c_ind = n_c_buffer[0] & 0xFF;
	fifoReadEnd(data->shared->inputs[ADDER_N_C]);

	float ivect_tmp[VALID_DATA_SIZE];
	float qvect_tmp[VALID_DATA_SIZE];

	for(int j = 0; j < VALID_DATA_SIZE; j ++) {
		ivect_tmp[j] = DPD_Adder_ic_lo;
		qvect_tmp[j] = DPD_Adder_qc_lo;
	}

	float *iInput1 = (float *) fifoReadStart(data->shared->inputs[ADDER_I_IN1]);
	float *qInput1 = (float *) fifoReadStart(data->shared->inputs[ADDER_Q_IN1]);

	if (n_ind == 5) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+4*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+4*TOKEN_SIZE];
		}
	}
	if (n_ind >= 4) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+3*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+3*TOKEN_SIZE];
		}
	}
	if (n_ind >= 3) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+2*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+2*TOKEN_SIZE];
		}
	}
	if (n_ind >= 2) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+1*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+1*TOKEN_SIZE];
		}
	}
	if (n_ind >= 1) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+0*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+0*TOKEN_SIZE];
		}
	}

	if (c_ind == 5) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+9*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+9*TOKEN_SIZE];
		}
	}
	if (c_ind >= 4) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+8*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+8*TOKEN_SIZE];
		}
	}
	if (c_ind >= 3) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+7*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+7*TOKEN_SIZE];
		}
	}
	if (c_ind >= 2) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+6*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+6*TOKEN_SIZE];
		}
	}
	if (c_ind >= 1) {
		for(int j = 0; j < VALID_DATA_SIZE; j ++) {
			ivect_tmp[j] += iInput1[j+5*TOKEN_SIZE];
			qvect_tmp[j] += qInput1[j+5*TOKEN_SIZE];
		}
	}
	fifoReadEnd(data->shared->inputs[ADDER_I_IN1]);
	fifoReadEnd(data->shared->inputs[ADDER_Q_IN1]);

	float *iOutput = (float *) fifoWriteStart(data->shared->outputs[ADDER_I_OUT]);
	float *qOutput = (float *) fifoWriteStart(data->shared->outputs[ADDER_Q_OUT]);
	for(int j = 0; j < VALID_DATA_SIZE; j ++) {
		iOutput[j] = ivect_tmp[j];
		qOutput[j] = qvect_tmp[j];
	}
	fifoWriteEnd(data->shared->outputs[ADDER_I_OUT]);
	fifoWriteEnd(data->shared->outputs[ADDER_Q_OUT]);
	return p;
}
