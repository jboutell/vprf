/****************************************************************************\

    OpenCL kernels for combined convolution+relu operations
    author: Jani Boutellier, Tampere Univ. of Technology

    Adapted from LIDE-C implementation written by Renjie Xie
    Described in "Resource-Constrained Implementation and Optimization
      of a Deep Neural Network for Vehicle Classification"
    by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
    EUSIPCO 2016

    v. 1.1 / Jan 05, 2017

\****************************************************************************/

#include "dnn.h"
//#include "l2wgt.h"
#include "conv_relu_l2.h"
#ifdef DBGPRINT
	#include <stdio.h>
#endif

int conv_relu_l2Init(conv_relu_l2_data_t *data) {
	char fileName[256];
	sprintf(fileName, "data/conv2_update.bin");
	readRawData (fileName, (void **) &data->wgt);
	return 0;
}

void conv_relu_l2Finish(conv_relu_l2_data_t *data) {
	free (data->wgt);
}


void* conv_relu_l2Fire (void *p) {
	const int nInWidth = PATCH2 + (2 * PAD_NUM);
	const int nWidth = PATCH2;
	const int nHeight = PATCH2;
 	const int nFilterWidth = CONV2SIZE;

	conv_relu_l2_data_t *data = (conv_relu_l2_data_t *) p;
	cl_float *fifo_in = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *fifo_out = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor conv_relu_l2 fires\n");
	#endif

	for (int fm = 0; fm < FM_COUNT; fm++) {
	for (int row = 0; row < nHeight; row += PAD_NUM) {
	for (int col = 0; col < nWidth; col += PAD_NUM) {

	float gssample[PAD_NUM*PAD_NUM] = {0};
	for (int c = 0; c < FM_COUNT; c++) {
		for (int rst = 0; rst < PAD_NUM; rst ++) {
			for (int cst = 0; cst < PAD_NUM; cst ++) {

				for (int i = 0; i < nFilterWidth; i++) {
					for (int j = 0; j < nFilterWidth; j++) {
						gssample[rst*PAD_NUM + cst] +=
							matrix1(fifo_in, (c*PATCH2SQPAD), nInWidth, (row+rst+i), (col+cst+j)) * 
							matrix1(data->wgt, (CONV2SQ * FM_COUNT * fm + c * CONV2SQ), nFilterWidth, (nFilterWidth-1-i), (nFilterWidth-1-j));
					}
				}

			}
		}
	}

	float tmp = 0;
	tmp = tmp > gssample[0] ? tmp : gssample[0];
	tmp = tmp > gssample[1] ? tmp : gssample[1];
	tmp = tmp > gssample[2] ? tmp : gssample[2];
	tmp = tmp > gssample[3] ? tmp : gssample[3];
    matrix1(fifo_out, fm*PATCH3SQ, (PATCH2/TILE_NUM), (row/2), (col/2)) = tmp;

	} // col
	} // row
	} // fm

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}

