#include "dnn.h"
#include "source.h"
#include <string.h>

extern volatile int globalRepetitions;

void sourceInit(source_data_t *data) {
	data->file = NULL;
	char sourceFileName[256];
	sprintf(sourceFileName, "data/c.bin");
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
	globalRepetitions = getFileSize(data->file) / (CHANNELS * PATCH1SQ * sizeof(float));
	data->iteration = 0;
	//printf("Running for %i images\n", globalRepetitions);
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		//printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[0]);
		#ifdef DBGPRINT
		printf("actor source fires\n");
		#endif
		// read image and add PAD_NUM padding on each side of image
		for (int c = 0; c < CHANNELS; c++) {
			cl_float *fifop = &wbufout1[c*PATCH1SQPAD];
			memset(fifop, 0, 2*(PATCH1 + 2*PAD_NUM)*sizeof(cl_float));
			for (int i = PAD_NUM; i < (PATCH1 + PAD_NUM); i++) {
				memset(&fifop[i * (PATCH1+2*PAD_NUM)], 0, PAD_NUM*sizeof(cl_float));
				int retval = fread(&fifop[i * (PATCH1+2*PAD_NUM) + PAD_NUM], sizeof(cl_float), PATCH1, data->file);
				if (retval != PATCH1) {
					printf("Source %s depleted\n", data->fn);
				}
				memset(&fifop[i * (PATCH1+2*PAD_NUM) + PAD_NUM + PATCH1], 0, PAD_NUM*sizeof(cl_float));
			}
			memset(&fifop[(PATCH1 + PAD_NUM) * (PATCH1+2*PAD_NUM)], 0, 2*(PATCH1 + 2*PAD_NUM)*sizeof(cl_float));
		}
		fifoWriteEnd(data->shared->outputs[0]);
		data->iteration ++;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

