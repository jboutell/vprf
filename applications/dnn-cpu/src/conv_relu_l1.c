/****************************************************************************\

    OpenCL kernels for combined convolution+relu operations
    author: Jani Boutellier, Tampere Univ. of Technology

    Adapted from LIDE-C implementation written by Renjie Xie
    Described in "Resource-Constrained Implementation and Optimization
      of a Deep Neural Network for Vehicle Classification"
    by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
    EUSIPCO 2016

    v. 1.1 / Jan 05, 2017

\****************************************************************************/

#include "dnn.h"
//#include "l1wgt.h"
#include "conv_relu_l1.h"
#include <string.h>
#ifdef DBGPRINT
	#include <stdio.h>
#endif

int conv_relu_l1Init(conv_relu_l1_data_t *data) {
	char fileName[256];
	sprintf(fileName, "data/conv1_update.bin");
	readRawData (fileName, (void **) &data->wgt);
	return 0;
}

void conv_relu_l1Finish(conv_relu_l1_data_t *data) {
	free (data->wgt);
}


void* conv_relu_l1Fire (void *p) {
	const int nInWidth = PATCH1 + (2 * PAD_NUM);
	const int nWidth = PATCH1;
	const int nHeight = PATCH1;
 	const int nFilterWidth = CONV1SIZE;

	conv_relu_l1_data_t *data = (conv_relu_l1_data_t *) p;
	cl_float *fifo_in = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *fifo_out = (cl_float *) fifoWriteStart(data->shared->outputs[0]);
	#ifdef DBGPRINT
	printf("actor conv_relu_l1 fires\n");
	#endif

	for (int fm = 0; fm < FM_COUNT; fm++) {

		memset (&fifo_out[fm*PATCH2SQPAD], 0, 2*(PATCH2+2*PAD_NUM)*sizeof(float));

		for (int row = 0; row < nHeight; row += PAD_NUM) {
			for (int col = 0; col < PAD_NUM; col ++) {
				fifo_out[fm*PATCH2SQPAD + ((row/2)+PAD_NUM)*(PATCH2+2*PAD_NUM) + col] = 0.0;
			}
			for (int col = 0; col < nWidth; col += PAD_NUM) {

				float gssample[PAD_NUM*PAD_NUM] = {0};
				for (int c = 0; c < CHANNELS; c++) {
					for (int rst = 0; rst < PAD_NUM; rst ++) {
						for (int cst = 0; cst < PAD_NUM; cst ++) {

							for (int i = 0; i < nFilterWidth; i++) {
								for (int j = 0; j < nFilterWidth; j++) {
									gssample[rst*PAD_NUM + cst] +=
										matrix1(fifo_in, (c*PATCH1SQPAD), nInWidth, (row+rst+i), (col+cst+j)) * 
										matrix1(data->wgt, (CONV1SQ * CHANNELS * fm + c * CONV1SQ), nFilterWidth, (nFilterWidth-1-i), (nFilterWidth-1-j));
								}
							}

						}
					}
				}

				float tmp = 0;
				tmp = tmp > gssample[0] ? tmp : gssample[0];
				tmp = tmp > gssample[1] ? tmp : gssample[1];
				tmp = tmp > gssample[2] ? tmp : gssample[2];
				tmp = tmp > gssample[3] ? tmp : gssample[3];
				matrix1(fifo_out, (fm*PATCH2SQPAD), (PATCH2+2*PAD_NUM), ((row/2)+PAD_NUM), ((col/2)+PAD_NUM)) = tmp;
			} // col
			for (int col = PATCH2+PAD_NUM; col < PATCH2+2*PAD_NUM; col ++) {
				fifo_out[fm*PATCH2SQPAD + ((row/2)+PAD_NUM)*(PATCH2+2*PAD_NUM) + col] = 0.0;
			}
		} // row
	
		memset (&fifo_out[fm*PATCH2SQPAD+(PATCH2+PAD_NUM)*(PATCH2+2*PAD_NUM)], 0, 2*(PATCH2+2*PAD_NUM)*sizeof(float));
	} // fm

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}

