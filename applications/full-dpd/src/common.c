#include "dpd.h"

complexe inicompl()
{
	complexe res;
	res.real=0.0f;
	res.imag=0.0f;
	return(res);
}

void read_complex(FILE *file, complexe *signal, int len) {
	//FILE *file = fopen(fn, "rb");
	if (file != NULL) {
		int retval = fread(signal, 1, len, file);
		if (retval != len) {
			printf("%i instead of %i samples acquired from file\n", retval, len);
		}
		//fclose(file);
	} else {
		printf("error opening file\n");
	}
}

void write_complex(FILE *file, complexe* signal, int len) {
	//FILE *file = fopen(fn, "wb");
	if (file != NULL) {
		fwrite(signal, len, 1, file);
		//fclose(file);
	} else {
		printf("error opening file\n");
	}
}

