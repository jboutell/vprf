#ifndef PARAMS_H
#define PARAMS_H

#define P	9 						// polynomial order; allowed values are {5,7,9,11}
#define M	1 						// memory depth - M+1 taps per order; allowed values between 0-3
#define L	((1+P)>>1)*(M+1)		// Evaluates to 10 when P=9 and M=1
#define BLOCKLENGTH	10000


typedef struct {
	float real;
	float imag;
} complexe;

#endif
