/*
 * main.c
 *
 *  Created on: 23 Mar 2018
 *      Author: meirhaeg
 */

 /**
 *	Digital predistortion with closed-loop learning, utilizing the
 * decorrelating/block LMS learning rule
 *
 * Example script
 *
 * Lauri Anttila / TUT
 * 2018-03-22
 *
 **/
#include "dpd.h"
#include "dpd_update.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "coeffs.h" 				// orthogonalization matrix Q is defined here
#include "ports.h"
#include "profile.h"

extern volatile int globalRepetitions;

//void orthogonalize(complexe *dpd_input, complexe *input, complexe *last, int varLength) {
void compute_DPD_Update(complexe *W,/* complexe *dpd_input,*/ complexe *input, complexe *last, complexe *conj_ErrorSignal, int varLength) {
	const int lim = (1+P)>>1;
	complexe Y[L];
	for(int j=0;j<lim;j++)
	{
		Y[j].real=last[j].real;
		Y[j].imag=last[j].imag;
	}
	float sum_DPD_update = 0.0;
	float LearningRate = 0.5;
		complexe Correlation[L-1];
	for(int j = 0; j < (L-1); j++) {
		Correlation[j] = inicompl();
	}

	for(int i=0;i<varLength;i++)
	{
		for(int k = 0; k < lim; k++)
		{
			Y[k+lim].real=Y[k].real;
			Y[k+lim].imag=Y[k].imag;
		}

		float squared_input_real = input[i].real * input[i].real;
		float squared_input_imag = input[i].imag * input[i].imag;
		float squared_input_sum = squared_input_real + squared_input_imag;

		Y[0].real = input[i].real;
		Y[0].imag = input[i].imag;

		float powabssignal = 1.0f;
		for(int k = 1; k < lim; k++) {
			powabssignal = powabssignal*squared_input_sum;
			Y[k].real = input[i].real * powabssignal;
			Y[k].imag = input[i].imag * powabssignal;
		}

		float srms = 0.0;
		for(int j = 0; j < L-1; j++) {
			complexe sum_Orth = inicompl();
			for(int k = 0; k < j+2; k++) {
				sum_Orth.real = sum_Orth.real + Y[k].real * Q[(j+1)*L+k];
				sum_Orth.imag = sum_Orth.imag + Y[k].imag * Q[(j+1)*L+k];
			}
			//sum_Orth.real = sum_Orth.real;
			//sum_Orth.imag = sum_Orth.imag;
//		}
	//}
//}

//void compute_DPD_Update(complexe *W, complexe *dpd_input, complexe *conj_ErrorSignal, int varLength) {

	//for(int i = 0; i < varLength; i++) {
//		for(int j = 0; j < (L-1); j++) {
			Correlation[j].real += sum_Orth.real * conj_ErrorSignal[i].real
								  - sum_Orth.imag * conj_ErrorSignal[i].imag;

			Correlation[j].imag += sum_Orth.real * conj_ErrorSignal[i].imag
								  + sum_Orth.imag * conj_ErrorSignal[i].real;

			srms += sum_Orth.real * sum_Orth.real
				  + sum_Orth.imag * sum_Orth.imag;
		}
		sum_DPD_update += sqrt(srms/(L-1));
	}

	for(int i=0;i<lim;i++)
	{
		last[i].real = Y[i].real;
		last[i].imag = Y[i].imag;
	}
	
	for(int i = 0; i < (L-1); i++) {
		W[i].real = W[i].real - (Correlation[i].real * LearningRate / sum_DPD_update);
		W[i].imag = W[i].imag - (Correlation[i].imag * LearningRate / sum_DPD_update);
	}
}

/*
void compute_DPD_Update(complexe *W, complexe *Wdiff) {
	for(int i = 0; i < (L-1); i++) {
	}
}*/

void dpd_updateInit(dpd_update_data_t *data) {
	data->iteration = 0;
	//data->dpd_input = (complexe *) malloc (sizeof(complexe) * L * BLOCKLENGTH);
	memset(data->last, 0, 10*sizeof(complexe));
	for(int i = 0; i < (L-1); i++) {
		data->W[i] = inicompl();
	}
}

void dpd_updateFinish(dpd_update_data_t *data) {
	//free (data->dpd_input);
}

void *dpd_updateFire(void *p) {

	dpd_update_data_t *data = (dpd_update_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		//actorTerminate();
	} else if (data->iteration == 0) {
		fifo_t *fifo_weigths_i = data->shared->outputs[DPD_UPDATE_WEIGHTS_I];
		fifo_t *fifo_weigths_q = data->shared->outputs[DPD_UPDATE_WEIGHTS_Q];
		float *token_weigths_i = (float *) fifoWriteStart(fifo_weigths_i);
		float *token_weigths_q = (float *) fifoWriteStart(fifo_weigths_q);
		for (int i = 0; i < L-1; i++) {
			token_weigths_i[i] = data->W[i].real;
			token_weigths_q[i] = data->W[i].imag;
		}
		fifoWriteEnd(fifo_weigths_i);
		fifoWriteEnd(fifo_weigths_q);
		data->iteration ++;
	} else {
		fifo_t *fifo_conf = data->shared->inputs[DPD_UPDATE_CONF];
		int *token_conf = (int *) fifoReadStart(fifo_conf);
		fifoReadEnd(fifo_conf);

		int varLength = token_conf[0];
		fifo_t *fifo_stacksignal = data->shared->inputs[DPD_UPDATE_STACKSIGNAL];
		complexe *token_stacksignal = (complexe *) fifoReadStart(fifo_stacksignal);
		fifo_t *fifo_errorsignal = data->shared->inputs[DPD_UPDATE_ERRORSIGNAL];
		complexe *token_errorsignal = (complexe *) fifoReadStart(fifo_errorsignal);
//		orthogonalize(data->dpd_input, &token_stacksignal[1], data->last, varLength);
		compute_DPD_Update(data->W, /*data->dpd_input,*/ &token_stacksignal[1], data->last, token_errorsignal, varLength);
		fifoReadEnd(fifo_stacksignal);
		fifoReadEnd(fifo_errorsignal);

		fifo_t *fifo_weigths_i = data->shared->outputs[DPD_UPDATE_WEIGHTS_I];
		fifo_t *fifo_weigths_q = data->shared->outputs[DPD_UPDATE_WEIGHTS_Q];
		float *token_weigths_i = (float *) fifoWriteStart(fifo_weigths_i);
		float *token_weigths_q = (float *) fifoWriteStart(fifo_weigths_q);

		for (int i = 0; i < L-1; i++) {
			token_weigths_i[i] = data->W[i].real;
			token_weigths_q[i] = data->W[i].imag;
		}

		fifoWriteEnd(fifo_weigths_i);
		fifoWriteEnd(fifo_weigths_q);

		data->iteration ++;
	}
	return p;
}


