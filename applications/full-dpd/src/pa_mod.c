#include "dpd.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ports.h"
#include "pa_mod.h"
#include "profile.h"

extern volatile int globalRepetitions;
extern volatile int globalApplicationFinished;

// This program computes the SNR of a complex-valued signal
// Equal to the following MATLAB code:
/*
	noise_i = noisy_i-ref_i;
	noise_q = noisy_q-ref_q;
	noise = noise_i + 1i*noise_q;
	ref = ref_i + 1i*ref_q;
	p_ref = sum(abs(ref).^2);
	p_noise = sum(abs(noise).^2);
	snr = 10*log10(p_ref/p_noise);
*/

void snr_iq(char *argv1, float *signal_i, float *signal_q) {
	int done = 0, cnt = 0;
	FILE *rf = fopen(argv1, "rb");
	if (rf == NULL) {
		printf("could not open %s\n", argv1);
		return;
	}
	float noise = 0.0;
	float ref = 0.0;
	float nval = 0.0;

	while (!done) {
		int retval1, retval2;
		float nival, nqval, rival, rqval;
		retval1 = fread(&rival, sizeof(float), 1, rf);
		retval2 = fread(&rqval, sizeof(float), 1, rf);
		if (retval1 != 1 || retval2 != 1) {
			done = 1;
			break;
		}
		nival = signal_i[cnt];
		nqval = signal_q[cnt++];

		nval += nival*nival + nqval*nqval;		

		nival -= rival;		
		nqval -= rqval;	

		noise += nival*nival + nqval*nqval;		
		ref += rival*rival + rqval*rqval;		
	}

	printf("SNR of %i [%.1f %.1f] samples is %3.2f dB\n", cnt, nval, ref, 10.0*log10(ref/noise));

	fclose(rf);
}

void read_complex(char *fn, complexe *signal, int len) {
	FILE *file = fopen(fn, "rb");
	if (file != NULL) {
		int retval = fread(signal, 1, len, file);
		if (retval != len) {
			printf("%i instead of %i samples acquired from %s\n", retval, len, fn);
		}
		fclose(file);
	} else {
		printf("error opening file %s\n", fn);
	}
}

void pa_modInit(pa_mod_data_t *data) {
	data->file = NULL;
	data->count = 0;
	data->sample_mem.real = 0.0f;
	data->sample_mem.imag = 0.0f;
	data->length = BLOCKLENGTH;
	globalRepetitions = ITER_MAX;//getFileSize(data->file) / (sizeof(complexe) * data->length);
	data->iteration = 0;
}

void pa_modFinish(pa_mod_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

void *pa_modFire(void *p) {
	int varLength = BLOCKLENGTH;
	pa_mod_data_t *data = (pa_mod_data_t *) p;
	char buffer1[256];
	fifo_t *fifo_dpd_output_i = data->shared->inputs[PA_MOD_DPD_OUTPUT_I];
	fifo_t *fifo_dpd_output_q = data->shared->inputs[PA_MOD_DPD_OUTPUT_Q];

	complexe input_signal[BLOCKLENGTH+1];

	if (data->iteration > 0) {
		fifo_t *fifo_conf = data->shared->inputs[PA_MOD_CONF];
		int *token_conf = (int *) fifoReadStart(fifo_conf);
		varLength = token_conf[0];
		fifoReadEnd(fifo_conf);
	}

	sprintf(buffer1,"input/signal_%i.bin", data->iteration);
	read_complex(buffer1, &input_signal[1], sizeof(complexe)*BLOCKLENGTH);

	fifo_t *fifo_stacksignal1 = data->shared->outputs[PA_MOD_STACKSIGNAL1];
	complexe *token_stacksignal1 = (complexe *) fifoWriteStart(fifo_stacksignal1);
	fifo_t *fifo_stacksignal2 = data->shared->outputs[PA_MOD_STACKSIGNAL2];
	complexe *token_stacksignal2 = (complexe *) fifoWriteStart(fifo_stacksignal2);
	fifo_t *fifo_dpd_input2_i = data->shared->outputs[PA_MOD_DPD_INPUT2_I];
	cl_float *token_dpd_input2_i = (cl_float *) fifoWriteStart(fifo_dpd_input2_i);
	fifo_t *fifo_dpd_input2_q = data->shared->outputs[PA_MOD_DPD_INPUT2_Q];
	cl_float *token_dpd_input2_q = (cl_float *) fifoWriteStart(fifo_dpd_input2_q);

	input_signal[0] = data->sample_mem;
	for (int i = 0; i < BLOCKLENGTH+1; i++) {
		token_dpd_input2_i[i] = input_signal[i].real;
		token_dpd_input2_q[i] = input_signal[i].imag;
	}
	for (int i = 0; i < varLength+1; i++) {
		token_stacksignal1[i] = input_signal[i];
		token_stacksignal2[i] = input_signal[i];
	}

	fifoWriteEnd(fifo_stacksignal1);
	fifoWriteEnd(fifo_stacksignal2);
	fifoWriteEnd(fifo_dpd_input2_i);
	fifoWriteEnd(fifo_dpd_input2_q);

	data->sample_mem = input_signal[BLOCKLENGTH];

	if (data->iteration > 0) {
		sprintf(buffer1,"output/ref/DPD_Output_%i.bin", data->count);
		float *token_dpd_output_i = (float *) fifoReadStart(fifo_dpd_output_i);
		float *token_dpd_output_q = (float *) fifoReadStart(fifo_dpd_output_q);
		if (SNR) {
			printf("%2i: ", data->count+1);
			snr_iq(buffer1, token_dpd_output_i, token_dpd_output_q); 
		}
		if (MATLAB) { // for MATLAB we need to write the DPD output to the file temp/dpd_out.bin
			FILE *out = fopen("temp/dpd_out.bin", "wb");
			fwrite(token_dpd_output_i, BLOCKLENGTH, sizeof(float), out);
			fwrite(token_dpd_output_q, BLOCKLENGTH, sizeof(float), out);
			fclose (out);
		}

		fifoReadEnd(fifo_dpd_output_i);
		fifoReadEnd(fifo_dpd_output_q);

		fifo_t *fifo_pa_output = data->shared->outputs[PA_MOD_PA_OUTPUT];
		complexe *token_pa_output = (complexe *) fifoWriteStart(fifo_pa_output);
		if(!MATLAB) { // without MATLAB, we produce a fixed PA output irrespective of PA input
			sprintf(buffer1,"pa-output/PA_Output_%i.bin", data->count);
			read_complex(buffer1, token_pa_output, sizeof(complexe)*varLength);
		} else {
			char cmdBuf[256];
			sprintf(cmdBuf, "matlab -nojvm -nosplash -nodisplay -r \"PA_mod(%i, %i, %i);exit\"", 0, 3, BLOCKLENGTH);
			system(cmdBuf); // matlab reads file temp/dpd_out.bin and produces file temp/PA_Output.bin
			FILE *in = fopen("temp/PA_Output.bin", "rb");
			for(int i = 0; i < varLength; i++) {
				float val;
				fread(&val, 1, sizeof(float), in);
				token_pa_output[i].real = val;
			}
			for(int i = 0; i < varLength; i++) {
				float val;
				fread(&val, 1, sizeof(float), in);
				token_pa_output[i].imag = val;
			}
			fclose (in);
		}
		fifoWriteEnd(fifo_pa_output);
		data->count++;
	}

	data->iteration ++;
	
	if(data->iteration > globalRepetitions) {
		globalApplicationFinished = 1;
		//actorTerminate();
	}

	return p;
}

