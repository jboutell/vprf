/*
 * main.c
 *
 *  Created on: 23 Mar 2018
 *      Author: meirhaeg
 */

 /**
 *	Digital predistortion with closed-loop learning, utilizing the
 * decorrelating/block LMS learning rule
 *
 * Example script
 *
 * Lauri Anttila / TUT
 * 2018-03-22
 *
 **/
#include "dpd.h"
#include "dpd_learn.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "ports.h"
#include "profile.h"

extern volatile int globalRepetitions;

void scale_est(complexe *ScaleEst, complexe *PA_Output, complexe *stacksignal, int varLength) {
	complexe sum_calc_err_tmp=inicompl();
	complexe sum_calc_err=inicompl();
	complexe conj_PA_Output;
	for(int i=0;i<varLength;i++)
	{
		float ppareal;
		float ppaimag;
		float ppareag;
		float ppaimal;
		float pspareal;
		float pspaimag;
		float pspareag;
		float pspaimal;
		complexe multpa;
		complexe multspa;

		conj_PA_Output.real=PA_Output[i].real;
		conj_PA_Output.imag=-PA_Output[i].imag;			/** Calculate error signal **/
		// First, calculate amplitude and phase correction coefficireal for the measured signal:
		ppareal=conj_PA_Output.real*PA_Output[i].real;
		ppaimag=conj_PA_Output.imag*PA_Output[i].imag;
		ppareag=conj_PA_Output.real*PA_Output[i].imag;
		ppaimal=conj_PA_Output.imag*PA_Output[i].real;
		pspareal=conj_PA_Output.real*stacksignal[i].real;
		pspaimag=conj_PA_Output.imag*stacksignal[i].imag;
		pspareag=conj_PA_Output.real*stacksignal[i].imag;
		pspaimal=conj_PA_Output.imag*stacksignal[i].real;
		multpa.real=ppareal-ppaimag;
		multpa.imag=ppareag+ppaimal;
		multspa.real=pspareal-pspaimag;
		multspa.imag=pspareag+pspaimal;
		sum_calc_err_tmp.real=sum_calc_err_tmp.real+multpa.real;
		sum_calc_err_tmp.imag=sum_calc_err_tmp.imag+multpa.imag;
		sum_calc_err.real=sum_calc_err.real+multspa.real;
		sum_calc_err.imag=sum_calc_err.imag+multspa.imag;
	}
	ScaleEst[0].real=sum_calc_err.real/sum_calc_err_tmp.real;
	ScaleEst[0].imag=sum_calc_err.imag/sum_calc_err_tmp.real;
}

void compute_ErrorSignal(complexe *conj_ErrorSignal, complexe *ScaleEst, complexe *PA_Output, complexe *stacksignal, int varLength) {
	complexe ErrorSignal;
	for(int i=0;i<varLength;i++)
	{
		float ppasreal;
		float ppasimag;
		float ppasreag;
		float ppasimal;
		complexe ppas;
		ppasreal=PA_Output[i].real*ScaleEst[0].real;
		ppasimag=PA_Output[i].imag*ScaleEst[0].imag;
		ppasreag=PA_Output[i].real*ScaleEst[0].imag;
		ppasimal=PA_Output[i].imag*ScaleEst[0].real;
		ppas.real=ppasreal-ppasimag;
		ppas.imag=ppasreag+ppasimal;
		ErrorSignal.real=ppas.real-stacksignal[i].real;
		ErrorSignal.imag=ppas.imag-stacksignal[i].imag;

		conj_ErrorSignal[i].real=ErrorSignal.real;
		conj_ErrorSignal[i].imag=-ErrorSignal.imag;
	}
}


void dpd_learnInit(dpd_learn_data_t *data) {
	data->iteration = 0;
}

void dpd_learnFinish(dpd_learn_data_t *data) {
	//printf("dpd_learn: %f (%i)\n", data->totalTime / data->iteration, data->iteration);
}

void *dpd_learnFire(void *p) {

	dpd_learn_data_t *data = (dpd_learn_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		//actorTerminate();
	} else {
		//unsigned long long t1, t2;
		//unsigned int cal;
		//float totalTime;

		complexe ScaleEst;
		int varLength;

//		cal = calibrate();
//		t1 = timestamp();

		fifo_t *fifo_conf = data->shared->inputs[DPD_LEARN_CONF];
		int *token_conf = (int *) fifoReadStart(fifo_conf);
		fifoReadEnd(fifo_conf);

		varLength = token_conf[0];

		fifo_t *fifo_stacksignal = data->shared->inputs[DPD_LEARN_STACKSIGNAL];
		complexe *token_stacksignal = (complexe *) fifoReadStart(fifo_stacksignal);
		fifo_t *fifo_pa_output = data->shared->inputs[DPD_LEARN_PA_OUTPUT];
		complexe *token_pa_output = (complexe *) fifoReadStart(fifo_pa_output);
		scale_est(&ScaleEst, token_pa_output, &token_stacksignal[1], varLength);
		fifo_t *fifo_errorsignal = data->shared->outputs[DPD_LEARN_ERRORSIGNAL];
		complexe *token_errorsignal = (complexe *) fifoWriteStart(fifo_errorsignal);
		compute_ErrorSignal(token_errorsignal, &ScaleEst, token_pa_output, &token_stacksignal[1], varLength);
		fifoReadEnd(fifo_pa_output);
		fifoReadEnd(fifo_stacksignal);
		fifoWriteEnd(fifo_errorsignal);

//		t2 = timestamp();	
//		data->totalTime += ((float) (((unsigned int)(t2-t1))-cal)) / 1000000.0;

		data->iteration ++;
	}
	return p;
}


