#include "dpd.h"
#include <stdio.h>
#include "common.h"

typedef struct {
	int iteration;
	int count;
	shared_t *shared;
	float totalTime;
} dpd_learn_data_t;

void dpd_learnInit(dpd_learn_data_t *data);
void *dpd_learnFire(void *p);
void dpd_learnFinish(dpd_learn_data_t *data);


