#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	shared_t *shared;
	char const *fn;
	int iteration;
} conf_data_t;

void confInit(conf_data_t *data);
void *confFire(void *p);
void confFinish(conf_data_t *data);


