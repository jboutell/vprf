#include "dpd.h"
#include "conf.h"
#include <stdlib.h>
#include <string.h>
#include "ports.h"

extern volatile int globalRepetitions;

void confInit(conf_data_t *data) {
	char confFileName[256];
	data->file = NULL;
	sprintf(confFileName, "%s.bin", data->fn);
	data->iteration = 0;
	data->file = fopen(confFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", confFileName);
		return;
	}
}

void *confFire(void *p) {
	conf_data_t *data = (conf_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		actorTerminate();
	} else {
		int enable;
		int retval = fread(&enable, sizeof(int), 1, data->file);
		if (retval != 1) {
			printf("Source %s depleted\n", data->fn);
		}
		int *wbufout0 = (int *) fifoWriteStart(data->shared->outputs[CONF_OUT1_0]);
		int *wbufout1 = (int *) fifoWriteStart(data->shared->outputs[CONF_OUT1_1]);
		int *wbufout2 = (int *) fifoWriteStart(data->shared->outputs[CONF_OUT1_2]);
		wbufout0[0] = enable;
		wbufout1[0] = enable;
		wbufout2[0] = enable;
		fifoWriteEnd(data->shared->outputs[CONF_OUT1_0]);
		fifoWriteEnd(data->shared->outputs[CONF_OUT1_1]);
		fifoWriteEnd(data->shared->outputs[CONF_OUT1_2]);

		data->iteration ++;
	}

	return p;
}

void confFinish(conf_data_t *data) {
//	if (data->file != NULL) {
//		fclose(data->file);
//		data->file = NULL;
//	}
}

