#include "dpd.h"
#include <stdio.h>
#include "common.h"

typedef struct {
	int iteration;
	int count;
	shared_t *shared;
	complexe last[10];
	//complexe *dpd_input;
	complexe W[(L-1)];
} dpd_update_data_t;

void dpd_updateInit(dpd_update_data_t *data);
void *dpd_updateFire(void *p);
void dpd_updateFinish(dpd_update_data_t *data);


