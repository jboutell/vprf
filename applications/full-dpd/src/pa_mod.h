#include "dpd.h"
#include <stdio.h>
#include "common.h"

typedef struct {
	int iteration;
	int count;
	shared_t *shared;
	FILE *file;
	int length;
	complexe sample_mem;
	char const *fn;
} pa_mod_data_t;

void pa_modInit(pa_mod_data_t *data);
void *pa_modFire(void *p);
void pa_modFinish(pa_mod_data_t *data);


