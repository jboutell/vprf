// Code adapted from intel oneDNN/examples/cnn_inference_f32.c
// Original version targets AlexNet, this modified code targets
// "Resource-Constrained Implementation and Optimization
//   of a Deep Neural Network for Vehicle Classification"
//   by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
//   EUSIPCO 2016

#include "dnn.h"
#include "conv_relu_l1.h"
#ifdef DBGPRINT
	#include <stdio.h>
#endif
#include "example_utils.h"
#include "profile.h"

int conv_relu_l1Init(conv_relu_l1_data_t *data) {

	data->convL1 = (dnn_conv_t *) malloc(sizeof(dnn_conv_t));
	data->reluL1 = (dnn_relu_t *) malloc(sizeof(dnn_relu_t));
	data->poolL1 = (dnn_pool_t *) malloc(sizeof(dnn_pool_t));

	// c1c2 and in exist just for configuration
	float *in = (float *) malloc(sizeof(float) * C1 * CONV1_DIM * CONV1_DIM);
	float *c1c2 = (float *) malloc(sizeof(float) * OC * CONV2_DIM * CONV2_DIM);
	data->wgt1 = (float *) malloc(sizeof(float) * C1 * OC * CONV_SIZE * CONV_SIZE);
    data->conv_bias = (float *)malloc(sizeof(float) * OC);

    memset(data->conv_bias, 0, OC * sizeof(float));

	load_data(data->wgt1, C1 * OC * CONV_SIZE * CONV_SIZE, "data/conv1_flipped.bin");

    dnnl_engine_kind_t engine_kind = validate_engine_kind(L1_DEVICE);
    CHECK(dnnl_engine_create(&data->engine1, engine_kind, 0));

    CHECK(dnnl_stream_create(&data->stream1, data->engine1, dnnl_stream_default_flags));

	const dnnl_memory_desc_t *p1;
    setup_conv(data->engine1, data->convL1, data->L1_n, data->L1_net, data->L1_args, CONV1_DIM, C1, in, data->wgt1, data->conv_bias);
    p1 = setup_relu(data->engine1, data->convL1, data->reluL1, data->L1_n, data->L1_net, data->L1_args);
    setup_pool(data->engine1, data->poolL1, data->L1_n, data->L1_net, data->L1_args, POOL1_DIM, in, p1, data->reluL1->relu_dst_memory);

	//printf("L1 JIT compilation ... ");
    write_to_dnnl_memory(in, data->convL1->conv_user_src_memory);
    run(data->stream1, data->L1_n, data->L1_net, data->L1_args);
    CHECK(dnnl_stream_wait(data->stream1));
    read_from_dnnl_memory(c1c2, data->poolL1->pool_user_dst_memory);
	//printf("done\n");

	free(c1c2);
	free(in);

	return 0;
}

void conv_relu_l1Finish(conv_relu_l1_data_t *data) {
	clean_conv(data->convL1);
	clean_relu(data->reluL1);
	clean_pool(data->poolL1);

	free(data->convL1);
	free(data->reluL1);
	free(data->poolL1);

	free(data->wgt1);
    free(data->conv_bias);

    for (uint32_t i = 0; i < data->L1_n[0]; ++i) {
        free_arg_node(&data->L1_args[i]);
 	}
	dnnl_stream_destroy(data->stream1);
    dnnl_engine_destroy(data->engine1);
}


void* conv_relu_l1Fire (void *p) {
	conv_relu_l1_data_t *data = (conv_relu_l1_data_t *) p;
	cl_float *fifo_in = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *fifo_out = (cl_float *) fifoWriteStart(data->shared->outputs[0]);

    write_to_dnnl_memory(fifo_in, data->convL1->conv_user_src_memory);
    run(data->stream1, data->L1_n, data->L1_net, data->L1_args);
    CHECK(dnnl_stream_wait(data->stream1));
    read_from_dnnl_memory(fifo_out, data->poolL1->pool_user_dst_memory);

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}

