// Code adapted from intel oneDNN/examples/cnn_inference_f32.c
// Original version targets AlexNet, this modified code targets
// "Resource-Constrained Implementation and Optimization
//   of a Deep Neural Network for Vehicle Classification"
//   by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
//   EUSIPCO 2016

// Required for posix_memalign
#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "onednn.h"
#include "example_utils.h"

size_t product(dnnl_dim_t *arr, size_t size) {
    size_t prod = 1;
    for (size_t i = 0; i < size; ++i)
        prod *= arr[i];
    return prod;
}

void load_data(float *data, uint32_t size, const char *fn) {
	int cnt = 0;
	int cntidx = 0;
	FILE *file = fopen(fn, "rb");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return;
	}

    for (int i = 0; i < size; ++i) {
		cnt += fread(&data[i], sizeof(float), 1, file);
		cntidx ++;
    }
	if (cntidx != cnt) {
		printf("Something went wrong in reading %s (sum difference)\n", fn);
	}
	fclose(file);
}

void save_data(float *data, int size, const char *fn) {
	FILE *file = fopen(fn, "wb");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return;
	}
    for (dnnl_dim_t i = 0; i < size; ++i) {
		fwrite(&data[i], sizeof(float), 1, file);
    }
	fclose(file);
}

static void prepare_arg_node(args_t *node, int nargs) {
    node->args = (dnnl_exec_arg_t *)malloc(sizeof(dnnl_exec_arg_t) * nargs);
    node->nargs = nargs;
}

void free_arg_node(args_t *node) {
    free(node->args);
}

void set_arg(dnnl_exec_arg_t *arg, int arg_idx, dnnl_memory_t memory) {
    arg->arg = arg_idx;
    arg->memory = memory;
}

static void init_data_memory(uint32_t dim, const dnnl_dim_t *dims,
        dnnl_format_tag_t user_tag, dnnl_engine_t engine,
        dnnl_memory_t *memory) {
    dnnl_memory_desc_t user_md;
    CHECK(dnnl_memory_desc_init_by_tag(
            &user_md, dim, dims, dnnl_f32, user_tag));
    CHECK(dnnl_memory_create(memory, &user_md, engine, DNNL_MEMORY_ALLOCATE));
}

dnnl_status_t prepare_reorder(dnnl_memory_t *user_memory, // in
        const dnnl_memory_desc_t *prim_memory_md, // in
        dnnl_engine_t prim_engine, // in: primitive's engine
        int dir_is_user_to_prim, // in: user -> prim or prim -> user
        dnnl_memory_t *prim_memory, // out: primitive's memory created
        dnnl_primitive_t *reorder, // out: reorder primitive created
        uint32_t *net_index, // primitive index in net (inc if reorder created)
        dnnl_primitive_t *net, args_t *net_args) { // net params
    const dnnl_memory_desc_t *user_memory_md;
    dnnl_memory_get_memory_desc(*user_memory, &user_memory_md);

    dnnl_engine_t user_mem_engine;
    dnnl_memory_get_engine(*user_memory, &user_mem_engine);

    if (!dnnl_memory_desc_equal(user_memory_md, prim_memory_md)) {
        CHECK(dnnl_memory_create(prim_memory, prim_memory_md, prim_engine,
                DNNL_MEMORY_ALLOCATE));

        dnnl_primitive_desc_t reorder_pd;
        if (dir_is_user_to_prim) {
            CHECK(dnnl_reorder_primitive_desc_create(&reorder_pd,
                    user_memory_md, user_mem_engine, prim_memory_md,
                    prim_engine, NULL));
        } else {
            CHECK(dnnl_reorder_primitive_desc_create(&reorder_pd,
                    prim_memory_md, prim_engine, user_memory_md,
                    user_mem_engine, NULL));
        }
        CHECK(dnnl_primitive_create(reorder, reorder_pd));
        CHECK(dnnl_primitive_desc_destroy(reorder_pd));

        net[*net_index] = *reorder;
        prepare_arg_node(&net_args[*net_index], 2);
        set_arg(&net_args[*net_index].args[0], DNNL_ARG_FROM,
                dir_is_user_to_prim ? *user_memory : *prim_memory);
        set_arg(&net_args[*net_index].args[1], DNNL_ARG_TO,
                dir_is_user_to_prim ? *prim_memory : *user_memory);
        (*net_index)++;
    } else {
        *prim_memory = NULL;
        *reorder = NULL;
    }

    return dnnl_success;
}

// Vehicle classifier convolution
// {BATCH, IC, in_size, in_size} (x) {OC, IC, 5, 5} ->
// {BATCH, OC, in_size, in_size}
// strides: {CONV_STRIDE, CONV_STRIDE}
void setup_conv(dnnl_engine_t engine, dnn_conv_t *conv_s,
	uint32_t *n, dnnl_primitive_t *net, args_t *net_args,
	const int in_size, const int in_channels,
	float *conv_src, float *conv_weights, float *conv_bias) {

	n[0] = 0;

    const int ndims = 4;
    dnnl_dims_t net_src_sizes = {BATCH, in_channels, in_size, in_size};

    dnnl_dims_t conv_user_src_sizes;
    for (int i = 0; i < ndims; i++)
        conv_user_src_sizes[i] = net_src_sizes[i];
    dnnl_dims_t conv_user_weights_sizes = {OC, in_channels, CONV_SIZE, CONV_SIZE};
    dnnl_dims_t conv_bias_sizes = {OC};
    dnnl_dims_t conv_user_dst_sizes = {BATCH, OC, in_size, in_size};
    dnnl_dims_t conv_strides = {CONV_STRIDE, CONV_STRIDE};
    dnnl_dims_t conv_padding = {CONV_PAD, CONV_PAD};

    // create memory for user data
    init_data_memory(ndims, conv_user_src_sizes, dnnl_nchw, engine,
            &conv_s->conv_user_src_memory);

    init_data_memory(ndims, conv_user_weights_sizes, dnnl_oihw, engine,
            &conv_s->conv_user_weights_memory);
    write_to_dnnl_memory(conv_weights, conv_s->conv_user_weights_memory);

    init_data_memory(1, conv_bias_sizes, dnnl_x, engine,
            &conv_s->conv_user_bias_memory);
    write_to_dnnl_memory(conv_bias, conv_s->conv_user_bias_memory);

    // create data descriptors for convolution w/ no specified format
    dnnl_memory_desc_t conv_src_md, conv_weights_md, conv_bias_md, conv_dst_md;
    CHECK(dnnl_memory_desc_init_by_tag(&conv_src_md, ndims, conv_user_src_sizes,
            dnnl_f32, dnnl_format_tag_any));
    CHECK(dnnl_memory_desc_init_by_tag(&conv_weights_md, ndims,
            conv_user_weights_sizes, dnnl_f32, dnnl_format_tag_any));
    CHECK(dnnl_memory_desc_init_by_tag(
            &conv_bias_md, 1, conv_bias_sizes, dnnl_f32, dnnl_x));
    CHECK(dnnl_memory_desc_init_by_tag(&conv_dst_md, ndims, conv_user_dst_sizes,
            dnnl_f32, dnnl_format_tag_any));

    // create a convolution
    dnnl_convolution_desc_t conv_any_desc;
    CHECK(dnnl_convolution_forward_desc_init(&conv_any_desc, dnnl_forward,
            dnnl_convolution_direct, &conv_src_md, &conv_weights_md,
            &conv_bias_md, &conv_dst_md, conv_strides, conv_padding,
            conv_padding));

    CHECK(dnnl_primitive_desc_create(
            &conv_s->conv_pd, &conv_any_desc, NULL, engine, NULL));


    // create memory for dst data, we don't need reorder it to user data
    const dnnl_memory_desc_t *dst_md
            = dnnl_primitive_desc_query_md(conv_s->conv_pd, dnnl_query_dst_md, 0);
    CHECK(dnnl_memory_create(
            &conv_s->conv_internal_dst_memory, dst_md, engine, DNNL_MEMORY_ALLOCATE));

    // create reorder primitives between user data and convolution srcs
    // if required

    const dnnl_memory_desc_t *src_md
            = dnnl_primitive_desc_query_md(conv_s->conv_pd, dnnl_query_src_md, 0);
    CHECK(prepare_reorder(&conv_s->conv_user_src_memory, src_md, engine, 1,
            &conv_s->conv_internal_src_memory, &conv_s->conv_reorder_src, n, net, net_args));

    const dnnl_memory_desc_t *weights_md
            = dnnl_primitive_desc_query_md(conv_s->conv_pd, dnnl_query_weights_md, 0);
    CHECK(prepare_reorder(&conv_s->conv_user_weights_memory, weights_md, engine, 1,
            &conv_s->conv_internal_weights_memory, &conv_s->conv_reorder_weights, n, net,
            net_args));

    dnnl_memory_t conv_src_memory = conv_s->conv_internal_src_memory
            ? conv_s->conv_internal_src_memory
            : conv_s->conv_user_src_memory;
    dnnl_memory_t conv_weights_memory = conv_s->conv_internal_weights_memory
            ? conv_s->conv_internal_weights_memory
            : conv_s->conv_user_weights_memory;

    // finally create a convolution primitive
    CHECK(dnnl_primitive_create(&conv_s->conv, conv_s->conv_pd));
    net[n[0]] = conv_s->conv;
    prepare_arg_node(&net_args[n[0]], 4);
    set_arg(&net_args[n[0]].args[0], DNNL_ARG_SRC, conv_src_memory);
    set_arg(&net_args[n[0]].args[1], DNNL_ARG_WEIGHTS, conv_weights_memory);
    set_arg(&net_args[n[0]].args[2], DNNL_ARG_BIAS, conv_s->conv_user_bias_memory);
    set_arg(&net_args[n[0]].args[3], DNNL_ARG_DST, conv_s->conv_internal_dst_memory);
    n[0]++;
}

// Vehicle classifier: relu
// {BATCH, OC, in_size, in_size} -> {BATCH, OC, in_size, in_size}
const dnnl_memory_desc_t* setup_relu(dnnl_engine_t engine, dnn_conv_t *conv_s, dnn_relu_t *relu_s,
	uint32_t *n, dnnl_primitive_t *net, args_t *net_args) {

    const int ndims = 4;
    float negative_slope = 0.0f;

    // create relu memory descriptor on dst memory descriptor
    // from previous primitive
    const dnnl_memory_desc_t *relu_src_md
            = dnnl_primitive_desc_query_md(conv_s->conv_pd, dnnl_query_dst_md, 0);

    // create a relu
    dnnl_eltwise_desc_t relu_desc;
    CHECK(dnnl_eltwise_forward_desc_init(&relu_desc, dnnl_forward,
            dnnl_eltwise_relu, relu_src_md, negative_slope, 0));

    CHECK(dnnl_primitive_desc_create(&relu_s->relu_pd, &relu_desc, NULL, engine, NULL));

    const dnnl_memory_desc_t *relu_dst_md
            = dnnl_primitive_desc_query_md(relu_s->relu_pd, dnnl_query_dst_md, 0);
    CHECK(dnnl_memory_create(
            &relu_s->relu_dst_memory, relu_dst_md, engine, DNNL_MEMORY_ALLOCATE));

    // finally create a relu primitive
    CHECK(dnnl_primitive_create(&relu_s->relu, relu_s->relu_pd));
    net[n[0]] = relu_s->relu;
    prepare_arg_node(&net_args[n[0]], 2);
    set_arg(&net_args[n[0]].args[0], DNNL_ARG_SRC, conv_s->conv_internal_dst_memory);
    set_arg(&net_args[n[0]].args[1], DNNL_ARG_DST, relu_s->relu_dst_memory);
    n[0]++;

	return relu_dst_md;
}

// Vehicle classifier: pool
// {BATCH, OC, in_size, in_size} -> {BATCH, OC, out_size, out_size}
// kernel: {3, 3}
// strides: {POOL_STRIDE, POOL_STRIDE}
// dilation: {0, 0}
void setup_pool(dnnl_engine_t engine, dnn_pool_t *pool_s,
	uint32_t *n, dnnl_primitive_t *net, args_t *net_args,
	const int out_size,
	float *net_dst, const dnnl_memory_desc_t *relu_dst_md, dnnl_memory_t src_memory) {

    const int ndims = 4;
    dnnl_dims_t net_dst_sizes = {BATCH, OC, out_size, out_size};
    dnnl_dims_t pool_dst_sizes;
    for (int i = 0; i < ndims; i++)
        pool_dst_sizes[i] = net_dst_sizes[i];
    dnnl_dims_t pool_kernel = {POOL_SIZE, POOL_SIZE};
    dnnl_dims_t pool_strides = {POOL_STRIDE, POOL_STRIDE};
    dnnl_dims_t pool_padding = {POOL_PAD, POOL_PAD};
    dnnl_dims_t pool_dilation = {0, 0};

    // create pooling memory descriptor on dst descriptor
    //  from previous primitive
    const dnnl_memory_desc_t *pool_src_md = relu_dst_md;

    // create descriptors for dst pooling data
    dnnl_memory_desc_t pool_dst_any_md;
    CHECK(dnnl_memory_desc_init_by_tag(&pool_dst_any_md, ndims, pool_dst_sizes,
            dnnl_f32, dnnl_format_tag_any));

    // create memory for user data
    init_data_memory(ndims, pool_dst_sizes, dnnl_nchw, engine,
            &pool_s->pool_user_dst_memory);

    // create a pooling
    dnnl_pooling_v2_desc_t pool_desc;
    CHECK(dnnl_pooling_v2_forward_desc_init(&pool_desc, dnnl_forward,
            dnnl_pooling_max, pool_src_md, &pool_dst_any_md, pool_strides,
            pool_kernel, pool_dilation, pool_padding, pool_padding));

    CHECK(dnnl_primitive_desc_create(&pool_s->pool_pd, &pool_desc, NULL, engine, NULL));

    // create memory for workspace
    const dnnl_memory_desc_t *pool_ws_md
            = dnnl_primitive_desc_query_md(pool_s->pool_pd, dnnl_query_workspace_md, 0);
    CHECK(dnnl_memory_create(
            &pool_s->pool_ws_memory, pool_ws_md, engine, DNNL_MEMORY_ALLOCATE));

    dnnl_memory_t pool_dst_memory;

    // create reorder primitives between user data and pooling dsts
    // if required
    const dnnl_memory_desc_t *pool_dst_md
            = dnnl_primitive_desc_query_md(pool_s->pool_pd, dnnl_query_dst_md, 0);
    n[0] += 1; // tentative workaround: preserve space for pooling that should
            // happen before the reorder
    CHECK(prepare_reorder(&pool_s->pool_user_dst_memory, pool_dst_md, engine, 0,
            &pool_s->pool_internal_dst_memory, &pool_s->pool_reorder_dst, n, net, net_args));
    n[0] -= pool_s->pool_reorder_dst ? 2 : 1;

    pool_dst_memory = pool_s->pool_internal_dst_memory ? pool_s->pool_internal_dst_memory
                                               : pool_s->pool_user_dst_memory;

    // finally create a pooling primitive
    CHECK(dnnl_primitive_create(&pool_s->pool, pool_s->pool_pd));
    net[n[0]] = pool_s->pool;
    prepare_arg_node(&net_args[n[0]], 3);
    set_arg(&net_args[n[0]].args[0], DNNL_ARG_SRC, src_memory);
    set_arg(&net_args[n[0]].args[1], DNNL_ARG_DST, pool_dst_memory);
    set_arg(&net_args[n[0]].args[2], DNNL_ARG_WORKSPACE, pool_s->pool_ws_memory);
    n[0]++;

    if (pool_s->pool_reorder_dst) n[0] += 1;
}

void run(dnnl_stream_t stream, uint32_t *n, dnnl_primitive_t *net, args_t *net_args) {

    for (uint32_t i = 0; i < n[0]; ++i) {
        CHECK(dnnl_primitive_execute(net[i], stream, net_args[i].nargs, net_args[i].args));
    }

}

void clean_conv(dnn_conv_t *conv_s) {
    CHECK(dnnl_primitive_desc_destroy(conv_s->conv_pd));
    dnnl_memory_destroy(conv_s->conv_user_src_memory);
    dnnl_memory_destroy(conv_s->conv_user_weights_memory);
    dnnl_memory_destroy(conv_s->conv_user_bias_memory);
    dnnl_memory_destroy(conv_s->conv_internal_src_memory);
    dnnl_memory_destroy(conv_s->conv_internal_weights_memory);
    dnnl_memory_destroy(conv_s->conv_internal_dst_memory);
    dnnl_primitive_destroy(conv_s->conv_reorder_src);
    dnnl_primitive_destroy(conv_s->conv_reorder_weights);
    dnnl_primitive_destroy(conv_s->conv);
}

void clean_relu(dnn_relu_t *relu_s) {
    CHECK(dnnl_primitive_desc_destroy(relu_s->relu_pd));
    dnnl_memory_destroy(relu_s->relu_dst_memory);
    dnnl_primitive_destroy(relu_s->relu);
}

void clean_pool(dnn_pool_t *pool_s) {
    CHECK(dnnl_primitive_desc_destroy(pool_s->pool_pd));
    dnnl_memory_destroy(pool_s->pool_user_dst_memory);
    dnnl_memory_destroy(pool_s->pool_internal_dst_memory);
    dnnl_memory_destroy(pool_s->pool_ws_memory);
    dnnl_primitive_destroy(pool_s->pool_reorder_dst);
    dnnl_primitive_destroy(pool_s->pool);
}

