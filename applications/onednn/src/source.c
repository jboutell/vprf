#include "dnn.h"
#include "source.h"
#include <string.h>
#include "onednn.h"

extern volatile int globalRepetitions;

static void load_input(float *data, uint32_t size, FILE *file) {
	int cnt;

	cnt = fread(data, sizeof(float), size, file);

	if (cnt != size) {
		printf("Something went wrong in reading input file\n");
	}
}

void sourceInit(source_data_t *data) {
	data->file = NULL;
	char sourceFileName[256];
	sprintf(sourceFileName, "data/c384.bin");
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
	globalRepetitions = getFileSize(data->file) / (CHANNELS * PATCH1SQ * sizeof(float));
	data->iteration = 0;
	printf("Running for %i images\n", globalRepetitions);
}

void *sourceFire(void *p) {
	source_data_t *data = (source_data_t *) p;
	if(data->iteration >= globalRepetitions) {
		//printf("Source finished\n");
		actorTerminate();
	} else if (data->file != NULL) {
		float *wbufout1 = (float *) fifoWriteStart(data->shared->outputs[0]);
		#ifdef DBGPRINT
		printf("actor source fires\n");
		#endif
		load_input(wbufout1, C1 * CONV1_DIM * CONV1_DIM, data->file);
		fifoWriteEnd(data->shared->outputs[0]);
		data->iteration ++;
	}

	return p;
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

