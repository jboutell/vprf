#ifndef RELUL2_H
#define RELUL2_H

#include "common.h"
#include "dnn.h"
#include "oneapi/dnnl/dnnl.h"
#include "onednn.h"

typedef struct {
	shared_t *shared;

    uint32_t L2_n[1];
    dnnl_primitive_t L2_net[12];
    args_t L2_args[12];

	dnn_conv_t *convL2;
	dnn_relu_t *reluL2;
	dnn_pool_t *poolL2;
	float *wgt2;
    float *conv_bias;
    dnnl_engine_t engine2;
    dnnl_stream_t stream2;
} conv_relu_l2_data_t;

int conv_relu_l2Init(conv_relu_l2_data_t *data);
void *conv_relu_l2Fire(void *p);
void conv_relu_l2Finish(conv_relu_l2_data_t *data);

#endif
