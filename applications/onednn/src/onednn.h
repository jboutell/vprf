#ifndef ONEDNN_H
#define ONEDNN_H

#include "oneapi/dnnl/dnnl.h"

#define BATCH 1
#define C1 3
#define OC 32
#define CONV_SIZE 5
#define CONV1_DIM 96
#define CONV2_DIM 48
#define CONV_STRIDE 1
#define CONV_PAD 2
#define POOL1_DIM 48
#define POOL2_DIM 24
#define POOL_SIZE 2
#define POOL_STRIDE 2
#define POOL_PAD 0

typedef struct {
    dnnl_primitive_desc_t conv_pd;
    dnnl_memory_t conv_user_src_memory;
	dnnl_memory_t conv_user_weights_memory;
	dnnl_memory_t conv_user_bias_memory;
    dnnl_memory_t conv_internal_src_memory; 
	dnnl_memory_t conv_internal_weights_memory;
    dnnl_memory_t conv_internal_dst_memory;
    dnnl_primitive_t conv_reorder_src;
	dnnl_primitive_t conv_reorder_weights;
    dnnl_primitive_t conv;
} dnn_conv_t;

typedef struct {
    dnnl_primitive_desc_t relu_pd;
    dnnl_memory_t relu_dst_memory;
    dnnl_primitive_t relu;
} dnn_relu_t;

typedef struct {
    dnnl_primitive_desc_t pool_pd;
    dnnl_memory_t pool_user_dst_memory;
    dnnl_memory_t pool_internal_dst_memory;
    dnnl_memory_t pool_ws_memory;
    dnnl_primitive_t pool_reorder_dst;
    dnnl_primitive_t pool;
} dnn_pool_t;

typedef struct {
    int nargs;
    dnnl_exec_arg_t *args;
} args_t;

void load_data(float *data, uint32_t size, const char *fn);
void save_data(float *data, int size, const char *fn);
void set_arg(dnnl_exec_arg_t *arg, int arg_idx, dnnl_memory_t memory);
void free_arg_node(args_t *node);
void setup_conv(dnnl_engine_t engine, dnn_conv_t *conv_s,
	uint32_t *n, dnnl_primitive_t *net, args_t *net_args,
	const int in_size, const int in_channels,
	float *conv_src, float *conv_weights, float *conv_bias);
const dnnl_memory_desc_t* setup_relu(dnnl_engine_t engine, 
	dnn_conv_t *conv_s, dnn_relu_t *relu_s,
	uint32_t *n, dnnl_primitive_t *net, args_t *net_args);
void setup_pool(dnnl_engine_t engine, dnn_pool_t *pool_s,
	uint32_t *n, dnnl_primitive_t *net, args_t *net_args,
	const int out_size,	float *net_dst, const dnnl_memory_desc_t *relu_dst_md,
	dnnl_memory_t src_memory);
void run(dnnl_stream_t stream, uint32_t *n, dnnl_primitive_t *net, args_t *net_args);
void clean_conv(dnn_conv_t *conv_s);
void clean_relu(dnn_relu_t *relu_s);
void clean_pool(dnn_pool_t *pool_s);

#endif

