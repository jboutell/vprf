#include <stdio.h>
#include "common.h"

typedef struct {
	FILE *file;
	shared_t *shared;
	int iteration;
	char const *fn;
} source_data_t;

void sourceInit(source_data_t *data);
void *sourceFire(void *p);
void sourceFinish(source_data_t *data);

