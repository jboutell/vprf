#include <math.h>
#include <string.h>
#include "dnn.h"
#include "sink.h"

extern volatile int globalApplicationFinished;
extern volatile int globalRepetitions;

void lide_c_mtx_mulf(cl_float* A, cl_float* B, cl_float* c_mul, int Mdim, int Ndim, int Kdim) {
    int i, j, z;
	memset (c_mul, 0, sizeof(cl_float) * Mdim * Kdim);
    for (i = 0; i < Mdim; i++)
    {
        for (j = 0; j < Kdim; j++)
        {
            for (z = 0; z < Ndim; z++)
            {
                matrix(c_mul, Kdim, i, j) += matrix(A, Ndim, i, z) * matrix(B, Kdim, z, j);
            }
        }
    }
}

void lide_c_image_relu(cl_float *bef_array, cl_float *aft_array, int row, int column) {
    int i,j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < column; j++)
        {
            cl_float tmp = 0;
            tmp = tmp > matrix(bef_array, column, i, j)? tmp : matrix(bef_array, column, i, j);
            matrix(aft_array, column, i, j) = tmp;
        }

    }
}

void lide_c_softmax_write(sink_data_t *data, cl_float *input_array, int m, int n) {
    int i,j;
    float sum = 0.0;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            sum += exp(matrix(input_array, n, i, j));
        }
    }
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
			fprintf(data->file, "%f\n", exp(matrix(input_array, n, i, j))/sum);
        }
    }
}

void openSink(sink_data_t *data) {
	char sinkFileName[256];
	data->file = NULL;
	data->length = CLASSES;
	sprintf(sinkFileName, "data/sink.bin");
	data->file = fopen (sinkFileName, "w");
	if (data->file == NULL) {
		printf("Could not open %s\n", sinkFileName);
		return;
	}
	if (globalRepetitions == 0) {
		printf("Warning: number of repetitions uninitialized\n");
	}
	data->repetitions = globalRepetitions;
	data->iteration = 0;
}

void sinkInit(sink_data_t *data) {
	char fileName[256];
	sprintf(fileName, "data/ip4.bin");
	readRawData(fileName, (void **) &data->coeff_l4);
	sprintf(fileName, "data/ip_last.bin");
	readRawData (fileName, (void **) &data->coeff_l5);
	openSink (data);
}

void *sinkFire(void *p) {
	sink_data_t *data = (sink_data_t *) p;
	if(data->iteration >= data->repetitions) {
		//printf("Sink finished\n");
		globalApplicationFinished = 1;
	} else {
		if (data->file != NULL) {
			cl_float fifo_relu_out_1[MAGIC];
			cl_float fifo_multi_out_2[MAGIC];
			cl_float fifo_relu_out_2[MAGIC];
			cl_float fifo_multi_out_3[CLASSES];
			cl_float *input = (cl_float *) fifoReadStart(data->shared->inputs[0]);
			lide_c_image_relu(input, fifo_relu_out_1, MAGIC, 1);
			fifoReadEnd(data->shared->inputs[0]);

			// LAYER 4
			lide_c_mtx_mulf(data->coeff_l4, fifo_relu_out_1, fifo_multi_out_2, MAGIC, MAGIC, 1);
			lide_c_image_relu(fifo_multi_out_2, fifo_relu_out_2, MAGIC, 1);

			// LAYER 5
			lide_c_mtx_mulf(data->coeff_l5, fifo_relu_out_2, fifo_multi_out_3, CLASSES, MAGIC, 1);
			lide_c_softmax_write (data, fifo_multi_out_3, 4, 1);
		}
		data->iteration += REPEAT;
	}

	return p;
}

void sinkFinish(sink_data_t *data) {
	if (data->file != NULL) {
		free (data->coeff_l4);
		free (data->coeff_l5);
		fflush (data->file);
		fclose (data->file);
		data->file = NULL;
	}
}

