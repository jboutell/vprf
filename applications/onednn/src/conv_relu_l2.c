// Code adapted from intel oneDNN/examples/cnn_inference_f32.c
// Original version targets AlexNet, this modified code targets
// "Resource-Constrained Implementation and Optimization
//   of a Deep Neural Network for Vehicle Classification"
//   by R. Xie, H. Huttunen, S. Lin, S. S. Bhattacharyya, J. Takala
//   EUSIPCO 2016

#include "dnn.h"
#include "conv_relu_l2.h"
#ifdef DBGPRINT
	#include <stdio.h>
#endif
#include "example_utils.h"

int conv_relu_l2Init(conv_relu_l2_data_t *data) {

	data->convL2 = (dnn_conv_t *) malloc(sizeof(dnn_conv_t));
	data->reluL2 = (dnn_relu_t *) malloc(sizeof(dnn_relu_t));
	data->poolL2 = (dnn_pool_t *) malloc(sizeof(dnn_pool_t));

	// c1c2 and out exist just for configuration
	float *c1c2 = (float *) malloc(sizeof(float) * OC * CONV2_DIM * CONV2_DIM);
	float *out = (float *) malloc(sizeof(float) * OC * POOL2_DIM * POOL2_DIM);
	data->wgt2 = (float *) malloc(sizeof(float) * OC * OC * CONV_SIZE * CONV_SIZE);
    data->conv_bias = (float *) malloc(sizeof(float) * OC);

    memset(data->conv_bias, 0, OC * sizeof(float));

	load_data(data->wgt2, OC * OC * CONV_SIZE * CONV_SIZE, "data/conv2_flipped.bin");

    dnnl_engine_kind_t engine_kind = validate_engine_kind(L2_DEVICE);
    CHECK(dnnl_engine_create(&data->engine2, engine_kind, 0));

    CHECK(dnnl_stream_create(&data->stream2, data->engine2, dnnl_stream_default_flags));

	const dnnl_memory_desc_t *p2;
    setup_conv(data->engine2, data->convL2, data->L2_n, data->L2_net, data->L2_args, CONV2_DIM, OC, c1c2, data->wgt2, data->conv_bias);
    p2 = setup_relu(data->engine2, data->convL2, data->reluL2, data->L2_n, data->L2_net, data->L2_args);
    setup_pool(data->engine2, data->poolL2, data->L2_n, data->L2_net, data->L2_args, POOL2_DIM, out, p2, data->reluL2->relu_dst_memory);

	//printf("L2 JIT compilation ... ");
    write_to_dnnl_memory(c1c2, data->convL2->conv_user_src_memory);
    run(data->stream2, data->L2_n, data->L2_net, data->L2_args);
    CHECK(dnnl_stream_wait(data->stream2));
    read_from_dnnl_memory(out, data->poolL2->pool_user_dst_memory);
	//printf("done\n");

	free(c1c2);
	free(out);

	return 0;
}

void conv_relu_l2Finish(conv_relu_l2_data_t *data) {
	clean_conv(data->convL2);
	clean_relu(data->reluL2);
	clean_pool(data->poolL2);

	free(data->convL2);
	free(data->reluL2);
	free(data->poolL2);

	free(data->wgt2);
    free(data->conv_bias);

    for (uint32_t i = 0; i < data->L2_n[0]; ++i) {
        free_arg_node(&data->L2_args[i]);
	} 
    dnnl_stream_destroy(data->stream2);
    dnnl_engine_destroy(data->engine2);
}


void* conv_relu_l2Fire (void *p) {
	conv_relu_l2_data_t *data = (conv_relu_l2_data_t *) p;
	cl_float *fifo_in = (cl_float *) fifoReadStart(data->shared->inputs[0]);
	cl_float *fifo_out = (cl_float *) fifoWriteStart(data->shared->outputs[0]);

    write_to_dnnl_memory(fifo_in, data->convL2->conv_user_src_memory);
    run(data->stream2, data->L2_n, data->L2_net, data->L2_args);
    CHECK(dnnl_stream_wait(data->stream2));
    read_from_dnnl_memory(fifo_out, data->poolL2->pool_user_dst_memory);

	fifoReadEnd(data->shared->inputs[0]);
	fifoWriteEnd(data->shared->outputs[0]);

	return p;
}

