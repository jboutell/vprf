#ifndef MULL3_H
#define MULL3_H

#include "common.h"
#include "dnn.h"
#include <stdio.h>

typedef struct {
	cl_float *wgt;
	shared_t *shared;
} mtx_mulf_l3_data_t;

int mtx_mulf_l3Init(mtx_mulf_l3_data_t *data);
void *mtx_mulf_l3Fire(void *p);
void mtx_mulf_l3Finish(mtx_mulf_l3_data_t *data);

#endif
