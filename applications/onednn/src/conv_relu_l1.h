#ifndef RELUL1_H
#define RELUL1_H

#include "common.h"
#include "dnn.h"
#include "oneapi/dnnl/dnnl.h"
#include "onednn.h"

typedef struct {
	shared_t *shared;

    uint32_t L1_n[1];
    dnnl_primitive_t L1_net[12];
    args_t L1_args[12];

	dnn_conv_t *convL1;
	dnn_relu_t *reluL1;
	dnn_pool_t *poolL1;
	float *wgt1;
    float *conv_bias;
    dnnl_engine_t engine1;
    dnnl_stream_t stream1;

} conv_relu_l1_data_t;

int conv_relu_l1Init(conv_relu_l1_data_t *data);
void *conv_relu_l1Fire(void *p);
void conv_relu_l1Finish(conv_relu_l1_data_t *data);

#endif
