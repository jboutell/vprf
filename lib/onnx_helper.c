#ifdef ONNX
#include "onnx_helper.h"

unsigned int hashCharArray(char *arr, unsigned int len) {
    unsigned int hash = 5381;
    for (unsigned int i = 0; i < len; i++) {
        hash = ((hash << 5) + hash) + arr[i];
    }
    return hash;
}

unsigned int onnx_tensor_checksum(struct onnx_tensor_t *t) {
    return hashCharArray((char *)t->datas, t->ndata);
}

unsigned int onnx_tensor_allocate(struct onnx_tensor_t *t) {
    unsigned int tsize = 1;
    for (int i = 0; i < t->ndim; i++) { tsize *= t->dims[i]; }
    t->ndata = tsize;
    t->datas = (char *) malloc(t->ndata * onnx_tensor_type_sizeof(t->type));
    return t->ndata;
}

void onnx_tensor_load(char *name, struct onnx_tensor_t *t) {
    FILE *tf = fopen(name, "rb");
    if (tf != NULL) {
        int ret = fread(&(t->ndata), sizeof(size_t), 1, tf);
        if (ret != 1) {
            printf("Read less bytes than expected from file %s\n", name);
        }
        int tsize = onnx_tensor_allocate(t)*onnx_tensor_type_sizeof(t->type);
        if (t->ndata > 0) {
            int len = fread(t->datas, onnx_tensor_type_sizeof(t->type), t->ndata, tf)*onnx_tensor_type_sizeof(t->type);
            if (len == tsize) {
                #ifdef DEBUG
                printf("%s: Loaded tensor of %i bytes with checksum %u\n", t->name, len, hashCharArray((char *)t->datas, t->ndata));
                #endif
            } else {
                printf("%s: Error: file %s size (%u) differs from tensor size of %u\n", t->name, name, len, tsize);
            }
            fclose(tf);
        } else {
            printf("%s: Allocated empty tensor of %i bytes\n", t->name, tsize);
        }
    } else {
        printf("%s: Error: could not open tensor file %s for reading\n", t->name, name);
    }
}

void onnx_node_init(struct onnx_node_t *n, int opset, int n_input, int n_output, Onnx__NodeProto* proto) {
    n->ctx = NULL;
    n->proto = proto;
    n->r = NULL;
    n->rctx = NULL;
    n->operator = NULL;
    n->reshape = NULL;
    n->init = NULL;
    n->opset = opset;
    n->ninput = n_input;
    n->inputs = (struct onnx_tensor_t **) malloc(sizeof(struct onnx_tensor_t *) * n->ninput);
    for(int j = 0; j < n->ninput; j++) {
        n->inputs[j] = (struct onnx_tensor_t *) malloc(sizeof(struct onnx_tensor_t));
    }
    n->noutput = n_output;
    n->outputs = (struct onnx_tensor_t **) malloc(sizeof(struct onnx_tensor_t *) * n->noutput);
    for(int j = 0; j < n->noutput; j++) {
        n->outputs[j] = (struct onnx_tensor_t *) malloc(sizeof(struct onnx_tensor_t));
    }
}

void onnx_node_free(struct onnx_node_t *n) {
    if(n->exit)
        n->exit(n);
    if(n->inputs) {
        for(int j = 0; j < n->ninput; j++) {
            if(n->inputs[j]) {
                onnx_tensor_free(n->inputs[j]);
            }
        }
        if(n->inputs) {
            free(n->inputs);
            n->inputs = NULL;
        }
    }
    if(n->outputs) {
        for(int j = 0; j < n->noutput; j++) {
            if(n->outputs[j]) {
                onnx_tensor_free(n->outputs[j]);
            }
        }
           if(n->outputs) {
            free(n->outputs);
            n->outputs = NULL;
        }
    }
    free(n);
}

void onnx_tensor_init(struct onnx_tensor_t *t, const char *name, enum onnx_tensor_type_t type, int ndim) {
    int namelen = strlen(name);
    t->name = malloc(namelen+1);
    strcpy(t->name, name);
    t->type = type;
    t->ndim = ndim;
    t->dims = malloc(sizeof(int) * ndim);
    t->strides = malloc(sizeof(int) * ndim);
    t->ndata = 0;
    t->datas = NULL;
}

static int reshape_dummy(struct onnx_node_t * n) {
    printf("Dummy reshape\n");
    return 1;
}

static void operator_dummy(struct onnx_node_t * n) {
    printf("Dummy operator\n");
}

void onnx_node_resolve(struct onnx_node_t *n) {
    if(!n->operator) {
        resolver_solve_operator(&resolver_default, n);
        if(n->operator) {
            n->r = &resolver_default;
            n->rctx = NULL;
        } else {
            printf("  failed\n");
        }
    }
    if(!n->reshape) {
        printf("Using dummy reshape\n");
        n->reshape = reshape_dummy;
    }
    if(!n->operator) {
        printf("Using dummy operator\n");
        n->operator = operator_dummy;
    }
    if(n->init) {
        if(n->init(n) <= 0)
        {
            printf("Error: n->init(n) <= 0\n");
            return;
        }
    } else {
        printf("Init does not exist\n");
    }
    if(n->reshape) {
        n->reshape(n);
    } else {
        printf("Reshape not applied\n");
    }
}
#endif
