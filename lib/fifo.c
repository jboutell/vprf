#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "fifo.h"

#define SYNC_FIFO CL_TRUE
//#define SIMPLE_FIFO

void fifoReset(fifo_t *fifo) {
	fifo->buffer = NULL;
	fifo->type = -1;
	fifo->tokenSize = 0;
	fifo->targetRate = 0;
	fifo->tokenDelays = 0;
	fifo->capacity = 0;

	fifo->writeSide = 0;
	fifo->readSide = 0;
}

void fifoFree(fifo_t *fifo) {
	pthread_mutex_destroy(&(fifo->mutex));
	pthread_cond_destroy(&(fifo->cond_full));
	pthread_cond_destroy(&(fifo->cond_empty));
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		for (int i = 0; i < fifo->numSlots; i++) {
			clReleaseMemObject((cl_mem)fifo->readSlot[i]);
#ifdef SIMPLE_FIFO
		}
#else
			clReleaseMemObject((cl_mem)fifo->writeSlot[i]);
		}
		if (fifo->tokenDelays > 0) {
			clReleaseMemObject((cl_mem)fifo->wrapWindow);
		}
		clReleaseMemObject((cl_mem)fifo->buffer);
#endif
		#endif
	} else if (fifo->type == T_FIFOGEN) {
		free(fifo->buffer);
	}
	free(fifo->readSlot);
	free(fifo->writeSlot);
}

void fifoSetName(fifo_t *fifo, char const *name) {
	fifo->name = name;
}

cl_mem fifoCreateSubBuffer(fifo_t *fifo, cl_mem buffer, int origin, int size) {
	#ifndef DISABLE_OPENCL
	cl_int err;
	cl_buffer_create_type type = CL_BUFFER_CREATE_TYPE_REGION;
	cl_buffer_region region;
	region.origin = origin;
	region.size = size;
	cl_mem subBuffer =
		clCreateSubBuffer(buffer, fifo->flags & (!CL_MEM_ALLOC_HOST_PTR), type, &region, &err);
	if (err) {
		printf("(%s) read:clCreateSubBuffer returned %i\n", fifo->name, err);
	}
	return subBuffer;
	#endif
}

void fifoAllocate(fifo_t *fifo, int type, int targetRate, int tokenSize, int tokenDelays,
				  int latency, void *data) {
	int i;
	fifo->type = type;
	fifo->tokenSize = tokenSize;
	fifo->targetRate = targetRate;
	fifo->tokenDelays = tokenDelays;
	fifo->numSlots = FIFO_SIZE;
	fifo->capacity = targetRate * fifo->numSlots + tokenDelays;

	if (tokenDelays > 1) {
		printf("Warning: Fifo %s has %i delays. Maximum 1 supported at the moment.\n", fifo->name,
			   tokenDelays);
	}

	pthread_mutex_init(&fifo->mutex, NULL);
	pthread_cond_init(&fifo->cond_full, NULL);
	pthread_cond_init(&fifo->cond_empty, NULL);

	fifo->readSlot = (void **)malloc(sizeof(void *) * fifo->numSlots);
	fifo->writeSlot = (void **)malloc(sizeof(void *) * fifo->numSlots);
	if (type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		cl_mem_params_t *params = (cl_mem_params_t *)data;
		fifo->flags = params->flags;
#ifdef SIMPLE_FIFO
		for (i = 0; i < fifo->numSlots; i++) {
			int err;
			fifo->readSlot[i] = clCreateBuffer(params->context, params->flags, tokenSize, NULL, &err);
			fifo->writeSlot[i] = fifo->readSlot[i];
			if (err) {
				printf("(%s) read:clCreateBuffer returned %i\n", fifo->name, err);
			}
		}
#else
		cl_mem tmp =
			clCreateBuffer(params->context, params->flags, tokenSize * fifo->capacity, NULL, NULL);
		fifo->buffer = tmp;
		for (i = 0; i < fifo->numSlots; i++) {
			fifo->readSlot[i] = fifoCreateSubBuffer(fifo, tmp, fifo->tokenSize * (targetRate * i),
													fifo->tokenSize * targetRate);
			fifo->writeSlot[i] =
				fifoCreateSubBuffer(fifo, tmp, fifo->tokenSize * (targetRate * i + tokenDelays),
									fifo->tokenSize * targetRate);
		}
		if (fifo->tokenDelays > 0) {
			fifo->wrapWindow =
				fifoCreateSubBuffer(fifo, tmp, fifo->tokenSize * targetRate * FIFO_SIZE,
									fifo->tokenSize * fifo->tokenDelays);
		}
#endif
		#endif
	} else if (type == T_FIFOGEN) {
		char *tmp = (char *)malloc(tokenSize * fifo->capacity);
		fifo->buffer = tmp;
		for (i = 0; i < fifo->numSlots; i++) {
			fifo->readSlot[i] = &tmp[fifo->tokenSize * (targetRate * i)];
			fifo->writeSlot[i] = &tmp[fifo->tokenSize * (targetRate * i + tokenDelays)];
		}
		if (fifo->tokenDelays > 0) {
			fifo->wrapWindow = &tmp[fifo->tokenSize * targetRate * FIFO_SIZE];
		}
	}
}

/*
	Write pointer and read pointer can be incremented indefinitely,
	as results will be correct even when an overflow happens at UINT_MAX.
*/

void *fifoReadLock(fifo_t *fifo) {
	pthread_mutex_lock(&(fifo->mutex));
#ifdef DBGPRINT
	if (fifo->writeSide - fifo->readSide < 1) {
		printf("reading actor sleeps as fifo %s does not have sufficient tokens\n", fifo->name);
	}
#endif
	if (fifo->writeSide - fifo->readSide < 1) {
		pthread_cond_wait(&(fifo->cond_empty), &(fifo->mutex));
	}

	void *token = fifo->readSlot[fifo->readSide % fifo->numSlots];
	pthread_mutex_unlock(&(fifo->mutex));
	return token;
}

void *fifoReadStart(fifo_t *fifo) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		fifo->clInToken = (cl_mem)fifoReadLock(fifo);
		fifo->inToken =
			(void *)clEnqueueMapBuffer(fifo->commQueue, fifo->clInToken, SYNC_FIFO, CL_MAP_READ, 0,
									   fifo->tokenSize * fifo->targetRate, 0, NULL, NULL, NULL);
		#endif
		return fifo->inToken;
	}
	return fifoReadLock(fifo);
}

void* fifoReadStartN(fifo_t *fifo, int rate) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		fifo->clInToken = (cl_mem) fifoReadLock(fifo);
		fifo->inToken = (void *) clEnqueueMapBuffer(fifo->commQueue, fifo->clInToken,
			CL_TRUE, CL_MAP_READ, 0, fifo->tokenSize*rate, 0, NULL, NULL, NULL);
		#endif
		return fifo->inToken;
	}
	return fifoReadLock(fifo);
}

void fifoReadUnlock(fifo_t *fifo) {
	pthread_mutex_lock(&(fifo->mutex));
	fifo->readSide++;
	pthread_cond_broadcast(&(fifo->cond_full));
	pthread_mutex_unlock(&(fifo->mutex));
}

void fifoReadEnd(fifo_t *fifo) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		clEnqueueUnmapMemObject(fifo->commQueue, fifo->clInToken, fifo->inToken, 0, NULL, NULL);
		#endif
	}
	fifoReadUnlock(fifo);
}

void *fifoPeekStart(fifo_t *fifo) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		fifo->clInToken = (cl_mem)fifoReadLock(fifo);
		fifo->inToken =
			(void *)clEnqueueMapBuffer(fifo->commQueue, fifo->clInToken, SYNC_FIFO, CL_MAP_READ, 0,
									   fifo->tokenSize * fifo->targetRate, 0, NULL, NULL, NULL);
		#endif
		return fifo->inToken;
	}
	return fifoReadLock(fifo);
}

void fifoPeekEnd(fifo_t *fifo) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		clEnqueueUnmapMemObject(fifo->commQueue, fifo->clInToken, fifo->inToken, 0, NULL, NULL);
		#endif
	}
}

void *fifoWriteLock(fifo_t *fifo) {
	pthread_mutex_lock(&(fifo->mutex));
#ifdef DBGPRINT
	if (fifo->writeSide - fifo->readSide > 1) {
		printf("writing actor sleeps as fifo %s has no space\n", fifo->name);
	}
#endif
	while (fifo->writeSide - fifo->readSide > 1) {
		pthread_cond_wait(&(fifo->cond_full), &(fifo->mutex));
	}

	void *slot = fifo->writeSlot[fifo->writeSide % fifo->numSlots];
	pthread_mutex_unlock(&(fifo->mutex));
	return slot;
}

void *fifoWriteStart(fifo_t *fifo) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		int err;
		fifo->clOutToken = (cl_mem)fifoWriteLock(fifo);
		fifo->outToken =
			(void *)clEnqueueMapBuffer(fifo->commQueue, fifo->clOutToken, SYNC_FIFO, CL_MAP_WRITE, 0,
									   fifo->tokenSize * fifo->targetRate, 0, NULL, NULL, &err);
		#endif
		return fifo->outToken;
	}
	return fifoWriteLock(fifo);
}

void* fifoWriteStartN(fifo_t *fifo, int rate) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		int err;
		fifo->clOutToken = (cl_mem) fifoWriteLock(fifo);
		fifo->outToken = (void *) clEnqueueMapBuffer(fifo->commQueue, fifo->clOutToken,
			CL_TRUE, CL_MAP_WRITE, 0, fifo->tokenSize*rate, 0, NULL, NULL, &err);
		#endif
		return fifo->outToken;
	}
	return fifoWriteLock(fifo);
}

void fifoWriteUnlock(fifo_t *fifo) {
	pthread_mutex_lock(&(fifo->mutex));
	if (fifo->tokenDelays > 0) {
		if (fifo->writeSide % FIFO_SIZE == (FIFO_SIZE - 1)) {
			if (fifo->type == T_FIFOCL) {
				#ifndef DISABLE_OPENCL
				clEnqueueCopyBuffer(fifo->commQueue, (cl_mem)fifo->wrapWindow,
									(cl_mem)fifo->readSlot[0], 0, 0,
									fifo->tokenSize * fifo->tokenDelays, 0, NULL, NULL);
				clFinish(fifo->commQueue);
				#endif
			} else {
				memcpy(fifo->readSlot[0], fifo->wrapWindow, fifo->tokenSize * fifo->tokenDelays);
			}
		}
	}
	fifo->writeSide++;
	pthread_cond_broadcast(&(fifo->cond_empty));
	pthread_mutex_unlock(&(fifo->mutex));
}

void fifoWriteEnd(fifo_t *fifo) {
	if (fifo->type == T_FIFOCL) {
		#ifndef DISABLE_OPENCL
		clEnqueueUnmapMemObject(fifo->commQueue, fifo->clOutToken, fifo->outToken, 0, NULL, NULL);
		#endif
	}
	fifoWriteUnlock(fifo);
}

void fifoSetCommandQueue(fifo_t *fifo, cl_command_queue commands) {
	fifo->commQueue = commands;
}

int fifoGetTokenSize(fifo_t *fifo) {
	return fifo->tokenSize;
}

int fifoGetType(fifo_t *fifo) {
	return fifo->type;
}

int fifoCountTokens(fifo_t *fifo) {
	return fifo->writeSide - fifo->readSide;
}

void* fifoTokenGet(fifo_t *fifo, int index) {
	return fifo->readSlot[index % fifo->numSlots];
}

