#include "network.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


volatile int globalApplicationFinished;
int globalRepetitions = 0;

void networkReset(network_t *network) {
	network->actors = NULL;
	network->actorCount = 0;

	network->fifos = NULL;
	network->fifoCount = 0;

	globalApplicationFinished = 0;
}

void networkFree(network_t *network) {
	int i;

	usleep(10);
	for (i = 0; i < network->actorCount; i++) {
		actorFree(&network->actors[i]);
	}
	free(network->actors);
	network->actors = NULL;
	network->actorCount = 0;

	for (i = 0; i < network->fifoCount; i++) {
		fifoFree(&network->fifos[i]);
	}
	free(network->fifos);
	network->fifos = NULL;
	network->fifoCount = 0;
}

int networkGetActorCount(network_t *network) {
	return network->actorCount;
}

int networkAddActor(network_t *network, char const *name) {
	int actorIndex;
	actorIndex = network->actorCount;
	network->actorCount++;
	network->actors = (actor_t *)realloc(network->actors, sizeof(actor_t) * network->actorCount);
	actorReset(&network->actors[actorIndex]);
	actorSetName(&network->actors[actorIndex], name);
	return actorIndex;
}

int networkAddFifo(network_t *network, char const *name) {
	int fifoIndex;
	fifoIndex = network->fifoCount;
	network->fifoCount++;
	network->fifos = (fifo_t *)realloc(network->fifos, sizeof(fifo_t) * network->fifoCount);
	fifoReset(&network->fifos[fifoIndex]);
	fifoSetName(&network->fifos[fifoIndex], name);
	return fifoIndex;
}

void networkAddConnection(network_t *network, int source, int sink, int fifo) {
	actorAddInput(&network->actors[sink], &network->fifos[fifo]);
	actorAddOutput(&network->actors[source], &network->fifos[fifo]);
}

void networkLaunchActors(network_t *network) {
	int i, rc = 0;
	pthread_t *threads;
	threads = (pthread_t *)malloc(sizeof(pthread_t) * network->actorCount);

	for (i = 0; i < network->actorCount; i++) {
		rc |= pthread_create(&threads[i], NULL, &actorWrapper, &network->actors[i]);
	}

	if (rc) {
		printf("error: pthread_create: %d\n", rc);
	}

#ifdef DBGPRINT
	printf("network: all actors dispatched (pthread_create)\n");
#endif

	while (!globalApplicationFinished) {
		usleep(10);
	}

	for (i = 0; i < network->actorCount; i++) {
		if (pthread_cancel(threads[i]) != 0) {
			//printf("canceling of thread %i returned %i\n", i, 1);
		}
	}

	free(threads);
}
