#pragma once

#include "pthread.h"
#include <CL/opencl.h>

#define T_FIFOCL 1
#define T_FIFOGEN 2

typedef struct {
	char const *name;
	void *buffer;
	void **writeSlot;
	void **readSlot;
	void *wrapWindow;
	int numSlots;
	int type;
	int tokenSize;
	int targetRate;
	int tokenDelays;
	unsigned int capacity;

	unsigned int writeSide;
	unsigned int readSide;
	// specific to CL_FIFO
	cl_mem_flags flags;
	cl_command_queue commQueue;
	cl_mem clInToken;
	cl_mem clOutToken;

	void *inToken;
	void *outToken;

	pthread_mutex_t mutex;
	pthread_cond_t cond_full;
	pthread_cond_t cond_empty;
} fifo_t;

void fifoReset(fifo_t *fifo);
void fifoFree(fifo_t *fifo);
void fifoSetName(fifo_t *fifo, char const *name);
void fifoAllocate(fifo_t *fifo, int type, int targetRate, int tokenSize, int tokenDelays,
				  int latency, void *data);
void *fifoReadLock(fifo_t *fifo);
void fifoReadUnlock(fifo_t *fifo);
void *fifoWriteLock(fifo_t *fifo);
void fifoWriteUnlock(fifo_t *fifo);
void *fifoReadStart(fifo_t *fifo);
void* fifoReadStartN(fifo_t *fifo, int rate);
void fifoReadEnd(fifo_t *fifo);
void *fifoPeekStart(fifo_t *fifo);
void fifoPeekEnd(fifo_t *fifo);
void *fifoWriteStart(fifo_t *fifo);
void* fifoWriteStartN(fifo_t *fifo, int rate);
void fifoWriteEnd(fifo_t *fifo);
void fifoSetCommandQueue(fifo_t *fifo, cl_command_queue commands);
int fifoGetTokenSize(fifo_t *fifo);
int fifoGetType(fifo_t *fifo);
void* fifoTokenGet(fifo_t *fifo, int index);

