#include "clinit.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>

void loadKernelFile(char const *name, char **buffer) {
	FILE *fp;
	fp = fopen(name, "r");
	if (!fp) {
		fprintf(stderr, "Error loading kernel file %s\n", name);
		exit(1);
	}

	size_t kernel_sz = getFileSize(fp);
	buffer[0] = (char *)malloc(kernel_sz + 1);
	if (fread(buffer[0], 1, kernel_sz, fp) != kernel_sz) {
		printf("Problem in reading kernel file %s\n", name);
	}
	buffer[0][kernel_sz] = '\0';
	fclose(fp);
}

int contextInit(cl_platform_id *platform, cl_context *context, cl_device_id *devices,
				cl_int platformId, cl_int devId) {
	#ifndef DISABLE_OPENCL
	int err = 0;
	cl_uint numPlatforms;
	cl_uint numDevices;

	err |= clGetPlatformIDs(0, NULL, &numPlatforms);

	err |= clGetPlatformIDs(numPlatforms, platform, NULL);

	if ((cl_uint) platformId >= numPlatforms) {
		printf("Platform #%i is not present\n", platformId);
		return EXIT_FAILURE;
	} else {
		err |= clGetDeviceIDs(platform[platformId], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);
		if (!err) {
			char name[64];
			clGetPlatformInfo(platform[platformId], CL_PLATFORM_NAME, 64, name, NULL);
			printf("Using platform %s\n", name);
		} else {
			printf("Error %i in clGetDeviceIDs\n", err);
		}
	}

	if ((cl_uint) devId >= numDevices) {
		printf("Device #%i of platform #%i is not present\n", devId, platformId);
		return EXIT_FAILURE;
	} else {
		err |= clGetDeviceIDs(platform[platformId], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);
		if (!err) {
			char name[64];
			clGetDeviceInfo(devices[devId], CL_DEVICE_NAME, 64, name, NULL);
			printf("Using device %s\n", name);
		} else {
			printf("Error %i in clGetDeviceIDs\n", err);
		}
	}

	context[0] = clCreateContext(0, 1, &devices[devId], NULL, NULL, &err);
	if (!context[0]) {
		printf("Error: Failed to create a compute context!\n");
		return EXIT_FAILURE;
	}
	#endif
	return 0;
}

int loadProgram(cl_context context, cl_device_id device, cl_program *program,
				char const *kernelPath, char const *includePath, float clVersion) {
	#ifndef DISABLE_OPENCL
	int err;
	char *kernelSource;
	char clParams[256];

	loadKernelFile(kernelPath, &kernelSource);

	program[0] = clCreateProgramWithSource(context, 1, (const char **)&kernelSource, NULL, &err);
	if (!program[0]) {
		printf("Error: Failed to create compute program!\n");
		return EXIT_FAILURE;
	}

	free(kernelSource);

	sprintf(clParams, "-cl-std=CL%1.1f %s", clVersion, includePath);
	err = clBuildProgram(program[0], 0, NULL, clParams, NULL, NULL);
	if (err != CL_SUCCESS) {
		printf("Error: Failed to build program executable! (%i)\n", err);
				size_t len;
				size_t buffer_size = 1000000;
				char buffer[1000000];

				err = clGetProgramBuildInfo(program[0], device, CL_PROGRAM_BUILD_LOG, buffer_size,
		   buffer, &len); printf("Log (%i):%s\n", err, buffer);
		exit(1);
	}
	#endif
	return 0;
}
