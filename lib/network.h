#pragma once

#include "actor.h"
#include "fifo.h"

typedef struct {
	actor_t *actors;
	int actorCount;

	fifo_t *fifos;
	int fifoCount;
} network_t;

void networkReset(network_t *network);
void networkFree(network_t *network);
int networkAddActor(network_t *network, char const *name);
int networkAddFifo(network_t *network, char const *name);
void networkAddConnection(network_t *network, int source, int sink, int fifo);
int networkGetActorCount(network_t *network);
void networkLaunchActors(network_t *network);
