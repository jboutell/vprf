#include "common.h"
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

int getFileSize(FILE *fp) {
	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	rewind(fp);
	return size;
}

void writeInputFilepath(const char *filename, char *fullpath) {
	// Attempt to generate using the pre-processor directive
	sprintf(fullpath, "%s/%s", INPUT_FILE_SEARCH_PATH, filename);

	// File can be read -> return
	if (access(fullpath, R_OK) != -1) {
		return;
	}
	printf("Input file at \"%s\" not found, falling back to %s/%s\n", fullpath, getenv("HOME"),
		   filename);

	// Generate path using $HOME as fallback
	sprintf(fullpath, "%s/%s", getenv("HOME"), filename);

	if (access(fullpath, R_OK) != -1) {
		return;
	}

	// Finally, fall back to using the original filename
	strcpy(fullpath, filename);
}

void writeOutputFilepath(const char *filename, char *fullpath) {
	DIR *dir = opendir(OUTPUT_FILE_DIRECTORY);

	// Directory exists -> generate & return
	if (dir) {
		closedir(dir);
		// Generate using the pre-processor directive
		sprintf(fullpath, "%s/%s", OUTPUT_FILE_DIRECTORY, filename);
		return;
	}
	printf("Output directory at \"%s\" does not exist, falling back to %s/%s\n",
		   OUTPUT_FILE_DIRECTORY, getenv("HOME"), filename);

	// Generate path using $HOME as fallback
	sprintf(fullpath, "%s/%s", getenv("HOME"), filename);

	if (access(fullpath, R_OK) != -1) {
		return;
	}

	// Finally, fall back to using the original filename
	strcpy(fullpath, filename);
}

FILE *fopenInput(const char *filename) {
	char fullpath[256];
	writeInputFilepath(filename, fullpath);
	return fopen(fullpath, "rb");
}

FILE *fopenOutput(const char *filename) {
	char fullpath[256];
	writeOutputFilepath(filename, fullpath);
	return fopen(fullpath, "w");
}

int readRawData(char *fn, void **wgt) {
	FILE *file = fopen(fn, "r");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return 0;
	}
	int len = getFileSize(file);
	if (len > 0) {
		wgt[0] = (void *)malloc(len);
		int cnt = fread(wgt[0], 1, len, file);
		if (cnt != len) {
			printf("Warning: while reading '%s': %i of %i samples acquired\n", fn, cnt, len);
		}
	}
	fclose(file);
	return len;
}

int readConstant(char *fn, void *val, int size) {
	FILE *file = fopen(fn, "rb");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return 0;
	}
	int len = getFileSize(file);
	if (len != size) {
		printf("Warning: constant in '%s' has %i instead of %i bytes.\n", fn, size, len);
	}
	int cnt = fread(val, 1, size, file);
	if (cnt != len) {
		printf("Warning: while reading '%s': %i of %i samples acquired\n", fn, cnt, len);
	}
	fclose(file);
	return len;
}

void actorTerminate() {
	pthread_exit(NULL);
}

