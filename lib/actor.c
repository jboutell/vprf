#ifndef _GNU_SOURCE
  #define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sched.h>

#include "actor.h"
#include "profile.h"

void actorReset(actor_t *actor) {
	actor->device = -1;
	actor->iteration = 0;
	actor->core = -1;

	actor->inputs = NULL;
	actor->inputCount = 0;

	actor->outputs = NULL;
	actor->outputCount = 0;

	actor->data = NULL;
}

void actorSetName(actor_t *actor, char const *name) {
	actor->name = name;
}

void actorFree(actor_t *actor) {
	#ifndef DISABLE_OPENCL
	if (actor->device == T_GPU) {
		gpu_actor_params_t *params = (gpu_actor_params_t *)actor->data;
		clReleaseKernel(params->kernel);
		if (params->constantData != NULL) {
			for (int i = 0; i < params->constantDataCount; i++) {
				clReleaseMemObject(params->constantData[i]);
			}
			free (params->constantData);
		}
	}
	#endif
	free(actor->outputs);
	actor->outputCount = 0;
	free(actor->inputs);
	actor->inputCount = 0;
}

void actorAllocate(actor_t *actor, int device, void *data) {
	actor->device = device;
	actor->data = data;
	if (actor->device == T_GPU) {
		gpu_actor_params_t *params = (gpu_actor_params_t *)actor->data;
		params->constantDataCount = 0;
		params->constantData = NULL;
	}
}

void actorAddInput(actor_t *actor, fifo_t *fifo) {
	int inputIndex;
	inputIndex = actor->inputCount;
	actor->inputCount++;
	actor->inputs = (fifo_t **)realloc(actor->inputs, sizeof(fifo_t *) * actor->inputCount);
	actor->inputs[inputIndex] = fifo;
}

void actorAddOutput(actor_t *actor, fifo_t *fifo) {
	int outputIndex;
	outputIndex = actor->outputCount;
	actor->outputCount++;
	actor->outputs = (fifo_t **)realloc(actor->outputs, sizeof(fifo_t *) * actor->outputCount);
	actor->outputs[outputIndex] = fifo;
}

void actorResolveIO(actor_t *actor, gpu_actor_params_t *params, io_data_t *ioData) {
	int *cBuffer = (int *)fifoPeekStart(actor->inputs[0]);
	ioData->cValue = cBuffer[0];
	fifoPeekEnd(actor->inputs[0]);
	(*params->ioFunction)(ioData);
}

void actorFire(actor_t *actor) {
#ifdef PROFILING
	unsigned long long start, end;
#endif
	if (actor->device == T_CPU) {
		cpu_actor_params_t *params = (cpu_actor_params_t *)actor->data;
#ifdef PROFILING
		start = timestamp();
#endif
		(*params->function)(params->appData);
#ifdef PROFILING
		end = timestamp();
#endif
	} else if (actor->device == T_GPU) {
		#ifndef DISABLE_OPENCL
		gpu_actor_params_t *params = (gpu_actor_params_t *)actor->data;
		int arg = 0;

		io_data_t ioData;
		if (params->isDynamic) {
			actorResolveIO(actor, params, &ioData);
			for (int j = 0; j < actor->inputCount; j++) {
				if (ioData.inputEnabled[j]) {
					cl_mem token = (cl_mem)fifoReadLock(actor->inputs[j]);
					clSetKernelArg(params->kernel, arg, sizeof(token), &token);
				}
				arg++;
			}
			for (int j = 0; j < actor->outputCount; j++) {
				if (ioData.outputEnabled[j]) {
					cl_mem token = (cl_mem)fifoWriteLock(actor->outputs[j]);
					clSetKernelArg(params->kernel, arg, sizeof(token), &token);
				}
				arg++;
			}
		} else {
			for (int j = 0; j < actor->inputCount; j++) {
				cl_mem token = (cl_mem)fifoReadLock(actor->inputs[j]);
				clSetKernelArg(params->kernel, arg++, sizeof(token), &token);
			}
			for (int j = 0; j < actor->outputCount; j++) {
				cl_mem token = (cl_mem)fifoWriteLock(actor->outputs[j]);
				clSetKernelArg(params->kernel, arg++, sizeof(token), &token);
			}
		}
#ifdef DBGPRINT
		printf("actor %s fires %i\n", actor->name, actor->iteration);
#endif
#ifdef PROFILING
		start = timestamp();
#endif
		int err = clEnqueueNDRangeKernel(params->commands, params->kernel, params->dimensions, NULL,
										 params->globalSize, params->localSize, 0, NULL, NULL);
		clFinish(params->commands);
		if (err)
			printf("%s:%i\n", actor->name, err);
#ifdef PROFILING
		end = timestamp();
#endif
		if (params->isDynamic) {
			for (int j = 0; j < actor->inputCount; j++) {
				if (ioData.inputEnabled[j]) {
					fifoReadUnlock(actor->inputs[j]);
				}
			}
			for (int j = 0; j < actor->outputCount; j++) {
				if (ioData.outputEnabled[j]) {
					fifoWriteUnlock(actor->outputs[j]);
				}
			}
		} else {
			for (int j = 0; j < actor->inputCount; j++) {
				fifoReadUnlock(actor->inputs[j]);
			}
			for (int j = 0; j < actor->outputCount; j++) {
				fifoWriteUnlock(actor->outputs[j]);
			}
		}
		#endif
	}
#ifdef PROFILING
	if (actor->iteration < PROFILE_DEPTH) {
		actor->ttimes[actor->iteration].start = start;
		actor->ttimes[actor->iteration].end = end;
		actor->ttimes[actor->iteration].valid = 1;
		actor->ttimes[actor->iteration].queue = actor->device + actor->core;
		actor->ttimes[actor->iteration].it = actor->iteration;
	}
#endif
	actor->iteration++;
}

void *actorWrapper(void *p) {
	actor_t *actor = (actor_t *)p;
	if (actor->core >= 0) {
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(actor->core, &cpuset);
		sched_setaffinity(0, sizeof(cpuset), &cpuset);
	}
#ifdef DBGPRINT
	printf("%s launched\n", actor->name);
#endif
	while (!globalApplicationFinished) {
		actorFire(actor);
	}
#ifdef DBGPRINT
	printf("%s done with %i iterations\n", actor->name, actor->iteration);
#endif

	while (1) {
		usleep(10);
	}

	return p;
}

void actorSetCore(actor_t *actor, int core) {
	actor->core = core;
}

void actorInitializeShared(actor_t *actor, shared_t *shared) {
	shared->inputs = (fifo_t **)malloc(sizeof(fifo_t *) * actor->inputCount);
	shared->outputs = (fifo_t **)malloc(sizeof(fifo_t *) * actor->outputCount);
	for (int i = 0; i < actor->inputCount; i++) {
		shared->inputs[i] = actor->inputs[i];
	}
	for (int i = 0; i < actor->outputCount; i++) {
		shared->outputs[i] = actor->outputs[i];
	}
}

void actorFreeShared(shared_t *shared) {
	free(shared->inputs);
	free(shared->outputs);
}

void actorSetGPUParams(gpu_actor_params_t *params, cl_command_queue commands, cl_kernel kernel,
					   int dimensions, size_t *globalSize, size_t *localSize) {
	params->kernel = kernel;
	params->globalSize = globalSize;
	params->localSize = localSize;
	params->commands = commands;
	params->dimensions = dimensions;
	params->isDynamic = 0;
}

void actorSetIoFunction(actor_t *actor, gpu_actor_params_t *params,
						PROCESS_RETURN_T (*ioFunction)(PROCESS_PTR_T)) {
	// FIXME: handle errors from clSetKernelArg
	#ifndef DISABLE_OPENCL
	int arg = 0;
	for (int j = 0; j < actor->inputCount; j++) {
		clSetKernelArg(params->kernel, arg++, sizeof(NULL), NULL);
	}
	for (int j = 0; j < actor->outputCount; j++) {
		clSetKernelArg(params->kernel, arg++, sizeof(NULL), NULL);
	}
	#endif
	params->isDynamic = 1;
	params->ioFunction = ioFunction;
}

void actorSetConstantData(cl_context context, gpu_actor_params_t *params, int argInd,
						  const char *name) {
	#ifndef DISABLE_OPENCL
	int err;
	const int fourByte = 4;
	char fileName[256];
	sprintf(fileName, "%s", name);
	FILE *f = fopen(fileName, "rb");
	if (f == NULL) {
		printf("Could not open %s.\n", name);
		return;
	}
	int size = getFileSize(f);
	fclose(f);
	params->constantData = (cl_mem *) realloc(params->constantData, sizeof(cl_mem) * (params->constantDataCount+1));
	if (size == fourByte) { // we assume this is a scalar
		int val;
		readConstant(fileName, &val, fourByte);
		err = clSetKernelArg(params->kernel, argInd, fourByte, &val);
		params->constantData[params->constantDataCount] = NULL;
	} else {
		void *hostArr;
		readRawData(fileName, &hostArr);
		if (size == 0) {
			size = sizeof(void *);
			hostArr = malloc(sizeof(void*));
		}
		params->constantData[params->constantDataCount] = clCreateBuffer(context, CL_MEM_READ_ONLY, size, NULL, NULL);
		err = clEnqueueWriteBuffer(params->commands, params->constantData[params->constantDataCount], CL_TRUE, 0, size,
									   hostArr, 0, NULL, NULL);
		clFinish(params->commands);
		if (err != CL_SUCCESS) {
			printf("Error: clEnqueueWriteBuffer (%i) failed for %s!\n", err, name);
			exit(1);
		}
		err =
			clSetKernelArg(params->kernel, argInd, sizeof(params->constantData[params->constantDataCount]),
			&params->constantData[params->constantDataCount]);
		free(hostArr);
	}
	if (err != CL_SUCCESS) {
		printf("Error: clSetKernelArg failed (%i) for %s!\n", err, name);
		exit(1);
	}
	params->constantDataCount++;
	#endif
}
