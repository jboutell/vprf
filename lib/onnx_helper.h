#ifdef ONNX

#include "onnx.h"
#include "default/default.h"

#define STR_MAX 256

void onnx_node_init(struct onnx_node_t *n, int opset, int n_input, int n_output, Onnx__NodeProto* proto);
void onnx_tensor_init(struct onnx_tensor_t *t, const char *name, enum onnx_tensor_type_t type, int ndim);
void onnx_node_resolve(struct onnx_node_t *n);
void onnx_tensor_load(char *name, struct onnx_tensor_t *t);
void onnx_node_free(struct onnx_node_t *n);
unsigned int onnx_tensor_checksum(struct onnx_tensor_t *t);
unsigned int onnx_tensor_allocate(struct onnx_tensor_t *t);
void resolver_solve_operator(struct onnx_resolver_t * r, struct onnx_node_t * n);

#endif
