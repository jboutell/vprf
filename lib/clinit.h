#pragma once

#include <CL/opencl.h>

int contextInit(cl_platform_id *platform, cl_context *context, cl_device_id *devices,
				cl_int platformId, cl_int devId);
int loadProgram(cl_context context, cl_device_id device, cl_program *program,
				char const *kernelPath, char const *includePath, float clVersion);
