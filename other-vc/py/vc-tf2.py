# Vehicle classifier for Tensorflow 2
# Authors: Mir Khan, mir.khan@tuni.fi
#          Jani Boutellier, jani.boutellier@uwasa.fi

import os
import glob
import socket
import numpy as np
from numpy import reshape, shape
import time
import tensorflow as tf

eps=np.finfo(np.float).eps

import tensorflow as tf
from tensorflow import zeros, float32, reshape
tf.config.run_functions_eagerly(True)

@tf.function
def conv2d(x, W):
  return tf.nn.conv2d(x,
                      W,
                      strides=[1, 1, 1, 1],
                      padding='SAME')

@tf.function
def max_pool_2x2(x):
  return tf.nn.max_pool(x,
                        ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1],
                        padding='SAME')

def readRaw4D(filename, size):
    wcomp_raw = np.fromfile(filename, dtype='float32');
    flipped = np.fliplr(np.flip(np.reshape(wcomp_raw, size, order='F'),0));
    return np.transpose(flipped, axes=[1,0,2,3]).astype('float32');

def readRaw3D(fileName, X, Y, Z, off): # 96, 96, 3
    D_vector1 = np.fromfile(fileName, dtype='float32', count=X*Y*Z, offset=off);
    D_matrix2 = np.reshape(D_vector1, [X, Y, Z], order='F');
    D_rotated = np.rot90(D_matrix2,-1);
    D_mirrored = np.fliplr(D_rotated);
    return D_mirrored;

def readRaw2D(filename, size):
    wcomp_raw = np.fromfile(filename, dtype='float32');
    return np.reshape(wcomp_raw, size, order='F');

def getTestData(offset):
    X_test=[];

    X_plaf = readRaw3D('data/c384.bin', 96, 96, 3, offset);

    X_test.append(X_plaf);
    return np.asarray(X_test)

@tf.function(experimental_compile=True)
def testgraph(X, w1, w2, W_d1, W_d2, W_out):
	W_conv1 = tf.Variable(w1, dtype=tf.float32);
	h_conv1 = tf.nn.relu(conv2d(X, W_conv1));
	h_pool1 = max_pool_2x2(h_conv1);

	W_conv2 = tf.Variable(w2,dtype=tf.float32);
	h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2)); 
	h_pool2 = max_pool_2x2(h_conv2);

	h_pool2_flat=tf.reshape(tf.transpose(tf.reshape(tf.reshape(h_pool2,[24,24,32]),[24*24,32])), [1, 24*24*32]);
	h_d1 = tf.nn.relu(tf.matmul(h_pool2_flat, tf.Variable(W_d1,dtype=tf.float32)));

	h_d2 = tf.nn.relu(tf.matmul(h_d1, tf.Variable(W_d2,dtype=tf.float32)));

	y_out = tf.matmul(h_d2, tf.Variable(W_out,dtype=tf.float32));
	return y_out;


#____________________________________
#------------- main() ------------- #
#____________________________________

print('Constructing network\n');
w1 = readRaw4D('parameter/conv1_update.bin', [5,5,3,32]);
w2 = readRaw4D('parameter/conv2_update.bin', [5,5,32,32]);
W_d1 = readRaw2D('parameter/ip3.bin',[24*24*32,100]);
W_d2 = readRaw2D('parameter/ip4.bin',[100,100]);
W_out = readRaw2D('parameter/ip_last.bin',[100,4]);
offset = 0;

start = time.time()
for x in range(1, 384):
    dev_im = getTestData(offset);
    res = testgraph(dev_im, w1, w2, W_d1, W_d2, W_out);
    #print(np.round(tf.nn.softmax(res), 4));
    offset = offset + 96*96*3*4;
end = time.time()

#print('computation time')   
print(end-start)	

