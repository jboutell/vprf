import os
import glob
import socket
import numpy as np
from numpy import reshape, shape
import time
import tensorflow as tf
eps=np.finfo(np.float).eps

from tensorflow import Session, zeros, float32, reshape

def conv2d(x, W):
  return tf.nn.conv2d(x,
                      W,
                      strides=[1, 1, 1, 1],
                      padding='SAME')

def max_pool_2x2(x):
  return tf.nn.max_pool2d(x,
                        ksize=[1, 2, 2, 1],
                        strides=[1, 2, 2, 1],
                        padding='SAME')

def readRaw4D(filename, size):
    wcomp_raw = np.fromfile(filename, dtype='float32');
    flipped = np.fliplr(np.flip(np.reshape(wcomp_raw, size, order='F'),0));
    return np.transpose(flipped, axes=[1,0,2,3]).astype('float32');

def readRaw3D(fileName, X, Y, Z): # 96, 96, 3
    D_vector1 = np.fromfile(fileName, dtype='float32');
    D_matrix2 = np.reshape(D_vector1, [X, Y, Z], order='F');
    D_rotated = np.rot90(D_matrix2,-1);
    D_mirrored = np.fliplr(D_rotated);
    return D_mirrored;

def readRaw2D(filename, size):
    wcomp_raw = np.fromfile(filename, dtype='float32');
    return np.reshape(wcomp_raw, size, order='F');

def getTestData():
    X_test=[];

    X_plaf = readRaw3D('../data/c.bin', 96, 96, 3);

    X_test.append(X_plaf);
    return np.asarray(X_test)

def testgraph(X, w1, w2, W_d1, W_d2, W_out):
	W_conv1 = tf.Variable(w1, dtype=tf.float32);
	h_conv1 = tf.nn.relu(conv2d(X, W_conv1));
	h_pool1 = max_pool_2x2(h_conv1);

	W_conv2 = tf.Variable(w2,dtype=tf.float32);
	h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2)); 
	h_pool2 = max_pool_2x2(h_conv2);

	h_pool2_flat=tf.reshape(tf.transpose(tf.reshape(tf.reshape(h_pool2,[24,24,32]),[24*24,32])), [1, 24*24*32]);
	h_d1 = tf.nn.relu(tf.matmul(h_pool2_flat, tf.Variable(W_d1,dtype=tf.float32)));

	h_d2 = tf.nn.relu(tf.matmul(h_d1, tf.Variable(W_d2,dtype=tf.float32)));

	y_out = tf.matmul(h_d2, tf.Variable(W_out,dtype=tf.float32));
	return y_out;

#____________________________________
#------------- main() ------------- #
#____________________________________

#first_time=True;
print('Constructing network\n');

dev_im = getTestData();

in_w = 96;
in_h = 96;
out_dim = 4;

X = tf.compat.v1.placeholder(tf.float32, shape=[None,in_w,in_h,3]);
y = tf.compat.v1.placeholder(tf.float32, shape=[None, out_dim]);
w1 = readRaw4D('../parameter/conv1_update.bin', [5,5,3,32]);
w2 = readRaw4D('../parameter/conv2_update.bin', [5,5,32,32]);
W_d1 = readRaw2D('../parameter/ip3.bin',[24*24*32,100]);
W_d2 = readRaw2D('../parameter/ip4.bin',[100,100]);
W_out = readRaw2D('../parameter/ip_last.bin',[100,4]);

start = time.time()

y_out = testgraph(dev_im, w1, w2, W_d1, W_d2, W_out);

end = time.time()
print('computation time')
print(end-start)

with tf.Session() as session:
    session.run(tf.compat.v1.global_variables_initializer());
    res = session.run(y_out, feed_dict={X:dev_im})
    print('y_out for white truck image')
    print(np.round(res, 4));

