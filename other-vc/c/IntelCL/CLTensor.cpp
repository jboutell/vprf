/*************************************************************************\
* Minimal ARMCL emulation library for general purpose Linux architectures *
* Authors: Jani Boutellier, jani.boutellier@uwasa.fi                      *
*          Mir Khan, mir.khan@tuni.fi                                     *
\*************************************************************************/

#include "CLFunctions.h"
#include "CLTensor.h"
#include "Types.h"
#include "dnn.h"

namespace arm_compute
{
	TensorShape::TensorShape(unsigned int){}
	TensorShape::TensorShape(unsigned int, unsigned int){}
	TensorShape::TensorShape(unsigned int, unsigned int, unsigned int){}
	TensorShape::TensorShape(unsigned int, unsigned int, unsigned int, unsigned int){}

	CLTensor::CLTensor(){}

	void CLTensor::unmap(){}

	void CLTensor::allocate(unsigned int size) {
		data = (float *) malloc (sizeof(float)*size);
	}

	void CLConvolutionLayer::configure(float *new_input, float *new_weights, float *biases, float *new_output, bool islayer1, void* weights_info) {
		input = new_input;
		weights = new_weights;
		output = new_output;
		layer1 = islayer1;
	}

	void CLConvolutionLayer::configure(float *new_input, float *new_weights, float *biases, float *new_output, bool islayer1) {
		input = new_input;
		weights = new_weights;
		output = new_output;
		layer1 = islayer1;
	}

	void CLConvolutionLayer::run() {
		if (layer1) {
			conv_relu_1(input, output, weights);
		} else {
			conv_relu_2(input, output, weights);
		}
	}

	void CLActivationLayer::configure(float *new_input, float *new_output, void *act_info, int row, int column) {
		input = new_input;
		output = new_output;
		r = row;
		c = column;
	}

	void CLActivationLayer::run() {
		lide_c_image_relu(input, output, r, c);
	}

	void CLPoolingLayer::configure(float *new_input, float *new_output, void *pool_info) {
		input = new_input;
		output = new_output;
	}

	void CLPoolingLayer::run() {
	}

	void CLFullyConnectedLayer::configure(float *new_input, float *new_weights, float *biases, float *new_output, int Mdim, int Ndim, int Kdim) {
		input = new_input;
		weights = new_weights;
		output = new_output;
		M = Mdim;
		N = Ndim;
		K = Kdim;
	}

	void CLFullyConnectedLayer::run() {
		lide_c_mtx_mulf(weights, input, output, M, N, K);
	}

	void CLSoftmaxLayer::configure(float *new_input, float *new_output, int mm, int nn) {
		input = new_input;
		output = new_output;
		m = mm;
		n = nn;
	}

	void CLSoftmaxLayer::run() {
		lide_c_softmax_write (output, input, m, n);
	}
}
