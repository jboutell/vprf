#include <stdio.h>

typedef struct {
	FILE *file;
	char *fn;
	int iteration;
	int repetitions;
	int length;
} source_data_t;

void sourceInit(source_data_t *data);
void sourceFinish(source_data_t *data);


