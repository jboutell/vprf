#include <string.h>
#include "sink.h"

void sinkInit(sink_data_t *data) {
	data->file = NULL;
	data->file = fopen (data->fn, "w");
	if (data->file == NULL) {
		printf("Could not open %s\n", data->fn);
		return;
	}
	if (data->repetitions == 0) {
		printf("Warning: number of repetitions uninitialized\n");
	}
	data->iteration = 0;
}

void sinkFinish(sink_data_t *data) {
	if (data->file != NULL) {
		fflush (data->file);
		fclose (data->file);
		data->file = NULL;
	}
}

