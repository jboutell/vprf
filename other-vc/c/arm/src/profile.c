#include "profile.h"
#include <sys/time.h>

unsigned int calibrate() {
	struct timezone tz;
	struct timeval tvs;
	struct timeval tve;

	// find out how long two repeated gettimeofdays take
	gettimeofday(&tvs, &tz);
	gettimeofday(&tve, &tz);

	return (unsigned int) ((tve.tv_sec * 1000000L + tve.tv_usec) - (tvs.tv_sec * 1000000L + tvs.tv_usec));
}

unsigned long long timestamp() {
	struct timezone tz;
	struct timeval tvs;

	gettimeofday(&tvs, &tz);

	return tvs.tv_sec * 1000000L + tvs.tv_usec;
}

