/*************************************************************************\
* Minimal ARMCL emulation library for general purpose Linux architectures *
* Authors: Jani Boutellier, jani.boutellier@uwasa.fi                      *
*          Mir Khan, mir.khan@tuni.fi                                     *
\*************************************************************************/

Associated CLHelper.c and CLHelper.h files in ../src/ folder

To avoid copyright related issues, detailed content of this folder is 
withheld, however author 1 can provide more information.

This folder should contain the following files that are part of the ARMCL 
library source:

CLActivationLayer.h
CLConvolutionLayer.h
CLFullyConnectedLayer.h
CLFunctions.h
CLPoolingLayer.h
CLSoftmaxLayer.h
CLTensorAllocator.h
CLTensor.cpp
CLTensor.h
ICLTensor.h
ITensorAllocator.h
Size2D.h
TensorInfo.h
TensorShape.h
Types.h

However, from each file several sections need to be commented out for
compatibility with CLHelper.c and CLHelper.h
