#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "cnn.h"

void pad (float *input, float *fifo, int width, int padding, int channels) {
	int cnt = 0;
	int pad_width = width + (2*padding);
	int patch1sqpad = pad_width*pad_width;
	for (int c = 0; c < channels; c++) {
		float *fifop = &fifo[c*patch1sqpad];
		for (int i = padding; i < (width + padding); i++) {
			for (int j = padding; j < (width + padding); j++) {
			    fifop[(i * pad_width) + j] = input[cnt++];
			}
		}
	}
}

void conv_relu_1 (float * fifo_in, float * fifo_out, float * wgt) {

	const int nInWidth = PATCH1 + (2 * PAD_NUM);
	const int nWidth = PATCH1;
	const int nHeight = PATCH1;
 	const int nFilterWidth = CONV1SIZE;
	float *tmp_in = (float*) malloc(sizeof(float)*100*100*3);
	memset(tmp_in, 0, sizeof(float)*100*100*3);
	pad(fifo_in, tmp_in, 96, 2, 3);


	for (int fm = 0; fm < FM_COUNT; fm++) {

		for (int row = 0; row < nHeight; row += PAD_NUM) {
			for (int col = 0; col < nWidth; col += PAD_NUM) {

				float gssample[PAD_NUM*PAD_NUM] = {0};
				for (int c = 0; c < CHANNELS; c++) {
					for (int rst = 0; rst < PAD_NUM; rst ++) {
						for (int cst = 0; cst < PAD_NUM; cst ++) {

							for (int i = 0; i < nFilterWidth; i++) {
								for (int j = 0; j < nFilterWidth; j++) {
									gssample[rst*PAD_NUM + cst] +=
										matrix1(tmp_in, (c*PATCH1SQPAD), nInWidth, (row+rst+i), (col+cst+j)) *
										matrix1(wgt, (CONV1SQ * CHANNELS * fm + c * CONV1SQ), nFilterWidth, (i), (j));
								}
							}

						}
					}
				}

				float tmp = 0;
				tmp = tmp > gssample[0] ? tmp : gssample[0];
				tmp = tmp > gssample[1] ? tmp : gssample[1];
				tmp = tmp > gssample[2] ? tmp : gssample[2];
				tmp = tmp > gssample[3] ? tmp : gssample[3];
				matrix1(fifo_out, fm*PATCH2SQ, PATCH2, row/2, col/2) = tmp;

			} // col
		} // row

	} // fm
	free(tmp_in);
}


void conv_relu_2 (float * fifo_in, float * fifo_out, float * wgt) {

	const int nInWidth = PATCH2 + (2 * PAD_NUM);
	const int nWidth = PATCH2;
	const int nHeight = PATCH2;
 	const int nFilterWidth = CONV2SIZE;
	float *tmp_in = (float*) malloc(sizeof(float)*52*52*32);
	memset(tmp_in, 0, sizeof(float)*52*52*32);
	pad(fifo_in, tmp_in, 48, 2, 32);

	for (int fm = 0; fm < FM_COUNT; fm++) {

		for (int row = 0; row < nHeight; row += PAD_NUM) {
			for (int col = 0; col < nWidth; col += PAD_NUM) {

				float gssample[PAD_NUM*PAD_NUM] = {0};
				for (int c = 0; c < FM_COUNT; c++) {
					for (int rst = 0; rst < PAD_NUM; rst ++) {
						for (int cst = 0; cst < PAD_NUM; cst ++) {

							for (int i = 0; i < nFilterWidth; i++) {
								for (int j = 0; j < nFilterWidth; j++) {
									gssample[rst*PAD_NUM + cst] +=
										matrix1(tmp_in, (c*PATCH2SQPAD), nInWidth, (row+rst+i), (col+cst+j)) *
										matrix1(wgt, (CONV2SQ * FM_COUNT * fm + c * CONV2SQ), nFilterWidth, (i), (j));
								}
							}

						}
					}
				}

				float tmp = 0;
				tmp = tmp > gssample[0] ? tmp : gssample[0];
				tmp = tmp > gssample[1] ? tmp : gssample[1];
				tmp = tmp > gssample[2] ? tmp : gssample[2];
				tmp = tmp > gssample[3] ? tmp : gssample[3];
		        matrix1(fifo_out, fm*PATCH3SQ, (PATCH2/TILE_NUM), (row/2), (col/2)) = tmp;

			} // col
		} // row

	} // fm
	free(tmp_in);
}

void lide_c_mtx_mulf(float* A, float* B, float* c_mul, int Mdim, int Ndim, int Kdim)
{
    int i, j, z;
	memset (c_mul, 0, sizeof(float) * Mdim * Kdim);
    for (i = 0; i < Mdim; i++)
    {
        for (j = 0; j < Kdim; j++)
        {
            for (z = 0; z < Ndim; z++)
            {
                matrix(c_mul, Kdim, i, j) += matrix(A, Ndim, i, z) * matrix(B, Kdim, z, j);
            }
        }
    }
}

void lide_c_image_relu(float *bef_array, float *aft_array, int row, int column)
{
    int i,j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < column; j++)
        {
            float tmp = 0;
            tmp = tmp > matrix(bef_array, column, i, j)? tmp : matrix(bef_array, column, i, j);
            matrix(aft_array, column, i, j) = tmp;
        }

    }
}

void lide_c_softmax_write(float *output_array, float *input_array, int m, int n) {
    int i,j;
	int ind = 0;
    float sum = 0.0;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            sum += exp(matrix(input_array, n, i, j));
        }
    }
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
			output_array[ind++] = exp(matrix(input_array, n, i, j)) / sum;
        }
    }
}

void writer(FILE *f, float *input_array, int len) {
	for (int i = 0; i < len; i++) {
		fprintf(f, "%f\n", input_array[i]);
	}
}

