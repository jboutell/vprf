/*************************************************************************\
* Minimal ARMCL emulation library for general purpose Linux architectures *
* Authors: Mir Khan, mir.khan@tuni.fi                                     *
*          Jani Boutellier, jani.boutellier@uwasa.fi                      *
\*************************************************************************/

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
  #include "arm_compute/core/Types.h"
#else
  #include "IntelCL/CLTensor.h"
#endif

using namespace arm_compute;

int getFileSize(FILE *fp);
CLTensor* CreateTensor1D(unsigned int x);
CLTensor* CreateTensor2D(unsigned int x, unsigned int y);
CLTensor* CreateTensor3D(unsigned int x, unsigned int y, unsigned int z);
CLTensor* CreateTensor4D(unsigned int x, unsigned int y, unsigned int z, unsigned int w);
void FillTensor(CLTensor* X);
int LoadTensor4D(CLTensor* X, char* fn, int width, int height, int batch, int fm, bool map);
int LoadTensor3D(CLTensor* X, char* fn, int width, int height, int batch, bool map);
int LoadTensor2D(CLTensor* X, char* fn, int width, int height, bool map);
int StreamTensor(CLTensor* X, FILE *file, int width, int height, int batch, bool map);
void StreamOutput(FILE* file, CLTensor* X, int sz, bool map);



