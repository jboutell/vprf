#include "source.h"
#include <string.h>
#include "CLHelper.h"

void sourceInit(source_data_t *data) {
	data->file = NULL;
	data->file = fopen(data->fn, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", data->fn);
		return;
	}
	data->repetitions = getFileSize(data->file) / (data->length * sizeof(float));
	data->iteration = 0;
	printf("Running for %i images\n", data->repetitions);
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

