#include <stdio.h>

typedef struct {
	FILE *file;
	int iteration;
	int repetitions;
	char *fn;
} sink_data_t;

void sinkInit(sink_data_t *data);
void sinkFinish(sink_data_t *data);

