/*************************************************************************\
* ARMCL vehicle classifier                                                *
* Authors: Mir Khan, mir.khan@tuni.fi                                     *
*          Jani Boutellier, jani.boutellier@uwasa.fi                      *
\*************************************************************************/

#ifdef ARM_API
  #include "arm_compute/runtime/CL/CLFunctions.h"
  #include "arm_compute/core/Types.h"
#else
  #include "IntelCL/CLTensor.h"
  #include "IntelCL/CLFunctions.h"
  #include "IntelCL/Types.h"
  #include "IntelCL/TensorShape.h"
#endif
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdarg.h>
#include <iostream>
#include <string.h>
#include "CLHelper.h"
#include "cnn.h"
#include "profile.h"
#include "sink.h"
#include "source.h"

using namespace arm_compute;

int main(int argc, char **argv)
{
	unsigned long long t1, t2, tsum = 0;
	unsigned int cal;
	float totalTime;

	source_data_t src;
	sink_data_t snk;
	CLConvolutionLayer conv1, conv2;
	CLPoolingLayer mxpool1, mxpool2;
	CLActivationLayer relu1, relu2, relu3, relu4;
	CLFullyConnectedLayer dense1, dense2, dense3;
	CLSoftmaxLayer softmax;

	if (argc < 3) {
		printf("vccl infile outfile\n");
		return 0;
	}

#ifdef ARM_API
	CLScheduler::get().default_init();
#endif

	printf("Initializing tensors\n");
	CLTensor* X = CreateTensor3D(96,96,3);
	CLTensor* weights1 = CreateTensor4D(5,5,3,32);
	CLTensor* weights2 = CreateTensor4D(5,5,32,32);
	CLTensor* weights3 = CreateTensor2D(32*24*24,100);
	CLTensor* weights4 = CreateTensor2D(100,100);
	CLTensor* weights5 = CreateTensor2D(100,4);
	CLTensor* out_conv1 = CreateTensor3D(96,96,32);
	CLTensor* out_relu1 = CreateTensor3D(96,96,32);
	CLTensor* out_conv2 = CreateTensor3D(48,48,32);
	CLTensor* out_relu2 = CreateTensor3D(48,48,32);
	CLTensor* out_relu3 = CreateTensor2D(100,1);
	CLTensor* out_relu4 = CreateTensor2D(100,1);
	CLTensor* out_mxpool1 = CreateTensor3D(48,48,32);
	CLTensor* out_mxpool2 = CreateTensor3D(24,24,32);
	CLTensor* out_dense1 = CreateTensor2D(100,1);
	CLTensor* out_dense2 = CreateTensor2D(100,1);
	CLTensor* out_dense3 = CreateTensor2D(4,1);
	CLTensor* out_softmax = CreateTensor2D(4,1);

	printf("Configuring kernels\n");
#ifdef ARM_API
	conv1.configure(X, weights1, NULL, out_conv1, PadStrideInfo(1,1,2,2), WeightsInfo());
	mxpool1.configure(out_relu1, out_mxpool1, PoolingLayerInfo(PoolingType::MAX, 2,PadStrideInfo(2,2,0,0)));
	relu1.configure(out_conv1, out_relu1, ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
	conv2.configure(out_mxpool1, weights2, NULL, out_conv2, PadStrideInfo(1,1,2,2));
	mxpool2.configure(out_relu2, out_mxpool2, PoolingLayerInfo(PoolingType::MAX, 2,PadStrideInfo(2,2,0,0)));
	relu2.configure(out_conv2, out_relu2, ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
	dense1.configure(out_mxpool2,weights3,NULL,out_dense1);
	relu3.configure(out_dense1, out_relu3, ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
	dense2.configure(out_relu3,weights4,NULL,out_dense2);
	relu4.configure(out_dense2, out_relu4, ActivationLayerInfo(ActivationLayerInfo::ActivationFunction::RELU));
	dense3.configure(out_relu4,weights5,NULL,out_dense3);
	softmax.configure(out_dense3, out_softmax);
#else
	conv1.configure(X->data, weights1->data, NULL, out_mxpool1->data, true, NULL);
	//relu1.configure(out_conv1->data, out_relu1->data, nullptr);
	//mxpool1.configure(out_relu1->data, out_mxpool1->data, nullptr);
	conv2.configure(out_mxpool1->data, weights2->data, NULL, out_mxpool2->data, false);
	//relu2.configure(out_conv2->data, out_relu2->data, nullptr);
	//mxpool2.configure(out_relu2->data, out_mxpool2->data, nullptr);
	dense1.configure(out_mxpool2->data,weights3->data,NULL,out_dense1->data, MAGIC, PATCH3SQ*FM_COUNT, 1);
	relu3.configure(out_dense1->data, out_relu3->data, NULL, MAGIC, 1);
	dense2.configure(out_relu3->data,weights4->data,NULL,out_dense2->data, MAGIC, MAGIC, 1);
	relu4.configure(out_dense2->data, out_relu4->data, NULL, MAGIC, 1);
	dense3.configure(out_relu4->data,weights5->data,NULL,out_dense3->data, CLASSES, MAGIC, 1);
	softmax.configure(out_dense3->data, out_softmax->data, 4, 1);
#endif

	printf("Loading data and allocating tensors\n");
	char fn_weights1[] = "data/conv1_flipped.bin";
	char fn_weights2[] = "data/conv2_flipped.bin";
	char fn_weights3[] = "data/ip3.bin";
	char fn_weights4[] = "data/ip4.bin";
	char fn_weights5[] = "data/ip_last.bin";
	LoadTensor4D(weights1, fn_weights1, 5, 5, 3, 32, true);
	LoadTensor4D(weights2, fn_weights2, 5, 5, 32, 32, true);
	LoadTensor2D(weights3, fn_weights3, 24*24*32, 100, true);
	LoadTensor2D(weights4, fn_weights4, 100, 100, true);
	LoadTensor2D(weights5, fn_weights5, 100, 4, true);

	printf("Allocating the rest of tensors\n");
	FillTensor(X);
	FillTensor(out_dense1);
	FillTensor(out_relu1);
	FillTensor(out_mxpool1);
	FillTensor(out_conv1);
	FillTensor(out_conv2);
	FillTensor(out_relu2);
	FillTensor(out_mxpool2);
	FillTensor(out_relu3);
	FillTensor(out_relu4);
	FillTensor(out_dense2);
	FillTensor(out_dense3);
	FillTensor(out_softmax);

	float snkToken[4];
	src.fn = argv[1];
	src.length = 96*96*3;
	sourceInit(&src);
	snk.fn = argv[2];
	snk.repetitions = src.repetitions;
	sinkInit(&snk);

	usleep(500000);
	cal = calibrate();

	for(int i = 0; i < src.repetitions; i++) {
		StreamTensor(X, src.file, 96, 96, 3, true);
		t1 = timestamp();
		#ifdef ARM_API
		conv1.run();
		relu1.run();
		mxpool1.run();
		conv2.run();
		relu2.run();
		mxpool2.run();
		#else
		conv1.run();
		conv2.run();
		#endif
		dense1.run();
		relu3.run();
		dense2.run();
		relu4.run();
		dense3.run();
		softmax.run();
		#ifdef ARM_API
		CLScheduler::get().sync();
		out_softmax->map();
		t2 = timestamp();
		StreamOutput(snk.file, out_softmax, 4, true);
		out_softmax->unmap();
		#else
		t2 = timestamp();
		StreamOutput(snk.file, out_softmax, 4, true);
		#endif
		tsum += t2 - t1;
	}
	totalTime = ((float) (((unsigned int)tsum)-cal)) / 1000000.0;
	printf("Computation time in main loop: %f\n", totalTime);

	sourceFinish(&src);
	sinkFinish(&snk);

#ifdef ARM_API
	weights1->allocator()->free();
	weights2->allocator()->free();
	weights3->allocator()->free();
	weights4->allocator()->free();
	weights5->allocator()->free();
	X->allocator()->free();
	out_conv1->allocator()->free();
	out_dense1->allocator()->free();
	out_relu1->allocator()->free();
	out_mxpool1->allocator()->free();
	out_conv2->allocator()->free();
	out_relu2->allocator()->free();
	out_mxpool2->allocator()->free();
	out_relu3->allocator()->free();
	out_relu4->allocator()->free();
	out_dense2->allocator()->free();
	out_dense3->allocator()->free();
	out_softmax->allocator()->free();
#else
	free(weights1->data);
	free(weights2->data);
	free(weights3->data);
	free(weights4->data);
	free(weights5->data);
	free(X->data);
	free(out_conv1->data);
	free(out_dense1->data);
	free(out_relu1->data);
	free(out_mxpool1->data);
	free(out_conv2->data);
	free(out_relu2->data);
	free(out_mxpool2->data);
	free(out_relu3->data);
	free(out_relu4->data);
	free(out_dense2->data);
	free(out_dense3->data);
	free(out_softmax->data);
#endif
	delete weights1;
	delete weights2;
	delete weights3;
	delete weights4;
	delete weights5;
	delete X;
	delete out_conv1;
	delete out_dense1;
	delete out_relu1;
	delete out_mxpool1;
	delete out_conv2;
	delete out_relu2;
	delete out_mxpool2;
	delete out_relu3;
	delete out_relu4;
	delete out_dense2;
	delete out_dense3;
	delete out_softmax;

	return 0;
}
