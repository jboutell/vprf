#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cnn.h"
#include "kernels.h"
#include "profile.h"

static void load_input(float *data, uint32_t size, FILE *file) {
	int cnt;

	cnt = fread(data, sizeof(float), size, file);

	if (cnt != size) {
		printf("Something went wrong in reading input file\n");
	}
}

void read (const char *fn, float *fifo) {
	int cnt;
	FILE *f = fopen (fn, "rb");
	if (f == NULL) {
		printf("Could not open %s\n", fn);
		return;
	}

	load_input(fifo, C1 * CONV1_DIM * CONV1_DIM, f);
	fclose (f);
}

void lide_c_softmax_write(FILE *f, float *input_array, int m, int n) {
    int i,j;
    float sum = 0.0;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            sum += exp(matrix(input_array, n, i, j));
        }
    }
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
			fprintf(f, "%.6f\n", exp(matrix(input_array, n, i, j))/sum);
        }
    }
}

void readConvWeights(const char *fn, float *wgt, int len) {
    FILE *file = fopen(fn, "r");
	if (file == NULL) {
		printf("Could not open %s\n", fn);
		return;
	}
	int cnt = fread(wgt, sizeof(float), len, file);
	if(cnt != len) {
		printf("file read error: %i of %i samples acquired\n", cnt, len);
	}
    fclose(file);
}

int main (int argc, char *argv[]) {

	// variables for profiling
	unsigned long long t1, t2;
	unsigned int cal;
	float totalTime;

	source_data_t *source_data = (source_data_t *) malloc(sizeof(source_data_t));
	conv_relu_l1_data_t *l1data = (conv_relu_l1_data_t *) malloc(sizeof(conv_relu_l1_data_t));
	conv_relu_l2_data_t *l2data = (conv_relu_l2_data_t *) malloc(sizeof(conv_relu_l2_data_t));

	float fifo_in[CHANNELS*PATCH1SQPAD] = {0.0};
    float fifo_matrix[FM_COUNT*PATCH2SQPAD] = {0.0};
    float fifo_out_l2[FM_COUNT*PATCH3SQ];
    float fifo_multi_out_1[MAGIC];
    float fifo_relu_out_1[MAGIC];
    float fifo_multi_out_2[MAGIC];
    float fifo_relu_out_2[MAGIC];
    float fifo_multi_out_3[CLASSES];

	float *coeff_l3 = (float *) malloc(sizeof(float) * MAGIC * PATCH3SQ * FM_COUNT);
	float *coeff_l4 = (float *) malloc(sizeof(float) * MAGIC * MAGIC);
	float *coeff_l5 = (float *) malloc(sizeof(float) * MAGIC * CLASSES);

	sourceInit(source_data);
	conv_relu_l1Init(l1data);
	conv_relu_l2Init(l2data);
	readConvWeights("data/ip3.bin", coeff_l3, MAGIC * PATCH3SQ * FM_COUNT);
	readConvWeights("data/ip4.bin", coeff_l4, MAGIC * MAGIC);
	readConvWeights("data/ip_last.bin", coeff_l5, MAGIC * CLASSES);
	FILE *fileout = fopen("data/sink.txt", "w");

	// start profiling
	cal = calibrate();
	t1 = timestamp();

	for(int i = 0; i < 384; i++) {
		sourceFire(source_data, fifo_in);

		// LAYER 1
		conv_relu_l1Fire(l1data, fifo_in, fifo_matrix);

		// LAYER 2
		conv_relu_l2Fire(l2data, fifo_matrix, fifo_out_l2);

		// LAYER 3
		lide_c_mtx_mulf(coeff_l3, fifo_out_l2, fifo_multi_out_1, MAGIC, PATCH3SQ * FM_COUNT, 1);
		lide_c_image_relu(fifo_multi_out_1, fifo_relu_out_1, MAGIC, 1);
	 
		// LAYER 4
		lide_c_mtx_mulf(coeff_l4, fifo_relu_out_1, fifo_multi_out_2, MAGIC, MAGIC, 1);
		lide_c_image_relu(fifo_multi_out_2, fifo_relu_out_2, MAGIC, 1);
		 
		// LAYER 5
		lide_c_mtx_mulf(coeff_l5, fifo_relu_out_2, fifo_multi_out_3, CLASSES, MAGIC, 1);
		lide_c_softmax_write (fileout, fifo_multi_out_3, 4, 1);
	}

	// stop profiling
	t2 = timestamp();
	totalTime = ((float) (((unsigned int)(t2-t1))-cal)) / 1000000.0;
	printf("%f\n", totalTime);

	// CLEANUP
	fclose (fileout);
	sourceFinish(source_data);
	free(source_data);
	conv_relu_l1Finish(l1data);
	free(l1data);
	conv_relu_l2Finish(l2data);
	free(l2data);
	free(coeff_l3);
	free(coeff_l4);
	free(coeff_l5);

	return 0;
}
