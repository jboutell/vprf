#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "kernels.h"
#include "example_utils.h"

static void load_input(float *data, uint32_t size, FILE *file) {
	int cnt;

	cnt = fread(data, sizeof(float), size, file);

	if (cnt != size) {
		printf("Something went wrong in reading input file\n");
	}
}

void sourceInit(source_data_t *data) {
	data->file = NULL;
	char sourceFileName[256];
	sprintf(sourceFileName, "data/c384.bin");
	data->file = fopen(sourceFileName, "rb");
	if (data->file == NULL) {
		printf("Could not open %s\n", sourceFileName);
		return;
	}
}

void sourceFire(source_data_t *data, float * fifo_out) {
	if (data->file != NULL) {
		#ifdef DBGPRINT
		printf("actor source fires\n");
		#endif
		load_input(fifo_out, C1 * CONV1_DIM * CONV1_DIM, data->file);
	}
}

void sourceFinish(source_data_t *data) {
	if (data->file != NULL) {
		fclose(data->file);
		data->file = NULL;
	}
}

int conv_relu_l1Init(conv_relu_l1_data_t *data) {

	data->convL1 = (dnn_conv_t *) malloc(sizeof(dnn_conv_t));
	data->reluL1 = (dnn_relu_t *) malloc(sizeof(dnn_relu_t));
	data->poolL1 = (dnn_pool_t *) malloc(sizeof(dnn_pool_t));

	// c1c2 and in exist just for configuration
	float *in = (float *) malloc(sizeof(float) * C1 * CONV1_DIM * CONV1_DIM);
	float *c1c2 = (float *) malloc(sizeof(float) * OC * CONV2_DIM * CONV2_DIM);
	data->wgt1 = (float *) malloc(sizeof(float) * C1 * OC * CONV_SIZE * CONV_SIZE);
    data->conv_bias = (float *)malloc(sizeof(float) * OC);

    memset(data->conv_bias, 0, OC * sizeof(float));

	load_data(data->wgt1, C1 * OC * CONV_SIZE * CONV_SIZE, "data/conv1_flipped.bin");

    dnnl_engine_kind_t engine_kind = validate_engine_kind(ONE_DEVICE);
    CHECK(dnnl_engine_create(&data->engine1, engine_kind, 0));

    CHECK(dnnl_stream_create(&data->stream1, data->engine1, dnnl_stream_default_flags));

	const dnnl_memory_desc_t *p1;
    setup_conv(data->engine1, data->convL1, data->L1_n, data->L1_net, data->L1_args, CONV1_DIM, C1, in, data->wgt1, data->conv_bias);
    p1 = setup_relu(data->engine1, data->convL1, data->reluL1, data->L1_n, data->L1_net, data->L1_args);
    setup_pool(data->engine1, data->poolL1, data->L1_n, data->L1_net, data->L1_args, POOL1_DIM, in, p1, data->reluL1->relu_dst_memory);

	//printf("L1 JIT compilation ... ");
    write_to_dnnl_memory(in, data->convL1->conv_user_src_memory);
    run(data->stream1, data->L1_n, data->L1_net, data->L1_args);
    CHECK(dnnl_stream_wait(data->stream1));
    read_from_dnnl_memory(c1c2, data->poolL1->pool_user_dst_memory);
	//printf("done\n");

	free(c1c2);
	free(in);

	return 0;
}

void conv_relu_l1Finish(conv_relu_l1_data_t *data) {
	clean_conv(data->convL1);
	clean_relu(data->reluL1);
	clean_pool(data->poolL1);

	free(data->convL1);
	free(data->reluL1);
	free(data->poolL1);

	free(data->wgt1);
    free(data->conv_bias);

    for (uint32_t i = 0; i < data->L1_n[0]; ++i) {
        free_arg_node(&data->L1_args[i]);
 	}
	dnnl_stream_destroy(data->stream1);
    dnnl_engine_destroy(data->engine1);
}


void conv_relu_l1Fire (conv_relu_l1_data_t *data, float * fifo_in, float * fifo_out) {

    write_to_dnnl_memory(fifo_in, data->convL1->conv_user_src_memory);
    run(data->stream1, data->L1_n, data->L1_net, data->L1_args);
    CHECK(dnnl_stream_wait(data->stream1));
    read_from_dnnl_memory(fifo_out, data->poolL1->pool_user_dst_memory);
}


int conv_relu_l2Init(conv_relu_l2_data_t *data) {

	data->convL2 = (dnn_conv_t *) malloc(sizeof(dnn_conv_t));
	data->reluL2 = (dnn_relu_t *) malloc(sizeof(dnn_relu_t));
	data->poolL2 = (dnn_pool_t *) malloc(sizeof(dnn_pool_t));

	// c1c2 and out exist just for configuration
	float *c1c2 = (float *) malloc(sizeof(float) * OC * CONV2_DIM * CONV2_DIM);
	float *out = (float *) malloc(sizeof(float) * OC * POOL2_DIM * POOL2_DIM);
	data->wgt2 = (float *) malloc(sizeof(float) * OC * OC * CONV_SIZE * CONV_SIZE);
    data->conv_bias = (float *) malloc(sizeof(float) * OC);

    memset(data->conv_bias, 0, OC * sizeof(float));

	load_data(data->wgt2, OC * OC * CONV_SIZE * CONV_SIZE, "data/conv2_flipped.bin");

    dnnl_engine_kind_t engine_kind = validate_engine_kind(ONE_DEVICE);
    CHECK(dnnl_engine_create(&data->engine2, engine_kind, 0));

    CHECK(dnnl_stream_create(&data->stream2, data->engine2, dnnl_stream_default_flags));

	const dnnl_memory_desc_t *p2;
    setup_conv(data->engine2, data->convL2, data->L2_n, data->L2_net, data->L2_args, CONV2_DIM, OC, c1c2, data->wgt2, data->conv_bias);
    p2 = setup_relu(data->engine2, data->convL2, data->reluL2, data->L2_n, data->L2_net, data->L2_args);
    setup_pool(data->engine2, data->poolL2, data->L2_n, data->L2_net, data->L2_args, POOL2_DIM, out, p2, data->reluL2->relu_dst_memory);

	//printf("L2 JIT compilation ... ");
    write_to_dnnl_memory(c1c2, data->convL2->conv_user_src_memory);
    run(data->stream2, data->L2_n, data->L2_net, data->L2_args);
    CHECK(dnnl_stream_wait(data->stream2));
    read_from_dnnl_memory(out, data->poolL2->pool_user_dst_memory);
	//printf("done\n");

	free(c1c2);
	free(out);

	return 0;
}

void conv_relu_l2Finish(conv_relu_l2_data_t *data) {
	clean_conv(data->convL2);
	clean_relu(data->reluL2);
	clean_pool(data->poolL2);

	free(data->convL2);
	free(data->reluL2);
	free(data->poolL2);

	free(data->wgt2);
    free(data->conv_bias);

    for (uint32_t i = 0; i < data->L2_n[0]; ++i) {
        free_arg_node(&data->L2_args[i]);
	} 
    dnnl_stream_destroy(data->stream2);
    dnnl_engine_destroy(data->engine2);
}


void conv_relu_l2Fire (conv_relu_l2_data_t *data, float * fifo_in, float * fifo_out) {

    write_to_dnnl_memory(fifo_in, data->convL2->conv_user_src_memory);
    run(data->stream2, data->L2_n, data->L2_net, data->L2_args);
    CHECK(dnnl_stream_wait(data->stream2));
    read_from_dnnl_memory(fifo_out, data->poolL2->pool_user_dst_memory);
}

void lide_c_mtx_mulf(float* A, float* B, float* c_mul, int Mdim, int Ndim, int Kdim)
{
    int i, j, z;
	memset (c_mul, 0, sizeof(float) * Mdim * Kdim);
    for (i = 0; i < Mdim; i++)
    {
        for (j = 0; j < Kdim; j++)
        {
            for (z = 0; z < Ndim; z++)
            {
                matrix(c_mul, Kdim, i, j) += matrix(A, Ndim, i, z) * matrix(B, Kdim, z, j);
            }
        }
    }
}

void lide_c_image_relu(float *bef_array, float *aft_array, int row, int column)
{
    int i,j;
    for (i = 0; i < row; i++)
    {
        for (j = 0; j < column; j++)
        {
            float tmp = 0;
            tmp = tmp > matrix(bef_array, column, i, j)? tmp : matrix(bef_array, column, i, j);
            matrix(aft_array, column, i, j) = tmp;
        }

    }
}

void lide_c_softmax_write(float *output_array, float *input_array, int m, int n) {
    int i,j;
	int ind = 0;
    float sum = 0.0;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            sum += exp(matrix(input_array, n, i, j));
        }
    }
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
			output_array[ind++] = exp(matrix(input_array, n, i, j)) / sum;
        }
    }
}

void writer(FILE *f, float *input_array, int len) {
	for (int i = 0; i < len; i++) {
		fprintf(f, "%f\n", input_array[i]);
	}
}

