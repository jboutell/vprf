#include "cnn.h"
#include "oneapi/dnnl/dnnl.h"
#include "onednn.h"

typedef struct {
	FILE *file;
	int iteration;
	char const *fn;
} source_data_t;

typedef struct {
    uint32_t L1_n[1];
    dnnl_primitive_t L1_net[12];
    args_t L1_args[12];

	dnn_conv_t *convL1;
	dnn_relu_t *reluL1;
	dnn_pool_t *poolL1;
	float *wgt1;
    float *conv_bias;
    dnnl_engine_t engine1;
    dnnl_stream_t stream1;

} conv_relu_l1_data_t;

typedef struct {
    uint32_t L2_n[1];
    dnnl_primitive_t L2_net[12];
    args_t L2_args[12];

	dnn_conv_t *convL2;
	dnn_relu_t *reluL2;
	dnn_pool_t *poolL2;
	float *wgt2;
    float *conv_bias;
    dnnl_engine_t engine2;
    dnnl_stream_t stream2;
} conv_relu_l2_data_t;

void sourceInit(source_data_t *data);
void sourceFire(source_data_t *data, float * fifo_out);
void sourceFinish(source_data_t *data);
int conv_relu_l1Init(conv_relu_l1_data_t *data);
void conv_relu_l1Fire (conv_relu_l1_data_t *data, float * fifo_in, float * fifo_out);
void conv_relu_l1Finish(conv_relu_l1_data_t *data);

int conv_relu_l2Init(conv_relu_l2_data_t *data);
void conv_relu_l2Fire (conv_relu_l2_data_t *data, float * fifo_in, float * fifo_out);
void conv_relu_l2Finish(conv_relu_l2_data_t *data);


