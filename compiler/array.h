#pragma once

class Array {
  private:
	Array(){};

  public:
	static unsigned int isInArray(unsigned int *array, unsigned int integerCount,
								  unsigned int integer);
	static unsigned int tryAddArray(unsigned int *array, unsigned int *integerCount,
									unsigned int integer);
};
