#include "entity.h"
#include "hash.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

Entity::Entity() : name(NULL), id(0), inputIndex(0), outputIndex(0) {
	ports = new std::vector<port *>();
}

Entity::~Entity() {
	for (unsigned int i = 0; i < ports->size(); i++) {
		free(ports->at(i)->name);
		delete ports->at(i);
	}
	delete ports;
	free(name);
}

Entity::Entity(const Entity &obj) {
	ports = new std::vector<port *>();
	setName(obj.name);
	id = obj.id;
	for (unsigned int i = 0; i < obj.ports->size(); i++) {
		copyPort(*obj.ports->at(i));
	}
	inputIndex = obj.inputIndex;
	outputIndex = obj.outputIndex;
}

void Entity::setName(char *name) {
	this->name = (char *)malloc(strlen(name) + 1);
	strcpy(this->name, name);
}

char *Entity::getName() {
	return name;
}

void Entity::setId(int id) {
	this->id = id;
}

int Entity::getId() {
	return this->id;
}

char *Entity::formPortName(char *str1, char *str2) {
	char *combStr = (char *)malloc(strlen(str1) + strlen(str2) + 2);
	sprintf(combStr, "%s_%s", str1, str2);
	return combStr;
}

port *Entity::addPort(char *vertexName, char *portName) {
	port *newPort = new port;
	newPort->name = formPortName(vertexName, portName);
	newPort->hash = Hash::hashCharArray(newPort->name, strlen(newPort->name));
	newPort->direction = UNDIRECTED;
	newPort->fanOut = 0;
	newPort->isDRP = false;
	ports->push_back(newPort);
	return newPort;
}

port *Entity::addPort(char *fullName) {
	port *newPort = new port;
	newPort->name = (char *)malloc(strlen(fullName) + 1);
	strcpy(newPort->name, fullName);
	newPort->hash = Hash::hashCharArray(fullName, strlen(fullName));
	newPort->direction = UNDIRECTED;
	newPort->fanOut = 0;
	newPort->isDRP = false;
	ports->push_back(newPort);
	return newPort;
}

void Entity::copyPort(port &refPort) {
	port *newPort = new port;
	newPort->name = (char *)malloc(strlen(refPort.name) + 1);
	strcpy(newPort->name, refPort.name);
	newPort->hash = refPort.hash;
	newPort->direction = refPort.direction;
	newPort->fanOut = refPort.fanOut;
	newPort->index = refPort.index;
	newPort->isDRP = refPort.isDRP;
	ports->push_back(newPort);
}

port *Entity::addBindingPort(char *vertexName) {
	return addPort(vertexName, (char *)bindingPort);
}

unsigned int Entity::getHash(port *p) {
	return p->hash;
}

unsigned int Entity::getHashByIndex(unsigned int index) {
	return ports->at(index)->hash;
}

unsigned int Entity::portCount() {
	return ports->size();
}

port *Entity::getPort(unsigned int portIndex) {
	return ports->at(portIndex);
}

port *Entity::getPortByHash(unsigned int hash) {
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->hash == hash) {
			return ports->at(i);
		}
	}
	return NULL;
}

int Entity::getPortNum(unsigned int hash) {
	for (int i = 0; i < (int)ports->size(); i++) {
		if (ports->at(i)->hash == hash) {
			return i;
		}
	}
	return -1;
}

int Entity::getPortDirection(unsigned int hash) {
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->hash == hash) {
			return ports->at(i)->direction;
		}
	}
	printf("Error: entity %s has no port with hash %u\n", name, hash);
	return 0;
}

void Entity::setPortIndex(unsigned int hash, int direction) {
	int portNum = -1;
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->hash == hash) {
			portNum = i;
		}
	}
	if (portNum == -1) {
		printf("Error: entity %s has no port with hash %u\n", name, hash);
	} else if (direction & INPUT) {
		ports->at(portNum)->index = inputIndex++;
	} else if (direction == OUTPUT) {
		ports->at(portNum)->index = outputIndex++;
	}
}

void Entity::incrementFanOut(unsigned int hash) {
	bool found = false;
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->hash == hash) {
			found = true;
			ports->at(i)->fanOut++;
		}
	}
	if (found == false) {
		printf("Error: entity %s has no port with hash %u\n", name, hash);
	}
}

unsigned int Entity::getFanOut(unsigned int hash) {
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->hash == hash) {
			return ports->at(i)->fanOut;
		}
	}
	printf("Error: entity %s has no port with hash %u\n", name, hash);
	return 0;
}

void Entity::printPorts() {
	printf("Entity %s has ports\n", name);
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->direction == INPUT) {
			printf("%i input %s fanout:%i\n", i, ports->at(i)->name, ports->at(i)->fanOut);
		} else if (ports->at(i)->direction == OUTPUT) {
			printf("%i output %s fanout:%i\n", i, ports->at(i)->name, ports->at(i)->fanOut);
		}
	}
}
