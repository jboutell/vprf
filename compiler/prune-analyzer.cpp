#include "prune-analyzer.h"
#include "array.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

void Analyzer::findDynamicActors(Network *network, std::vector<Actor *> *dynamic) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDynamic() != NULL) {
				dynamic->push_back(a);
			}
		}
	}
}

Actor *Analyzer::findConfigurationActor(Network *network, Actor *a) {
	Fifo *v = getFifoByName(network, a->getDynamic());
	if (v == NULL) {
		return NULL;
	}
	Vertex *p = network->getVertexByIndex(v->getPredecessor(0));
	if (p->getResourceType() == ACTOR) {
		return static_cast<Actor *>(p);
	}
	printf("Error: (findConfigurationActor) predecessor \
			%s of FIFO %s is not an actor\n",
		   p->getName(), v->getName());
	return NULL;
}

bool Analyzer::findPrecedence(Network *network, Actor *x, Actor *y) {
	int yPrecedesX = network->isPredecessor(network->getActorIndex(x), network->getActorIndex(y));
	int xPrecedesY = network->isPredecessor(network->getActorIndex(y), network->getActorIndex(x));
	if (!yPrecedesX && !xPrecedesY) {
		printf("Error: dynamic actors %s and %s are disconnected\n", x->getName(), y->getName());
	}
	if (yPrecedesX && xPrecedesY) {
		printf("Error: dynamic actors %s and %s form a loop\n", x->getName(), y->getName());
	}
	if (yPrecedesX) {
		return false;
	}
	return true;
}

bool Analyzer::hasCycle(Network *network, Actor *x, Actor *y) {
	int yPrecedesX = network->isPredecessor(network->getActorIndex(x), network->getActorIndex(y));
	int xPrecedesY = network->isPredecessor(network->getActorIndex(y), network->getActorIndex(x));
	if (yPrecedesX && xPrecedesY) {
		return true;
	}
	return false;
}

void Analyzer::formDPGs(Network *network, std::vector<dGraph *> *DPGs) {
	std::vector<Actor *> *dynamic = new std::vector<Actor *>();
	std::vector<bool> *marked;
	findDynamicActors(network, dynamic);
	marked = new std::vector<bool>(dynamic->size());
	for (unsigned int i = 0; i < dynamic->size(); i++) {
		marked->at(i) = false;
	}
	for (unsigned int i = 0; i < dynamic->size(); i++) {
		for (unsigned int j = 0; j < dynamic->size(); j++) {
			if ((j != i) && (!marked->at(i) && !marked->at(j))) {
				Actor *qx = findConfigurationActor(network, dynamic->at(i));
				Actor *qy = findConfigurationActor(network, dynamic->at(j));
				if (qx == NULL) {
					printf("(formDPGs) qx is NULL!\n");
					break;
				}
				if (qy == NULL) {
					printf("(formDPGs) qy is NULL!\n");
					break;
				}
				if (qy->getIndex() == qx->getIndex()) {
					// TODO: this assumes all conf actor output ports are conf ports
					Actor *DPA = NULL;
					for (unsigned int k = 0; k < qx->getSuccessorCount(); k++) {
						int bInd = network->getVertexByIndex(
							qx->getSuccessor(k))->getSuccessor(0);
						Actor *a = static_cast<Actor *>(
							network->getVertexByIndex(bInd));
						if (a->getDynamic() == NULL) {
							//printf(" Actor %s labeled as DPA\n", a->getName());
							a->setDPA(true);
							DPA = a;
						}
					}
					dGraph *DPG = new dGraph();
					if (hasCycle(network, dynamic->at(i), dynamic->at(j))) {
						if (DPA != NULL) {	// primarily use DPAs to figure out x&y order
							if (network->isPredecessor(network->getActorIndex(dynamic->at(i)), 
								network->getActorIndex(DPA)) && network->isPredecessor(network->getActorIndex(DPA),
								network->getActorIndex(dynamic->at(j)))) {
								DPG->x = dynamic->at(i);
								DPG->y = dynamic->at(j);
							} else {
								DPG->x = dynamic->at(j);
								DPG->y = dynamic->at(i);
							}
						} else {
							printf("Warning: Unsure about order of DAs when no DPAs exist\n");
							DPG->x = dynamic->at(i);
							DPG->y = dynamic->at(j);
						}
					} else {
						if (findPrecedence(network, dynamic->at(i), dynamic->at(j))) {
							DPG->x = dynamic->at(i);
							DPG->y = dynamic->at(j);
						} else {
							DPG->x = dynamic->at(j);
							DPG->y = dynamic->at(i);
						}
					}
					DPG->q = qx;
					DPG->q->setConfActor(true);
					DPGs->push_back(DPG);
					marked->at(i) = true;
					marked->at(j) = true;
				}
			}
		}
	}
	delete marked;
	delete dynamic;
}

Fifo *Analyzer::getFifoByName(Network *network, char *name) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v == NULL) {
			printf("(getFifoByName) v is NULL\n");
			break;
		}
		if ((v->getResourceType() == FIFO) && (strcmp(v->getName(), name) == 0)) {
			return static_cast<Fifo *>(v);
		}
	}
	printf("Error: did not find FIFO by name %s\n", name);
	return NULL;
}

// Part 1: ensuring that DC actors are static processing actors
void Analyzer::checkDesignRule3_1(Network *network, dGraph *DPG, int index) {
	bool result = true;
	printf("DPG %i: checking design rule 3(1) -- ", index);
	for (unsigned int i = 0; i < DPG->DCs->size(); i++) {
		if (DPG->DCs->at(i)->isDC) {
			for (unsigned int j = 0; j < DPG->DCs->at(i)->a->size(); j++) {
				Actor *a = DPG->DCs->at(i)->a->at(j);
				if (a->hasDynamicPorts() || a->isConfActor()) { // hasDynamicPorts is only used for DAs
					printf("actor %s belongs to a DC but is not an SPA or a DPA -- ", a->getName());
					result = false;
				}
			}
		}
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

// Part 2: ensuring that no actor is not associated with more than 1 dynamic
// actor
bool Analyzer::checkDesignRule3_2forActor(Network* network, Actor *a) {
	bool result = true;
	if (a->getPredecessorCount() > 1) {
		unsigned int *dynamic =
			(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
		unsigned int dynamicCount = 0;
		for (unsigned int k = 0; k < a->getPredecessorCount(); k++) {
			int bInd = network->getVertexByIndex(a->getPredecessor(k))->getPredecessor(0);
			if (static_cast<Actor *>(network->getVertexByIndex(bInd))->getDynamic() !=
				NULL) {
				if (!Array::isInArray(dynamic, dynamicCount, bInd)) {
					Array::tryAddArray(dynamic, &dynamicCount, bInd);
				}
			}
		}
		if (dynamicCount > 1) {
			printf("actor %s\n", a->getName());
			result = false;
		}
		free(dynamic);
	}
	if (a->getSuccessorCount() > 1) {
		unsigned int *dynamic =
			(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
		unsigned int dynamicCount = 0;
		for (unsigned int k = 0; k < a->getSuccessorCount(); k++) {
			int bInd = network->getVertexByIndex(a->getSuccessor(k))->getSuccessor(0);
			if (static_cast<Actor *>(network->getVertexByIndex(bInd))->getDynamic() !=
				NULL) {
				if (!Array::isInArray(dynamic, dynamicCount, bInd)) {
					Array::tryAddArray(dynamic, &dynamicCount, bInd);
				}
			}
		}
		if ((dynamicCount > 1) && (a->isConfActor() == false)) {
			printf("actor %s\n", a->getName());
			result = false;
		}
		free(dynamic);
	}
	return result;
}

void Analyzer::checkDesignRule3_2forDPG(Network* network, dGraph *DPG) {
	bool result = true;
	printf("Checking design rule 3(2) -- ");
	for (unsigned int j = 0; j < DPG->DCs->size(); j++) {
		for (unsigned int i = 0; i < DPG->DCs->at(j)->a->size(); i++) {

			if(checkDesignRule3_2forActor(network, DPG->DCs->at(j)->a->at(i)) == false) {
				result = false;
			}
		}
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

void Analyzer::checkDesignRule4(Network *network) {
	bool result = true;
	printf("Checking design rule 4 -- ");
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->checkDesignRule4() == false) {
				result = false;
			}
		}
	}
	if (result == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
}

	/*
		before:
		(i-f1-o)-(i-e1-o)-(i-a1-o)
		after:
		(i-f1-o)-(i-e1-o)-(i-a2-o)-(i-e2-o)-(i-f2-o)-(i-e3-o)-(i-a1-o)
					mod     new       new     new      new      mod
	*/

#define STR_MAX 256

Actor *Analyzer::insertDummyActor(Network *network, Vertex *f1, Vertex *a1) {
	char buffer[STR_MAX];
	sprintf(buffer, "actor%i", network->vertexCount());
	Actor *a2 = network->addActor(buffer);
	a2->setResourceType(ACTOR);
	sprintf(buffer, "fifo%i", network->vertexCount());
	Fifo *f2 = network->addFifo(buffer);
	f2->setResourceType(FIFO);
	sprintf(buffer, "edge%i", network->edgeCount());
	Edge *e2 = network->addConnection(buffer);
	sprintf(buffer, "edge%i", network->edgeCount());
	Edge *e3 = network->addConnection(buffer);

	sprintf(buffer, "in");
	//	a2: add input port "a2_in"
	port *a2i = a2->addPort(a2->getName(), buffer);
	a2i->direction = INPUT;
	//	e2:	add output port "f2_in"
	port *e2o = e2->addPort(f2->getName(), buffer);
	e2o->direction = OUTPUT;
	//	f2: add input port "f2_in"
	port *f2i = f2->addPort(f2->getName(), buffer);
	f2i->direction = INPUT;
	//	e3:	add output port "a1_in"
	port *a1i = discoverDRP(network, static_cast<Actor *>(a1), static_cast<Fifo *>(f1), INPUT);
	port *e3o = e3->addPort(a1i->name);
	e3o->direction = OUTPUT;

	sprintf(buffer, "out");
	//	a2:	add output port "a2_out"
	port *a2o = a2->addPort(a2->getName(), buffer);
	a2o->direction = OUTPUT;
	//	e2: add input port "a2_out"
	port *e2i = e2->addPort(a2->getName(), buffer);
	e2i->direction = INPUT;
	//	f2:	add output port "f2_out"
	port *f2o = f2->addPort(f2->getName(), buffer);
	f2o->direction = OUTPUT;
	//	e3: add input port "f2_out"
	port *e3i = e3->addPort(f2->getName(), buffer);
	e3i->direction = INPUT;

	//	e1: modify output port to hold "a2_in"
	char e1oName[STR_MAX];
	for (unsigned int i = 0; i < f1->portCount(); i++) {
		if (f1->getPortDirection(f1->getPort(i)->hash) == OUTPUT) {
			port *f1o = f1->getPort(i);
			Edge *e1 = network->getEdgeByPortHash(f1o->hash);
			port *e1o = e1->getOtherEndByHash(f1o->hash);
			strcpy(e1oName, e1o->name);
			network->reconnectEdge(e1, a2i, OUTPUT);
		}
	}

	//  f1: modify successor to be "a2"
	f1->setSuccessor(0, network->getActorIndex(a2));

	//	a2:	add predecessor "f1"
	a2->addPredecessor(network->getFifoIndex(f1));
	//	a2:	add successor "f2"
	a2->addSuccessor(network->getFifoIndex(f2));

	//	f2:	add predecessor "a2"
	f2->addPredecessor(network->getActorIndex(a2));
	//	f2:	add successor "a1"
	f2->addSuccessor(network->getActorIndex(a1));

	//	a1: modify predecessor to be "f2"
	for (unsigned int i = 0; i < a1->getPredecessorCount(); i++) {
		if (((int)a1->getPredecessor(i)) == network->getFifoIndex(f1)) {
			a1->setPredecessor(i, network->getFifoIndex(f2));
		}
	}

	return a2;
}

void Analyzer::findEmptyDCs(Network *network, dGraph *DPG) {
	for (unsigned int i = 0; i < DPG->x->getSuccessorCount(); i++) {
		Vertex *f = network->getVertexByIndex(DPG->x->getSuccessor(i));
		for (unsigned int j = 0; j < f->getSuccessorCount(); j++) {
			if (f->getSuccessor(j) == (unsigned int)network->getActorIndex(DPG->y)) {
#ifdef DEBUG
				Actor *d = insertDummyActor(network, f, DPG->y);
				printf("Info: DPG actor %s is directly connected to %s -- inserted "
					   "dummy actor '%s'\n",
					   DPG->x->getName(), DPG->y->getName(), d->getName());
#else
				insertDummyActor(network, f, DPG->y);
#endif
			}
		}
	}
}

port *Analyzer::discoverDRP(Network *network, Actor *x, Fifo *f, int direction) {
	for (unsigned int i = 0; i < x->portCount(); i++) {
		if (x->getPortDirection(x->getPort(i)->hash) == direction) {
			port *xo = x->getPort(i);
			port *eo = network->getEdgeByPortHash(xo->hash)->getOtherEndByHash(xo->hash);
			for (unsigned int i = 0; i < f->portCount(); i++) {
				if (f->getPortDirection(f->getPort(i)->hash) == (3 - direction)) {
					if (f->getPort(i)->hash == eo->hash) {
						return xo;
					}
				}
			}
		}
	}
	return NULL;
}

int Analyzer::compareVertexIndices(Network *network, Vertex *v, std::vector<pfPair *> *DRPs) {
	for (unsigned int i = 0; i < DRPs->size(); i++) {
		if (network->getFifoIndex(DRPs->at(i)->v) == network->getFifoIndex(v)) {
			return (int)i;
		}
	}
	return -1;
}

void Analyzer::printDC(dC *DC, bool printPorts) {
	printf("DC: ");
	for (unsigned int k = 0; k < DC->a->size(); k++) {
		printf("%s ", DC->a->at(k)->getName());
	}
	if (printPorts) {
		printf("\n inputs: ");
		for (unsigned int k = 0; k < DC->DRPx->size(); k++) {
			printf("%s ", DC->DRPx->at(k)->name);
		}
		printf("\n outputs: ");
		for (unsigned int k = 0; k < DC->DRPy->size(); k++) {
			printf("%s ", DC->DRPy->at(k)->name);
		}
	}
	printf("\n");
}

// If the construction of DCs by this function succeeds, the hosting
// DPG implicitly fulfills Design Rule 5 and Design Rule 1
void Analyzer::formDCs(Network *network, dGraph *DPG, int index) {
	bool DCsValid = true;
	int subGraphCount = 1;
	unsigned int *subGraphs = (unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
	std::vector<pfPair *> DRPsX, DRPsY;
	
	// this loop considers all immediate successors (fifos) of the left DA
	for (unsigned int i = 0; i < DPG->x->getSuccessorCount(); i++) {
		Vertex *f = network->getVertexByIndex(DPG->x->getSuccessor(i));
		// only consider successors that eventually end up in the right DA
		if (network->isSuccessor(network->getFifoIndex(f), network->getActorIndex(DPG->y))) {
			pfPair *newPair = new pfPair();
			newPair->v = f;
			newPair->DRP = discoverDRP(network, DPG->x, static_cast<Fifo *>(f), OUTPUT);
			newPair->DRP->isDRP = true;
			DRPsX.push_back(newPair);
		}
	}
	// this loop considers all immediate predecessors (fifos) of the right DA
	for (unsigned int i = 0; i < DPG->y->getPredecessorCount(); i++) {
		Vertex *f = network->getVertexByIndex(DPG->y->getPredecessor(i));
		// only consider predecessors that eventually end up in the left DA
		if (network->isPredecessor(network->getFifoIndex(f), network->getActorIndex(DPG->x))) {
			pfPair *newPair = new pfPair();
			newPair->v = f;
			newPair->DRP = discoverDRP(network, DPG->y, static_cast<Fifo *>(f), INPUT);
			newPair->DRP->isDRP = true;
			DRPsY.push_back(newPair);
		}
	}
	// now disconnect DRPs from DCs
	for (int i = DRPsX.size() - 1; i >= 0; i--) {
		Vertex *f = DRPsX.at(i)->v; // f is the FIFO connected to the DRP
		DPG->x->removeSuccessor(network->getFifoIndex(f));
		f->removePredecessor(f->getPredecessor(0));
	}
	for (int i = DRPsY.size() - 1; i >= 0; i--) {
		Vertex *f = DRPsY.at(i)->v;
		DPG->y->removePredecessor(network->getFifoIndex(f));
		f->removeSuccessor(f->getSuccessor(0));
	}

	int qOutputCount = DPG->q->getSuccessorCount();
	int *initial_tokens = (int *) malloc(sizeof(int)*qOutputCount);
	// disconnect all fifos between control actor and DAs/DPAs
	printf(" Control actor %s controls actors:", DPG->q->getName());
	for (int i = qOutputCount - 1; i >= 0; i--) {
		Vertex *f = network->getVertexByIndex(DPG->q->getSuccessor(i));

		if (f->getResourceType() == FIFO) {
			Fifo *fifo = static_cast<Fifo *>(f);
			initial_tokens[i] = fifo->getInitialTokens();
		} else {
			printf(" error: resource %s departing from q is not a fifo!\n", f->getName());
		}

		Vertex *a = network->getVertexByIndex(f->getSuccessor(0));
		printf("%s, ", a->getName());
		DPG->q->removeSuccessor(network->getFifoIndex(f));
		f->removePredecessor(f->getPredecessor(0));
	}

	bool dr2passed = true;
	printf("\nDPG %i: design rule 2 -- ", index);
	for (int i = 1; i < qOutputCount; i++) {
		if(initial_tokens[i] != initial_tokens[0]) {
			//printf(" info: fifo %i initial tokens: %i\n", i, initial_tokens[i]);
			dr2passed = false;
		}
	}
	if (dr2passed == true) {
		printf("passed\n");
	} else {
		printf("FAILED\n");
	}
	free(initial_tokens);

	// the array subGraphs contains the index of a random vertex of that subgraph
	subGraphCount = network->findDisconnectedSubgraphs(subGraphs);
	// label subgraphs that include one of x, y or q as not DCs
	bool *isDC = (bool *) malloc(sizeof(bool) * subGraphCount);
	for (int i = 0; i < subGraphCount; i++) {
		isDC[i] = true;
		unsigned int *actorInds =
			(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
		unsigned int SGSize = network->getMaximalSubgraph(actorInds, subGraphs[i]);
		for (unsigned int j = 0; j < SGSize; j++) {
			if (((int)actorInds[j] == network->getActorIndex(DPG->y))
				|| ((int)actorInds[j] == network->getActorIndex(DPG->x))
				|| ((int)actorInds[j] == network->getActorIndex(DPG->q))) {
				isDC[i] = false;
			}
		}
		free (actorInds);
	}

	DPG->DCs = new std::vector<dC *>();
	for (int i = 0; i < subGraphCount; i++) {
		if (isDC[i] == true) {
			printf(" subgraph %i appears to be a DC, starting construction ...\n", i);
			unsigned int *DCinds =
				(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
			unsigned int DCSize = network->getMaximalSubgraph(DCinds, subGraphs[i]);
			dC *thisDC = new dC();
			thisDC->a = new std::vector<Actor *>();
			thisDC->DRPx = new std::vector<port *>();
			thisDC->DRPy = new std::vector<port *>();
			printf("  DC actors: ");
			for (unsigned int j = 0; j < DCSize; j++) {
				Vertex *v = network->getVertexByIndex(DCinds[j]);
				if (v->getResourceType() == ACTOR) {
					Actor *a = static_cast<Actor *>(v);
					if (a->isDPA() == true) {
						printf("%s(DPA), ", a->getName());
					} else {
						printf("%s(SPA), ", a->getName());
					}
					thisDC->a->push_back(a);
				} else {
					int indX = compareVertexIndices(network, v, &DRPsX);
					if (indX > -1) {
						thisDC->DRPx->push_back(DRPsX.at(indX)->DRP);
					}
					int indY = compareVertexIndices(network, v, &DRPsY);
					if (indY > -1) {
						thisDC->DRPy->push_back(DRPsY.at(indY)->DRP);
					}
				}
			}
			printf("\n");
			if ((thisDC->DRPx->size() == 0) && (thisDC->DRPy->size() == 0)) {
				printf("Warning: Subgraph ");
				printDC(thisDC, true);
				printf(" misses input and output DRPs.\n");
				thisDC->isDC = false;
			} else {
				if (thisDC->DRPx->size() * thisDC->DRPy->size() == 0) {
					printf("Warning: Subgraph ");
					printDC(thisDC, true);
					printf(" misses input or output DRPs -- not a host graph, nor a DC.\n");
					DCsValid = false;
				} else {
					//printf("Info: host graph of DPG (q)%s (x)%s (y)%s has actors: ",
					//	DPG->q->getName(), DPG->x->getName(), DPG->y->getName());
					//printDC (thisDC, false);
					thisDC->isDC = true;
				}
			}
			DPG->DCs->push_back(thisDC);
			free(DCinds);
			printf(" done\n");
		}
	}

	if (DPG->DCs->size() == 0) {
		printf("Info: after disconnecting DRPs of delta, DPG %s-%s-%s appears to consist of %i subgraphs\n", DPG->q->getName(), DPG->x->getName(),
		   DPG->y->getName(), subGraphCount);
		for (int i = 0; i < subGraphCount; i++) {
			unsigned int *actorInds =
				(unsigned int *)malloc(sizeof(unsigned int) * network->vertexCount());
			unsigned int SGSize = network->getMaximalSubgraph(actorInds, subGraphs[i]);
			printf(" subgraph %i (size:%i): ", i, SGSize);
			for (unsigned int j = 0; j < SGSize; j++) {
				if (network->getVertexByIndex(actorInds[j])->getResourceType() == ACTOR) {
					printf("%s, ", network->getVertexByIndex(actorInds[j])->getName());
				}
			}
			printf("\n");
			for (unsigned int j = 0; j < SGSize; j++) {
				if ((int)actorInds[j] == network->getActorIndex(DPG->y)) {
					printf("  subgraph %i contains actor y -- not a DC\n", i);
				}
				else if ((int)actorInds[j] == network->getActorIndex(DPG->x)) {
					printf("  subgraph %i contains actor x -- not a DC\n", i);
				}
				else if ((int)actorInds[j] == network->getActorIndex(DPG->q)) {
					printf("  subgraph %i contains actor q -- not a DC\n", i);
				}
			}
			free (actorInds);
		}
	}

	printf("DPG %i: design rule 1 -- ", index);
	if ((DPG->DCs->size() > 0) && DCsValid) {
		printf("passed, if control relationships are as described above\n");
	} else {
		if (DPG->DCs->size() == 0) {
			printf("due to 0 DCs: \n");
		}
		printf("FAILED\n");
	}
	printf("DPG %i: design rule 5 -- ", index);
	if ((DPG->DCs->size() > 0) && DCsValid) {
		printf("passed\n");
	} else {
		if (DPG->DCs->size() == 0) {
			printf("due to 0 DCs: \n");
		}
		printf("FAILED\n");
	}
	for (unsigned int i = 0; i < DRPsX.size(); i++) {
		delete DRPsX.at(i);
	}
	for (unsigned int i = 0; i < DRPsY.size(); i++) {
		delete DRPsY.at(i);
	}
	free(subGraphs);
	free(isDC);
}

void Analyzer::formDCMaps(Network *network, const char *dir) {
	Network *networkCopy = new Network(*network);
	std::vector<dGraph *> *DPGs = new std::vector<dGraph *>();
	formDPGs(networkCopy, DPGs);
	for (unsigned int i = 0; i < DPGs->size(); i++) {
		printf("Info: DPG %i is %s-%s-%s\n", i, DPGs->at(i)->q->getName(),
			   DPGs->at(i)->x->getName(), DPGs->at(i)->y->getName());
		//checkDesignRule2(network, DPGs->at(i), i);
		findEmptyDCs(networkCopy, DPGs->at(i));
		formDCs(networkCopy, DPGs->at(i), i);
		for (unsigned int j = 0; j < DPGs->at(i)->DCs->size(); j++) {
			if (DPGs->at(i)->DCs->at(j)->isDC) {
				printf(" DC %i: ", j);
				printDC (DPGs->at(i)->DCs->at(j), true);
			}
		}
		checkDesignRule3_1(networkCopy, DPGs->at(i), i);
		checkDesignRule3_2forDPG(network, DPGs->at(i));
	}
	for (unsigned int i = 0; i < DPGs->size(); i++) {
		for (unsigned int j = 0; j < DPGs->at(i)->DCs->size(); j++) {
			delete DPGs->at(i)->DCs->at(j)->a;
			delete DPGs->at(i)->DCs->at(j)->DRPx;
			delete DPGs->at(i)->DCs->at(j)->DRPy;
		}
		delete DPGs->at(i)->DCs;
		delete DPGs->at(i);
	}
	delete DPGs;
	delete networkCopy;
}

void Analyzer::doAnalysis(Network *network, const char *dir) {
	std::vector<dGraph *> *DPGs = new std::vector<dGraph *>();
	printf("Forming DPGs\n");
	formDPGs(network, DPGs);
	checkDesignRule4(network);
	formDCMaps(network, dir);
	for (unsigned int i = 0; i < DPGs->size(); i++) {
		delete DPGs->at(i);
	}
	delete DPGs;
}
