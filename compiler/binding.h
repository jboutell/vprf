#pragma once

#include "edge.h"

class Binding : public Edge {
  private:
	int dimensions;
	int workGroups[MAX_WG_DIM];
	int workItems[MAX_WG_DIM];

  public:
	Binding();
	~Binding();
	int countWGDimensions(char *wgs);
	void setWorkGroups(char *wgs);
	int getWorkGroupSize(int dimension);
	int getWorkGroupDimensions();
	void setWorkItems(char *wis);
	int getWorkItemSize(int dimension);
};
