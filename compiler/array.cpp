#include "array.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

unsigned int Array::isInArray(unsigned int *array, unsigned int integerCount,
							  unsigned int integer) {
	for (unsigned int i = 0; i < integerCount; i++) {
		if (array[i] == integer) {
			return 1;
		}
	}
	return 0;
}

unsigned int Array::tryAddArray(unsigned int *array, unsigned int *integerCount,
								unsigned int integer) {
	if (!isInArray(array, integerCount[0], integer)) {
		array[integerCount[0]++] = integer;
		return 1;
	}
	return 0;
}

