#pragma once

#include "processor.h"

#define GEN_DEVICE 1
#define CL_DEVICE 2
#define ANY_DEVICE 3

#define ACTOR_LOCAL 1
#define ACTOR_IO 2
#define ACTOR_ANY 3

typedef struct {
	char *data;
	int index;
} constant_data_t;

class Actor : public Vertex {
  private:
	Processor *processor;
	char *filename;
	int deviceType;
	int deviceIndex;
	int actorType;
	int dimensions;
	int workGroups[MAX_WG_DIM];
	int workItems[MAX_WG_DIM];
	char *dynamicFifo;
	bool confActor;
	bool DPA;
	std::vector<constant_data_t*> *constantData;

  public:
	Actor();
	~Actor();
	Actor(Actor &obj);
	void setProcessor(Processor *proc);
	Processor *getProcessor();
	void setSource(char *filename);
	char *getSource();
	void setConstantData(char *filename, int index);
	std::vector<constant_data_t*> *getConstantData();
	void setDeviceType(int type);
	int getDeviceType();
	void setDeviceIndex(int index);
	int getDeviceIndex();
	void setActorType(int type);
	int getActorType();
	void setWorkGroupDimensions(int dimensions);
	int getWorkGroupDimensions();
	void setWorkGroupSize(int wgs, int dimension);
	int getWorkGroupSize(int dimension);
	void setWorkItemSize(int wis, int dimension);
	int getWorkItemSize(int dimension);
	void setDynamic(char *portName);
	char *getDynamic();
	bool checkDesignRule4();
	bool hasDynamicPorts();
	void setConfActor(bool val);
	bool isConfActor();
	void setDPA(bool val);
	bool isDPA();
};
