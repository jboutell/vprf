#pragma once

class Hash {
  private:
	Hash(){};

  public:
	static unsigned int hashCharArray(char *arr, unsigned int len);
	static unsigned int hashCharArray(char *arr, unsigned int len, unsigned int seed);
	static unsigned int hashIntArray(unsigned int *arr, unsigned int len);
	static unsigned int hashIntArraySorted(unsigned int *list, unsigned int len);
};
