#pragma once

#include "network.h"

typedef struct {
	std::vector<Actor *> *a;
	std::vector<port *> *DRPx;
	std::vector<port *> *DRPy;
	bool isDC;
} dC;
typedef struct {
	Actor *q;
	Actor *x;
	Actor *y;
	std::vector<dC *> *DCs;
} dGraph;

class Analyzer {
  private:
	typedef struct { // a structure that links DRPs and their attached Fifos (v)
		port *DRP;
		Vertex *v;
	} pfPair;
	Actor *findConfigurationActor(Network *network, Actor *a);
	Fifo *getFifoByName(Network *network, char *name);
	bool hasCycle(Network *network, Actor *x, Actor *y);
	bool findPrecedence(Network *network, Actor *x, Actor *y);
	void findDynamicActors(Network *network, std::vector<Actor *> *dynamic);
	void findEmptyDCs(Network *network, dGraph *DPG);
	void formDCs(Network *network, dGraph *DPG, int index);
	void printDC(dC *DC, bool printPorts);
	void formDPGs(Network *network, std::vector<dGraph *> *DPGs);
	void formDCMaps(Network *network, const char *dir);
	void checkDesignRule2(Network *network, dGraph *DPG, int index);
	void checkDesignRule3_1(Network *network, dGraph *DPG, int index);
	bool checkDesignRule3_2forActor(Network* network, Actor *a);
	void checkDesignRule3_2forDPG(Network* network, dGraph *DPG);
	void checkDesignRule4(Network *network);
	Actor *insertDummyActor(Network *network, Vertex *f1, Vertex *a1);
	port *discoverDRP(Network *network, Actor *x, Fifo *f, int direction);
	int compareVertexIndices(Network *network, Vertex *v, std::vector<pfPair *> *DRPs);
	void writeControlFunction(Network *network, dGraph *DPG, Actor *a, bool isX, const char *dir);

  public:
	void doAnalysis(Network *network, const char *dir);
};
