#pragma once

#include "edge.h"
#include "vertex.h"

class Graph : public Entity {
  protected:
	typedef struct {
		unsigned int origin;
		unsigned int *found;
		unsigned int foundCount;
	} traversal;

	std::vector<Vertex *> *vertices;
	std::vector<Edge *> *edges;
	Graph();
	~Graph();
	void initGraphTraversal(traversal *t, unsigned int origin);
	void deinitGraphTraversal(traversal *t);
	void findConnectedVertices(traversal *t, int currId);
	void findPredecessors(traversal *t, unsigned int currInd);
	void findSuccessors(traversal *t, unsigned int currInd);
	int sortNodes(int *nodeList, int listLen);

  public:
	Graph(const Graph &obj);
	int isPredecessor(int orgId, int trgId);
	int isSuccessor(int orgId, int trgId);
	void printPredecessors();
	void printSuccessors(Vertex *v);
	void printSuccessors();
	void dumpPortData();
	int getVertexIndex(unsigned int hash);
	unsigned int resourceCount(int type);
	unsigned int vertexCount();
	unsigned int edgeCount();
	Vertex *getVertex(unsigned int hash);
	Vertex *getVertexByIndex(unsigned int index);
	Vertex *getVertexByOrder(int order, int type);
	Edge *getEdgeByPortHash(unsigned int hash);
	void connectUndirectedGraph();
	void connectDirectedGraph();
	int findDisconnectedSubgraphs(unsigned int *output);
	int getMaximalSubgraph(unsigned int *output, unsigned int origin);
	void generateIndices();
};
