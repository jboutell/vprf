#include "commandLineArguments.h"
#include "mappingXMLreader.h"
#include "networkXMLreader.h"
#include "platformXMLreader.h"
#include "portHeaderWriter.h"
#include "prune-analyzer.h"
#include "templateProcessing/writeCMakeLists.h"
#include "toplevelWriter.h"
#include "vertexHeaderWriter.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>

int graphSanityCheck(Graph *graph) {
	int resourceCount = graph->vertexCount();
	unsigned int *array = (unsigned int *)malloc(sizeof(unsigned int) * resourceCount);
	int subgraphCount = graph->findDisconnectedSubgraphs(array);
	free(array);
	return subgraphCount;
}

bool mappingSanityCheck(Network *app) {
	for (unsigned int i = 0; i < app->vertexCount(); i++) {
		Vertex *v = app->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			Processor *p = a->getProcessor();
			if (p != NULL) {
#ifdef DEBUG
				printf("Actor %s is bound to processor %s\n", a->getName(), p->getName());
#endif
				if ((p->getProcessorType() == PROCESSOR_GPU) && (a->getWorkGroupSize(0) == 0)) {
					printf("Warning: OpenCL number of workgroups left undefined for "
						   "actor '%s'\n",
						   a->getName());
					printf("         (is type of source file 'cl'?)\n");
				}
			}
		}
	}
	return true;
}

bool generateCMakeLists(Network *network, const char *outputFilename, bool useDalApi) {
	std::string cMakeLists;
	auto error = TemplateProcessing::writeCMakeLists(
			"templates/CMakeLists.txt.in", network, useDalApi, &cMakeLists);
	if (error.code != 0) {
		printf("Unable to create CMakeLists.txt: %s\n", error.str.c_str());
		return false;
	}

	// Write the CMakeLists.txt to a file
	FILE *outputFile = fopen(outputFilename, "w");
	if (outputFile == NULL) {
		printf("Unable to open %s for writing\n", outputFilename);
		return false;
	}
	fprintf(outputFile, "%s", cMakeLists.c_str());
	fclose(outputFile);

	return true;
}

///////////////////////////////////////////////////////////////
//                      _________________                    //
//                     |                 |                   //
//   platform.xml ---> |                 | ---> Makefile     //
//                     |                 |                   //
//                     |                 | ---> toplevel.c   //
//    network.xml ---> |      PRUNE      |                   //
//                     |                 | ---> ports.h      //
//                     |                 |                   //
//    mapping.xml ---> |                 | ---> vertices.h   //
//                     |_________________|                   //
//                                                           //
///////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
	CommandLineArguments args;
	try {
		args = CommandLineArguments::parse(argc, argv);
	}
	// Suppress errors as the parse prints it's own error
	// message.
	catch (std::invalid_argument exception) {
		return 0;
	}

	bool specificationOk = true;

	// the size of the input files are unknown thus, dynamic memory within heap
	// at run time is allocated "new" operator returns the address of the
	// allocated heap memory  later "delete" operator is called to de-allocate
	// these memories

	// the address which "new" operator returns is stored in pointer named
	// platformReader data type of new operand is provided by
	// PlatformXMLReader() function call NOTE : the empty set of parentheses
	// cannot be used to call the default constructor PlatformXMLReader() is a
	// function declaration, not an object declaration PlatformXMLReader()
	// function takes no arguments and returns the value of its class type
	PlatformXMLReader platformReader = PlatformXMLReader();

	//"->" operator accesses the member fields of an object e.g. member
	// functions of a class
	// object  the address of the first read file i.e. input-platform.xml is in
	// argv[1]  the address in argv[1] is passed to readPlatform function call
	// which is member function of calss object that its address is in
	// platformReader pointer  readPlatform function returns an address to the
	// read file which is stored in pform pointer
	Platform *pform = platformReader.readPlatform(args["input-platform.xml"]);

	// dynamic memory allocation for reading the file which its address is in
	// argv[2], same as argv[1]
	NetworkXMLReader networkReader = NetworkXMLReader();
	Network *network = networkReader.readNetwork(args["input-network.xml"]);

	// dynamic memory allocation for reading the file which its address is in
	// argv[3], same as argv[1]
	MappingXMLReader mappingReader = MappingXMLReader();
	Mapping *mapping = mappingReader.readMapping(args["input-mapping.xml"]);

	//if (argc == 9) 
	{
		printf("Starting graph analysis\n");
		Analyzer *analyzer = new Analyzer();
		analyzer->doAnalysis(network, args["output-control-function.c"]);
		delete analyzer;
	}

	mapping->connectBindings(pform, network);

	int pformSubgraphs = graphSanityCheck(pform);
	if (pformSubgraphs == 1) {
		printf("All %i platform graph resources are connected\n", pform->vertexCount());
	} else {
		printf("Resource graph forms %i disconnected subgraphs!\n", pformSubgraphs);
		specificationOk = false;
	}

	int networkSubgraphs = graphSanityCheck(network);
	if (networkSubgraphs == 1) {
		printf("All %i application graph resources are connected\n", network->vertexCount());
	} else {
		printf("Application graph forms %i disconnected subgraphs!\n", networkSubgraphs);
		specificationOk = false;
	}

	if (!mappingSanityCheck(network)) {
		specificationOk = false;
	}
	network->generateIndices();
	network->generateDeviceIndices(CL_DEVICE);
	network->discoverFifoTypes();
	network->computeFanOuts();
	network->replaceBroadcasts();

	if (specificationOk) {
		if (generateCMakeLists(network, args["output-CMakeLists.txt"], args.useDalApi))
			printf("Produced CMakeLists.txt\n");
		else
			printf("Could not produce CMakeLists.txt\n");
	}
	if (specificationOk) {
		if (args.useKrnFn == true) {
			printf(" assuming kernel name equals kernel file name (--krn-fn)\n");
		} else {
			printf(" assuming kernel name equals actor name (no --krn-fn)\n");
		}
		ToplevelWriter tw(pform, network, args.useDalApi, args.useKrnFn);
		if (tw.openFile(args["output-toplevel.cpp"])) {
			tw.writeFile();
			tw.closeFile();
			printf("Produced Toplevel\n");
		}
	}
	if (specificationOk) {
		PortHeaderWriter pw = PortHeaderWriter(network);
		if (pw.openFile(args["output-ports.h"])) {
			pw.writeFile();
			pw.closeFile();
			printf("Produced port headers\n");
		}
	}
	if (specificationOk) {
		VertexHeaderWriter vw = VertexHeaderWriter(network);
		if (vw.openFile(args["output-vertices.h"])) {
			vw.writeFile();
			vw.closeFile();
			printf("Produced vertex headers\n");
		}
	}
	printf("Cleaning up\n");
	delete pform;
	delete network;
	delete mapping;
	printf("done\n");

	return 0;
}
