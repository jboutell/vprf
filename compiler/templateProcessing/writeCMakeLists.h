#pragma once

#include "error.h"
#include <string>

class Network;

namespace TemplateProcessing {

/*! \brief Prints a template based CMakeLists.txt into a variable.
 *
 * Write a CMakeLists.txt to the variable by opening and filling a template
 * based on the KPN network.
 * \param templateFilename is the template source
 * \param application is the KPN-network application
 * \param output is filled by the function with contents of the CMakeLists.txt
 * \return Error struct that has it's .code set to zero if no error occurred
 */
Error writeCMakeLists(std::string const &templateFilename, Network *application, bool useDalApi,
					  std::string *output);

} // namespace TemplateProcessing
