#pragma once

#include <fstream>
#include <string>
#include <unordered_map>

namespace TemplateProcessing {

void apply_replacements(std::string *line,
						std::unordered_map<std::string, std::string> const &replacements);
void render(std::ifstream &templateFile,
			std::unordered_map<std::string, std::string> const &replacements, std::string *output);

} // namespace TemplateProcessing
