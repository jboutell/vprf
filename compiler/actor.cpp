#include "actor.h"
#include "hash.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

Actor::Actor()
	: processor(NULL), filename(NULL), deviceType(0), deviceIndex(0), actorType(0),
	dimensions(0), dynamicFifo(NULL), confActor(false), DPA(false), constantData(NULL)  {
	constantData = new std::vector<constant_data_t *>;
	workGroups[0] = 0;
	workGroups[1] = 0;
	workGroups[2] = 0;
	workItems[0] = 0;
	workItems[1] = 0;
	workItems[2] = 0;
}

Actor::~Actor() {
	for (unsigned int i = 0; i < constantData->size(); i++) {
		free(constantData->at(i)->data);
		delete constantData->at(i);
	}
	delete constantData;
	delete[] dynamicFifo;
	free(filename);
}

Actor::Actor(Actor &obj) : Vertex(obj) {
	processor = obj.processor;
	char *src = obj.getSource();
	if (src != NULL) {
		setSource(src);
	}
	constantData = obj.constantData;
	deviceType = obj.deviceType;
	deviceIndex = obj.deviceIndex;
	actorType = obj.actorType;
	dimensions = obj.dimensions;
	for (int i = 0; i < MAX_WG_DIM; i++) {
		workGroups[i] = obj.workGroups[i];
	}
	for (int i = 0; i < MAX_WG_DIM; i++) {
		workItems[i] = obj.workItems[i];
	}
	if (obj.getDynamic() != NULL) {
		setDynamic(obj.getDynamic());
	} else {
		dynamicFifo = NULL;
	}
	confActor = obj.confActor;
}

void Actor::setSource(char *filename) {
	int fnBodyLen = strcspn(filename, ".");
	this->filename = (char *)malloc(fnBodyLen + 1);
	strncpy(this->filename, filename, fnBodyLen);
	this->filename[fnBodyLen] = '\0';
}

char *Actor::getSource() {
	if (filename == NULL) {
		printf("*** Error: actor %s missing file name!\n", this->getName());
	}
	return filename;
}

void Actor::setConstantData(char *filename, int index) {
	constant_data_t *item = new constant_data_t;
	int fnLen = strlen(filename);
	item->data = (char *)malloc(fnLen + 1);
	strcpy(item->data, filename);
	item->data[fnLen] = '\0';
	item->index = index;
	constantData->push_back(item);
}

std::vector<constant_data_t*> *Actor::getConstantData() {
	return constantData;
}

void Actor::setProcessor(Processor *proc) {
	processor = proc;
}

Processor *Actor::getProcessor() {
	return processor;
}

void Actor::setDeviceType(int type) {
	deviceType = type;
}

int Actor::getDeviceType() {
	return deviceType;
}

void Actor::setDeviceIndex(int index) {
	deviceIndex = index;
}

int Actor::getDeviceIndex() {
	return deviceIndex;
}

void Actor::setActorType(int type) {
	actorType = type;
}

int Actor::getActorType() {
	return actorType;
}

void Actor::setWorkGroupDimensions(int dimensions) {
	this->dimensions = dimensions;
}

int Actor::getWorkGroupDimensions() {
	return dimensions;
}

void Actor::setWorkGroupSize(int wgs, int dimension) {
	workGroups[dimension] = wgs;
}

int Actor::getWorkGroupSize(int dimension) {
	return workGroups[dimension];
}

void Actor::setWorkItemSize(int wis, int dimension) {
	workItems[dimension] = wis;
}

int Actor::getWorkItemSize(int dimension) {
	return workItems[dimension];
}

void Actor::setDynamic(char *fifoName) {
	dynamicFifo = new char[strlen(fifoName) + 1];
	strcpy(dynamicFifo, fifoName);
}

char *Actor::getDynamic() {
	return dynamicFifo;
}

bool Actor::hasDynamicPorts() {
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->isDRP == true) {
			return true;
		}
	}
	return false;
}

bool Actor::checkDesignRule4() {
	bool hasInputDRP = false;
	bool hasOutputDRP = false;
	bool result;
	for (unsigned int i = 0; i < ports->size(); i++) {
		if (ports->at(i)->isDRP == true) {
			if (ports->at(i)->direction & INPUT) {
				hasInputDRP = true;
			} else if (ports->at(i)->direction == OUTPUT) {
				hasOutputDRP = true;
			}
		}
	}
	result = !(hasInputDRP & hasOutputDRP);
	if (result == false) {
		printf("actor %s\n", name);
	}
	return result;
}

void Actor::setConfActor(bool val) {
	confActor = val;
}

bool Actor::isConfActor() {
	return confActor;
}

void Actor::setDPA(bool val) {
	DPA = val;
}

bool Actor::isDPA() {
	return DPA;
}
