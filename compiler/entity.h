#pragma once

#include <vector>

#define UNDIRECTED 0
#define INPUT 1
#define OUTPUT 2
#define CONFIGURATION 3

#define MAX_WG_DIM 3

typedef struct {
	char *name;
	unsigned int hash;
	unsigned int direction;
	unsigned int index;
	unsigned int fanOut;
	bool isDRP;
} port;

const char bindingPort[] = "bindingPort";

class Entity {
  protected:
	char *name;
	int id;
	std::vector<port *> *ports;
	int inputIndex;
	int outputIndex;

	Entity();
	~Entity();
	Entity(const Entity &obj);

	char *formPortName(char *str1, char *str2);
	void copyPort(port &refPort);

  public:
	unsigned int getHash(port *p);
	unsigned int getHashByIndex(unsigned int portIndex);

	void setName(char *name);
	char *getName();

	void setId(int id);
	int getId();

	port *addBindingPort(char *vertexName);
	port *addPort(char *vertexName);
	port *addPort(char *vertexName, char *port);
	port *getPort(unsigned int index);
	port *getPortByHash(unsigned int hash);
	int getPortNum(unsigned int hash);
	unsigned int portCount();
	int getPortDirection(unsigned int hash);
	void setPortIndex(unsigned int hash, int direction);
	void incrementFanOut(unsigned int hash);
	unsigned int getFanOut(unsigned int hash);
	void printPorts();
};
