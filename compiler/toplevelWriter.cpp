#include "toplevelWriter.h"
#include <cstring>
#include <stdarg.h>
#include <stdio.h>

#define MAX_STR 512

ToplevelWriter::ToplevelWriter(Platform *platform, Network *network, bool useDalApi, bool useKrnFn) {
	this->platform = platform;
	this->network = network;
	this->useDalApi = useDalApi;
	this->useKrnFn = useKrnFn;
}

void ToplevelWriter::printResourceStrings(const char *str, int resourceType, int deviceType) {
	char tmp[MAX_STR];
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == resourceType) {
			if (resourceType == ACTOR) {
				Actor *a = static_cast<Actor *>(v);
				if (a->getDeviceType() & deviceType) {
					upperCase(tmp, a->getName());
					fprintf(file, str, tmp, a->getName());
				}
			} else if (resourceType == FIFO) {
				Fifo *f = static_cast<Fifo *>(v);
				if (f->getFifoType() == deviceType) {
					upperCase(tmp, f->getName());
					fprintf(file, str, tmp, f->getName());
				}
			} else {
				upperCase(tmp, v->getName());
				fprintf(file, str, tmp, v->getName());
			}
		}
	}
}

void ToplevelWriter::printActorSetCore(const char *str) {
	char tmp[MAX_STR];
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == GEN_DEVICE) {
				upperCase(tmp, a->getName());
				if (a->getProcessor() != NULL) {
					fprintf(file, str, tmp, a->getProcessor()->getCoreId());
				}
			}
		}
	}
}

void ToplevelWriter::printConnectionString(Vertex *v, bool isConfigurationPort) {
	char tmp1[MAX_STR], tmp2[MAX_STR], tmp3[MAX_STR];
	port *fifoInput = static_cast<Fifo *>(v)->getFifoPort(INPUT);
	port *fifoOutput = static_cast<Fifo *>(v)->getFifoPort(OUTPUT);
	Vertex *p = network->getVertexByIndex(v->getPredecessor(0));
	p->setPortIndex(
		network->getEdgeByPortHash(fifoInput->hash)->getOtherEndByHash(fifoInput->hash)->hash,
		OUTPUT);
	Vertex *s = network->getVertexByIndex(v->getSuccessor(0));
	s->setPortIndex(
		network->getEdgeByPortHash(fifoOutput->hash)->getOtherEndByHash(fifoOutput->hash)->hash,
		INPUT);
	upperCase(tmp1, p->getName());
	upperCase(tmp2, s->getName());
	upperCase(tmp3, v->getName());
	fprintf(file, "\tnetworkAddConnection(&network, indA[%s], indA[%s], indF[%s]);", tmp1, tmp2,
			tmp3);
	if (isConfigurationPort) {
		fprintf(file, " // configuration port\n");
	} else {
		fprintf(file, "\n");
	}
}

// The configuration ports of dynamic CL actors need to have index #0 for that
// actor. To ensure this, FIFOs attached to such ports are attached to actors
// first.
void ToplevelWriter::printConnectionStrings() {
	std::vector<bool> fifoPrinted(network->vertexCount());
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == FIFO) {
			fifoPrinted.at(i) = false;
			Vertex *s = network->getVertexByIndex(v->getSuccessor(0));
			if (s->getResourceType() == ACTOR) {
				Actor *a = static_cast<Actor *>(s);
				if (a->getDynamic() != NULL) {
					if (!strcmp(v->getName(), a->getDynamic())) {
						printConnectionString(v, true);
						fifoPrinted.at(i) = true;
					}
				}
			} else {
				printf("Error: (printConnectionStrings) successor %s of FIFO %s is not "
					   "an actor\n",
					   s->getName(), v->getName());
			}
		}
	}
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == FIFO) {
			if (fifoPrinted.at(i) == false) {
				printConnectionString(v, false);
			}
		}
	}
}

void ToplevelWriter::printAllocateFifos(int fifoType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == FIFO) {
			Fifo *f = static_cast<Fifo *>(v);
			if (f->getFifoType() == fifoType) {
				char tmp[MAX_STR];
				upperCase(tmp, f->getName());
				if (f->getFifoType() == FIFO_GEN) {
					fprintf(file,
							"\tfifoAllocate(&network.fifos[indF[%s]], T_FIFOGEN, %i, %i, "
							"%i, 0, "
							"NULL);\n",
							tmp, f->getTokenRate(), f->getTokenSize(), f->getInitialTokens());
				} else {
					fprintf(file,
							"\tfifoAllocate(&network.fifos[indF[%s]], T_FIFOCL, %i, %i, "
							"%i, 0, "
							"&fifoParams);\n",
							tmp, f->getTokenRate(), f->getTokenSize(), f->getInitialTokens());
				}
			}
		}
	}
}

void ToplevelWriter::printDeviceParamDecls(const char *str, int deviceType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				fprintf(file, str, a->getName());
			}
		}
	}
}

void ToplevelWriter::printActorParamDecls(const char *str) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == GEN_DEVICE) {
				fprintf(file, str, a->getSource(), a->getName());
			}
		}
	}
}

void ToplevelWriter::printSourceToDeviceParamAssignments(const char *str, int deviceType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				fprintf(file, str, a->getName(), a->getSource());
			}
		}
	}
}

void ToplevelWriter::printSetIOFunction(const char *str) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if ((a->getDeviceType() == CL_DEVICE) && a->getDynamic()) {
				char tmp[MAX_STR];
				upperCase(tmp, a->getName());
				fprintf(file, str, tmp, a->getName(), a->getName());
			}
		}
	}
}

void ToplevelWriter::printSourceAssignments(const char *str, int deviceType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				fprintf(file, str, a->getSource());
			}
		}
	}
}

void ToplevelWriter::printDeviceAssignments(const char *str, int deviceType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				fprintf(file, str, a->getDeviceIndex(), a->getSource());
			}
		}
	}
}

void ToplevelWriter::printDeviceIndices(const char *str, int deviceType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				fprintf(file, str, a->getDeviceIndex());
			}
		}
	}
}

void ToplevelWriter::printActorToDeviceParamAssignments(const char *str, int deviceType,
														int actorType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				if (a->getActorType() & actorType) {
					fprintf(file, str, a->getName(), a->getName());
				}
			}
		}
	}
}

// constantDataIndex: the programmer can explicitly set an index to constant data that is
// passed to the OpenCL kernel as the argument number. This must be done in multiple
// constant data inputs are provided to the kernel. If only one constant data input
// is specified, the index can be omitted, in which case it defaults to the next free
// argument number after the dataflow inputs / outputs (i.e. port count)
void ToplevelWriter::printConstantAssignments(int deviceType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				std::vector<constant_data_t*> *cData = a->getConstantData();
				for (unsigned int j = 0; j < cData->size(); j++) {
					constant_data_t* item = cData->at(j);
					int constantDataIndex = item->index;
					if (constantDataIndex == -1) {
						constantDataIndex = a->getPredecessorCount() + a->getSuccessorCount();
					}
					fprintf(file, "\tactorSetConstantData(context, &%sDeviceParams, %i, \"%s\");\n",
							a->getName(), constantDataIndex, item->data);
				}
			}
		}
	}
}

void ToplevelWriter::getKernelName(Actor *a, char *krnName) {
	if (useKrnFn) {
		strcpy (krnName, a->getSource());
	} else {
		strcpy (krnName, a->getName());
	}
}

void ToplevelWriter::printActorToGPUParamAssignments(const char *str, int deviceType,
													 int actorType) {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				if (a->getActorType() & actorType) {
					fprintf(file, "\tsize_t globalSize%s[3];\n", a->getName());
					fprintf(file, "\tsize_t localSize%s[3];\n", a->getName());
					char krnName[256];
					getKernelName(a, krnName);
					if (a->getWorkItemSize(0) != 0) {
						for (int d = 0; d < a->getWorkGroupDimensions(); d++) {
							fprintf(file, "\tglobalSize%s[%i] = %i;\n", a->getName(), d,
									a->getWorkGroupSize(d));
						}
						for (int d = 0; d < a->getWorkGroupDimensions(); d++) {
							fprintf(file, "\tlocalSize%s[%i] = %i;\n", a->getName(), d,
									a->getWorkItemSize(d));
						}
						char lsName[256];
						sprintf(lsName, "localSize%s", a->getName());
						fprintf(file, str, a->getName(), a->getDeviceIndex(), krnName,
								a->getWorkGroupDimensions(), a->getName(), lsName);
					} else {
						for (int d = 0; d < a->getWorkGroupDimensions(); d++) {
							fprintf(file, "\tglobalSize%s[%i] = %i;\n", a->getName(), d,
									a->getWorkGroupSize(d));
						}
						fprintf(file, str, a->getName(), a->getDeviceIndex(), krnName,
								a->getWorkGroupDimensions(), a->getName(), "NULL");
					}
				}
			}
		}
	}
}

void ToplevelWriter::printActorCalls(const char *str, int deviceType, int actorType) {
	for (unsigned int i = 0; i < network->resourceCount(ACTOR); i++) {
		Actor *a = static_cast<Actor *>(network->getVertexByOrder(i, ACTOR));
		if (a->getDeviceType() == deviceType) {
			if (a->getActorType() & actorType) {
				fprintf(file, str, a->getSource(), a->getName());
			}
		}
	}
}

void ToplevelWriter::printWithEach(const char *fmt, ...) {
	va_list args;
	va_start(args, fmt);
	vfprintf(file, fmt, args);
	va_end(args);
}

void ToplevelWriter::writeFile() {
	if (!file) {
		printf("Cannot write toplevel as file is not open\n\n");
		return;
	}
	bool usesGPU = network->countDeviceActors(CL_DEVICE) > 0;

	if (usesGPU) {
		fprintf(file, "#include \"clinit.h\"\n");
	}
	fprintf(file, "#include \"%s.h\"\n", network->getName());
	printFilenames("#include \"%s.h\"\n");
	fprintf(file, "\n");
	fprintf(file, "#include \"profile.h\"\n");
	fprintf(file, "#include \"network.h\"\n");
	fprintf(file, "#include \"vertices.h\"\n");
	fprintf(file, "\n");
	fprintf(file, "#ifdef ONNX\n");
	fprintf(file, "  struct onnx_context_t *onnx_ctx;\n");
	fprintf(file, "#endif\n");
	fprintf(file, "\n");
	fprintf(file, "int main(int argc, char** argv) {\n");
	fprintf(file, "\n");
	fprintf(file, "\tunsigned long long t1, t2;\n");
	fprintf(file, "\tunsigned int cal;\n");
	fprintf(file, "\tfloat totalTime;\n");
	if (usesGPU) {
		fprintf(file, "\tint err;\n");
		fprintf(file, "\tcl_context context;\n");
		fprintf(file, "\tcl_command_queue commands;\n");
		fprintf(file, "\tcl_device_id devices[3];\n");
		fprintf(file, "\tcl_platform_id platform[2];\n");
		fprintf(file, "\tcl_program program[%i];\n", network->countDeviceActors(CL_DEVICE));
	}
	fprintf(file, "\n");
	fprintf(file, "\tnetwork_t network;\n");
	fprintf(file, "\tint indA[%u], indF[%u];\n", network->resourceCount(ACTOR),
			network->resourceCount(FIFO));
	fprintf(file, "\n");
	fprintf(file, "#ifdef ONNX\n");
	fprintf(file, "\tonnx_ctx = onnx_context_alloc_from_file(argv[1], NULL, 0);\n");
	fprintf(file, "#endif\n");
	if (usesGPU) {
		fprintf(file, "\tcontextInit(platform, &context, devices, SELECTED_PLATFORM, "
					  "SELECTED_DEVICE);\n");
		fprintf(file, "\tcommands = clCreateCommandQueue(context, "
					  "devices[SELECTED_DEVICE], 0, &err);\n");
	}
	fprintf(file, "\tnetworkReset(&network);\n");
	fprintf(file, "\n");
	printResourceStrings("\tindA[%s] = networkAddActor(&network, \"%s\");\n", ACTOR, ANY_DEVICE);
	fprintf(file, "\n");
	printResourceStrings("\tindF[%s] = networkAddFifo(&network, \"%s\");\n", FIFO, FIFO_GEN);
	printResourceStrings("\tindF[%s] = networkAddFifo(&network, \"%s\");\n", FIFO, FIFO_CL_READ);
	printResourceStrings("\tindF[%s] = networkAddFifo(&network, \"%s\");\n", FIFO,
						 FIFO_CL_READWRITE);
	printResourceStrings("\tindF[%s] = networkAddFifo(&network, \"%s\");\n", FIFO, FIFO_CL_WRITE);
	fprintf(file, "\n");

	if (usesGPU) {
		fprintf(file, "\tcl_mem_params_t fifoParams;\n");
		fprintf(file, "\tfifoParams.context = context;\n");
	}
		printAllocateFifos(FIFO_GEN);
	if (usesGPU) {
		fprintf(file, "\tfifoParams.flags = CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR;\n");
		printAllocateFifos(FIFO_CL_READ);
		fprintf(file, "\tfifoParams.flags = CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR;\n");
		printAllocateFifos(FIFO_CL_WRITE);
		fprintf(file, "\tfifoParams.flags = CL_MEM_READ_WRITE;\n");
		printAllocateFifos(FIFO_CL_READWRITE);
		fprintf(file, "\n");
		printResourceStrings("\tfifoSetCommandQueue(&network.fifos[indF[%s]], commands);\n", FIFO,
							 FIFO_CL_READ);
		printResourceStrings("\tfifoSetCommandQueue(&network.fifos[indF[%s]], commands);\n", FIFO,
							 FIFO_CL_WRITE);
		printResourceStrings("\tfifoSetCommandQueue(&network.fifos[indF[%s]], commands);\n", FIFO,
							 FIFO_CL_READWRITE);
		fprintf(file, "\n");
	}
	printConnectionStrings();
	fprintf(file, "\n");
	if (usesGPU) {
		printDeviceParamDecls("\tgpu_actor_params_t %sDeviceParams;\n", CL_DEVICE);
		printDeviceAssignments("\tloadProgram(context, devices[SELECTED_DEVICE], &program[%i], \"src/%s.cl\", \"-I src -I "
							   "src-gen\", TARGETCL_VERSION);\n",
							   CL_DEVICE);
		printActorToGPUParamAssignments("\tactorSetGPUParams(&%sDeviceParams, commands, "
										"clCreateKernel(program[%i], \"%s\", &err), %i, globalSize%s, "
										"%s);\n",
										CL_DEVICE, ACTOR_ANY);
		fprintf(file, "\n");
		printSetIOFunction("\tactorSetIoFunction(&network.actors[indA[%s]], "
						   "&%sDeviceParams, &%sIO);\n");
		fprintf(file, "\n");
	}
	printDeviceParamDecls("\tcpu_actor_params_t %sDeviceParams;\n", GEN_DEVICE);
	if (useDalApi)
		printSourceToDeviceParamAssignments("\t%sDeviceParams.function = &%s_fire;\n", GEN_DEVICE);
	else
		printSourceToDeviceParamAssignments("\t%sDeviceParams.function = &%sFire;\n", GEN_DEVICE);
	fprintf(file, "\n");
	if (useDalApi) {
		for (unsigned int i = 0; i < network->vertexCount(); i++) {
			Vertex *v = network->getVertexByIndex(i);
			if (v->getResourceType() == ACTOR) {
				Actor *a = static_cast<Actor *>(v);
				if (a->getDeviceType() == GEN_DEVICE) {
					fprintf(file, "\t%s_State %sLocal;\n", a->getSource(), a->getName());
					fprintf(file,
							"\tDALProcess %sActorParams = "
							"makeDalProcess((void*)(&%sLocal), "
							"&%sDeviceParams.shared);\n",
							a->getName(), a->getName(), a->getName());
				}
			}
		}
	} else {
		printActorParamDecls("\t%s_data_t %sActorParams;\n");
	}
	fprintf(file, "\n");
	printActorToDeviceParamAssignments("\t%sDeviceParams.appData = &%sActorParams;\n", GEN_DEVICE,
									   ACTOR_ANY);
	fprintf(file, "\n");
	if (!useDalApi) {
		printActorToDeviceParamAssignments("\t%sActorParams.shared = &%sDeviceParams.shared;\n",
										   GEN_DEVICE, ACTOR_ANY);
		fprintf(file, "\n");
		printActorToDeviceParamAssignments("\t%sActorParams.fn = \"%s\";\n", GEN_DEVICE, ACTOR_IO);
		fprintf(file, "\n");
	}
	printResourceStrings("\tactorInitializeShared(&network.actors[indA[%s]], "
						 "&%sDeviceParams.shared);\n",
						 ACTOR, GEN_DEVICE);
	fprintf(file, "\n");
	if (useDalApi)
		printActorCalls("\t%s_init(&%sActorParams);\n", GEN_DEVICE, ACTOR_ANY);
	else
		printActorCalls("\t%sInit(&%sActorParams);\n", GEN_DEVICE, ACTOR_ANY);
	fprintf(file, "\n");
	printResourceStrings("\tactorAllocate(&network.actors[indA[%s]], T_CPU, &%sDeviceParams);\n",
						 ACTOR, GEN_DEVICE);
	printResourceStrings("\tactorAllocate(&network.actors[indA[%s]], T_GPU, &%sDeviceParams);\n",
						 ACTOR, CL_DEVICE);
	fprintf(file, "\n");
	printActorSetCore("\tactorSetCore(&network.actors[indA[%s]], %i);\n");
	printConstantAssignments(CL_DEVICE);
	fprintf(file, "\n");
	fprintf(file, "\tcal = calibrate();\n");
	fprintf(file, "\tt1 = timestamp();\n");
	fprintf(file, "\n");
	fprintf(file, "\tnetworkLaunchActors(&network);\n");
	fprintf(file, "\n");
	fprintf(file, "\tt2 = timestamp();	\n");
	fprintf(file, "\ttotalTime = ((float) (((unsigned int)(t2-t1))-cal)) / 1000000.0;\n");
	fprintf(file, "\n");
	fprintf(file, "\tprintf(\"Computation time in main loop: %%f\\n\", totalTime);\n");
	fprintf(file, "\n");
	if (useDalApi)
		printActorCalls("\t%s_finish(&%sActorParams);\n", GEN_DEVICE, ACTOR_ANY);
	else
		printActorCalls("\t%sFinish(&%sActorParams);\n", GEN_DEVICE, ACTOR_ANY);
	fprintf(file, "\n");
	printDeviceParamDecls("\tactorFreeShared(&%sDeviceParams.shared);\n", GEN_DEVICE);
	fprintf(file, "\n");
	if (usesGPU) {
		printDeviceIndices("\tclReleaseProgram(program[%i]);\n", CL_DEVICE);
		fprintf(file, "\tclReleaseCommandQueue(commands);\n");
		fprintf(file, "\tclReleaseContext(context);\n");
		fprintf(file, "\n");
	}
	fprintf(file, "\tnetworkFree(&network);\n");
	fprintf(file, "\n");
	fprintf(file, "\treturn 0;\n");
	fprintf(file, "}\n\n");
}
