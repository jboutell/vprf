#include "fifo.h"
#include <cstdio>

Fifo::Fifo() : fifoType(0), tokenSize(0), initialTokens(0), tokenRate(0) {}

Fifo::~Fifo() {}

void Fifo::setFifoType(int type) {
	fifoType = type;
}

int Fifo::getFifoType() {
	if (fifoType == 0) {
		printf("Warning: fifo %s type uninitialized\n", name);
	}
	return fifoType;
}

void Fifo::setTokenSize(int size) {
	this->tokenSize = size;
}

int Fifo::getTokenSize() {
	if (tokenSize == 0) {
		printf("Warning: fifo %s size uninitialized\n", name);
	}
	return tokenSize;
}

void Fifo::setInitialTokens(int count) {
	initialTokens = count;
}

int Fifo::getInitialTokens() {
	return initialTokens;
}

void Fifo::setTokenRate(int rate) {
	this->tokenRate = rate;
}

int Fifo::getTokenRate() {
	if (tokenRate == 0) {
		printf("Warning: fifo %s rate uninitialized\n", name);
	}
	return tokenRate;
}

port *Fifo::getFifoPort(unsigned int direction) {
	if (ports->size() != 2) {
		printf("Warning: Fifo %s has %lu ports\n", name, ports->size());
		return NULL;
	}
	if (ports->at(0)->direction == direction) {
		return ports->at(0);
	}
	return ports->at(1);
}
