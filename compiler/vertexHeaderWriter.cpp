#include "vertexHeaderWriter.h"
#include <cstdio>

#define MAX_STR 512

VertexHeaderWriter::VertexHeaderWriter(Network *network) {
	this->network = network;
}

VertexHeaderWriter::~VertexHeaderWriter() {}

void VertexHeaderWriter::writeFile() {
	for (unsigned int i = 0; i < network->vertexCount(); i++) {
		Vertex *v = network->getVertexByIndex(i);
		char tmp[MAX_STR];
		upperCase(tmp, v->getName());
		fprintf(file, "#define %s %i\n", tmp, v->getIndex());
	}
}
