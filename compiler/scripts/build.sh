#!/bin/sh

##  Prune/compiler directory
SRC_DIR=$(dirname $0)/..

## Prune/compiler/bin directory
BINARY_DIR=$SRC_DIR/bin
TARGET=$BINARY_DIR/prune-compile

## Create the binary directory (Prune/compiler/bin)
mkdir -p $BINARY_DIR

## Build prune-compile in Prune/compiler/bin/prune-compile
g++ $(find $SRC_DIR -type f -iname "*.cpp" -print) -O3 -Wall -I$SRC_DIR -I$SRC_DIR/../external/include -I/usr/include/libxml2 -lxml2 -o $TARGET -std=c++11

## Verify build success and report
if [ $? -eq 0 ]
then
	echo "Generated $(readlink -f $TARGET)"
    exit 0
else
	exit 1
fi
