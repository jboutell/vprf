#pragma once

#include <args.hxx>
#include <string>
#include <unordered_map>

/**
 * Parses command line arguments.
 */
class CommandLineArguments {
  public:
	/**
	 * \brief Parses command line arguments. Returns NULL on failure.
	 */
	static CommandLineArguments parse(int argc, char **argv);
	virtual ~CommandLineArguments(){};

	/**
	 * \brief Get positional arguments by name
	 *
	 * Get positional arguments by name. The names are
	 * 1: <input-platform.xml> 2: <input-network.xml> 3: <input-mapping.xml>
	 * 4: <output-CMakeLists.txt> 5: <output-toplevel.cpp> 6: <output-ports.h>
	 * 7: <output-vertices.h>
	 */
	const char *operator[](std::string const &key);

	/**
	 * \brief Compile using the DAL-API
	 */
	bool useDalApi;
	bool useKrnFn;

  private:
	std::unordered_map<std::string, std::string> positionals;
};
