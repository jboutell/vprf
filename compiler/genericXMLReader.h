#pragma once

#include "link.h"
#include <libxml/xmlreader.h>

#define TEXTSKIP 14
#define COMMENT 8

class GenericXMLReader {
  public:
	port *storePort(Entity *entity, char *pointName, xmlTextReaderPtr reader);
	port *ipxactStorePort(Entity *entity, char *pointName, xmlTextReaderPtr reader);
	void readEndPointDetails(Link *link, xmlTextReaderPtr reader, unsigned char *pointName,
							 int num);
	void storeEndPoint(Link *link, xmlTextReaderPtr reader, int num);
};
