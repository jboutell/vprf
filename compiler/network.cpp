#include "network.h"
#include "hash.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

Network::Network(const Network &obj) {
	vertices = new std::vector<Vertex *>();
	edges = new std::vector<Edge *>();
	for (unsigned int i = 0; i < obj.vertices->size(); i++) {
		if (obj.vertices->at(i)->getResourceType() == ACTOR) {
			Actor *ov = static_cast<Actor *>(obj.vertices->at(i));
			Actor *nv = new Actor(*ov);
			vertices->push_back(nv);
		} else if (obj.vertices->at(i)->getResourceType() == FIFO) {
			Fifo *ov = static_cast<Fifo *>(obj.vertices->at(i));
			Fifo *nv = new Fifo(*ov);
			vertices->push_back(nv);
		} else {
			printf("Error: vertex resource of type %i encountered\n",
				   obj.vertices->at(i)->getResourceType());
		}
	}
	for (unsigned int i = 0; i < obj.edges->size(); i++) {
		Link *oe = static_cast<Link *>(obj.edges->at(i));
		Link *ne = new Link(*oe);
		edges->push_back(ne);
	}
}

Actor *Network::addActor(char *name) {
	Actor *item = new Actor();
	item->setName(name);
	item->addBindingPort(name); // this adds a port for binding processors to actors
	vertices->push_back(item);
	return item;
}

Fifo *Network::addFifo(char *name) {
	Fifo *item = new Fifo();
	item->setName(name);
	vertices->push_back(item);
	return item;
}

Link *Network::addConnection(char *name) {
	Link *item = new Link();
	item->setName(name);
	edges->push_back(item);
	return item;
}

int Network::getActorIndex(Vertex *x) {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == ACTOR) {
			if (strcmp(v->getName(), x->getName()) == 0) {
				return i;
			}
		}
	}
	return -1;
}

int Network::getFifoIndex(Vertex *x) {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == FIFO) {
			if (strcmp(v->getName(), x->getName()) == 0) {
				return i;
			}
		}
	}
	return -1;
}

void Network::generateDeviceIndices(int deviceType) {
	int actorIndex = 0;
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				a->setDeviceIndex(actorIndex++);
			}
		}
	}
}

int Network::countDeviceActors(int deviceType) {
	int actorIndex = 0;
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			if (a->getDeviceType() == deviceType) {
				actorIndex++;
			}
		}
	}
	return actorIndex;
}

void Network::discoverFifoTypes() {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		Vertex *v = vertices->at(i);
		if (v->getResourceType() == FIFO) {
			int predecessorDeviceType = -1;
			int successorDeviceType = -1;
			Fifo *f = static_cast<Fifo *>(v);
			if (f->getPredecessorCount() != 1) {
				printf("Error: fifo %s has %i inputs\n", f->getName(), f->getPredecessorCount());
			} else {
				Vertex *p = vertices->at(f->getPredecessor(0));
				if (p->getResourceType() == ACTOR) {
					Actor *pa = static_cast<Actor *>(p);
					predecessorDeviceType = pa->getDeviceType();
				} else {
					printf("Error: predecessor of fifo %s is not an actor (type %i)\n",
						   f->getName(), p->getResourceType());
				}
			}
			if (f->getSuccessorCount() != 1) {
				printf("Error: fifo %s has %i outputs\n", f->getName(), f->getSuccessorCount());
			} else {
				Vertex *s = vertices->at(f->getSuccessor(0));
				if (s->getResourceType() == ACTOR) {
					Actor *sa = static_cast<Actor *>(s);
					successorDeviceType = sa->getDeviceType();
				} else {
					printf("Error: successor of fifo %s is not an actor (type %i)\n", f->getName(),
						   s->getResourceType());
				}
			}
			if ((successorDeviceType == CL_DEVICE) && (predecessorDeviceType == CL_DEVICE)) {
				f->setFifoType(FIFO_CL_READWRITE);
			} else if ((successorDeviceType == CL_DEVICE) &&
					   (predecessorDeviceType == GEN_DEVICE)) {
				f->setFifoType(FIFO_CL_READ);
			} else if ((successorDeviceType == GEN_DEVICE) &&
					   (predecessorDeviceType == CL_DEVICE)) {
				f->setFifoType(FIFO_CL_WRITE);
			} else {
				f->setFifoType(FIFO_GEN);
			}
#ifdef DEBUG
			printf("%s: set fifo type to %i\n", f->getName(), f->getFifoType());
#endif
		}
	}
}

void Network::handleConfigurationPorts() {
	for (unsigned int i = 0; i < vertices->size(); i++) {
		if (vertices->at(i)->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(vertices->at(i));
			if (a->getDynamic() != NULL) {
				char *dynamicPort = formPortName(a->getName(), a->getDynamic());
				unsigned int hash = Hash::hashCharArray(dynamicPort, strlen(dynamicPort));
				port *p = a->getPortByHash(hash);
				if (p != NULL) {
					p->direction = CONFIGURATION;
					char *fifoName =
						getVertex(getEdgeByPortHash(hash)->getOtherEndByHash(hash)->hash)
							->getName();
					free(a->getDynamic());
					a->setDynamic(fifoName);
				} else {
					printf("Error: actor %s has no configuration port of name %s", a->getName(),
						   a->getDynamic());
				}
				free(dynamicPort);
			}
		}
	}
}

#define STR_MAX 256

void Network::printActorConnections(Actor *a) {
	for (unsigned int k = 0; k < a->getSuccessorCount(); k++) {
		Vertex *v = vertices->at(a->getSuccessor(k));
		if (v->getResourceType() == FIFO) {
			Fifo *f = static_cast<Fifo *>(v);
			port *fifoInput = f->getFifoPort(INPUT);
			Edge *e = getEdgeByPortHash(fifoInput->hash);
			printf("Successor %i: fifo %s input port %s connects to edge %s that has "
				   "%i ports\n",
				   k, v->getName(), fifoInput->name, e->getName(),
				   e->portCount()); // getOtherEndByHash(fifoInput->hash)->name);
			for (unsigned int m = 0; m < e->portCount(); m++) {
				if (e->getPort(m)->direction == UNDIRECTED) {
					printf("  undirected %s\n", e->getPort(m)->name);
				} else if (e->getPort(m)->direction == INPUT) {
					printf("  input %s\n", e->getPort(m)->name);
				} else if (e->getPort(m)->direction == OUTPUT) {
					printf("  output %s\n", e->getPort(m)->name);
				}
			}
		}
	}
}

port *Network::reconnectEdge(Edge *e, port *newPort, unsigned int direction) {
	for (unsigned int m = 0; m < e->portCount(); m++) {
		if (e->getPort(m)->direction == direction) {
			// printf("renamed port %s of edge %s ", e->getPort(m)->name, e->getName());
			char *newName = (char *)realloc(e->getPort(m)->name, strlen(newPort->name) + 1);
			if (newName != NULL) {
				e->getPort(m)->name = newName;
				strcpy(e->getPort(m)->name, newPort->name);
				e->getPort(m)->name[strlen(newPort->name)] = '\0';
				e->getPort(m)->hash =
					Hash::hashCharArray(e->getPort(m)->name, strlen(e->getPort(m)->name));
				// printf("as %s (hash %u)\n", newPort->name, e->getPort(m)->hash);
			} else {
				printf("Network::reconnectEdge -- error with realloc()\n");
			}
		}
	}
	// FIXME: what should this function return?
	return newPort;
}

void Network::replaceBroadcast(Actor *a, port *p) {
	char baseName[STR_MAX];
	port **pNewPort;

	pNewPort = (port **)malloc(sizeof(port *) * p->fanOut);

	int pos = strlen(p->name);

	if (pos > STR_MAX - 5) {
		printf("Warning: overlong port name %s -- may cause problems\n", p->name);
	}

	char *newName = (char *)realloc(p->name, strlen(p->name) + 3);

	if (newName != NULL) {
		p->name = newName;
		p->name[pos] = '_';
		p->name[pos + 1] = 65;
		p->name[pos + 2] = '\0';
	} else {
		printf("Network::computeFanOuts -- error with realloc()\n");
	}
	printf("      new name is %s (%u)\n", p->name, p->hash);

	strcpy(baseName, p->name);

	for (unsigned int k = 1; k < p->fanOut; k++) {
		int len = strlen(baseName);
		baseName[len - 1] = 65 + k;
		pNewPort[k] = a->addPort(baseName);
		printf("      added port %s (%u)\n", pNewPort[k]->name, pNewPort[k]->hash);
		pNewPort[k]->direction = OUTPUT;
		pNewPort[k]->fanOut = 1;
	}

	p->fanOut = 1;
	p->hash = Hash::hashCharArray(p->name, strlen(p->name));
	pNewPort[0] = p;

	// Note: limit of k used to be a->getSuccessorCount(), but was
	// replaced due to problems with graphs that have cycles
	for (unsigned int k = 0; k < a->getSuccessorCount(); k++) {
		Vertex *v = vertices->at(a->getSuccessor(k));
		if (v->getResourceType() == FIFO) {
			Fifo *f = static_cast<Fifo *>(v);
			port *fifoInput = f->getFifoPort(INPUT);
			Edge *e = getEdgeByPortHash(fifoInput->hash);
			reconnectEdge(e, pNewPort[k], INPUT);
		}
	}

	free(pNewPort);
}

void Network::computeFanOuts() {
	for (unsigned int i = 0; i < vertexCount(); i++) {
		Vertex *v = getVertexByIndex(i);
		if (v->getResourceType() == FIFO) {
			Fifo *f = static_cast<Fifo *>(v);
			port *fifoInput = f->getFifoPort(INPUT);
			unsigned int hash =
				getEdgeByPortHash(fifoInput->hash)->getOtherEndByHash(fifoInput->hash)->hash;
			getVertexByIndex(v->getPredecessor(0))->incrementFanOut(hash);
		}
	}
}

void Network::replaceBroadcasts() {
	for (unsigned int i = 0; i < vertexCount(); i++) {
		Vertex *v = getVertexByIndex(i);
		if (v->getResourceType() == ACTOR) {
			Actor *a = static_cast<Actor *>(v);
			for (unsigned int j = 0; j < a->portCount(); j++) {
				if (a->getPort(j)->direction == OUTPUT) {
					if (a->getPort(j)->fanOut == 0) {
						printf("Warning: output port %s has fanOut 0\n", a->getPort(j)->name);
					} else if (a->getPort(j)->fanOut > 1) {
						printf("Info: port %s (%u) has fanout %i\n", a->getPort(j)->name,
								a->getPort(j)->hash,
								a->getPort(j)->fanOut);
						// printActorConnections(a);
						replaceBroadcast(a, a->getPort(j));
						// printActorConnections(a);
					}
				}
			}
		}
	}
}
