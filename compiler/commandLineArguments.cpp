#include "commandLineArguments.h"
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;

const int POSITIONAL_COUNT = 8;
const string positional_names[] = {
	"input-platform.xml",  "input-network.xml", "input-mapping.xml", "output-CMakeLists.txt",
	"output-toplevel.cpp", "output-ports.h",	"output-vertices.h", "output-control-function.c"};

CommandLineArguments CommandLineArguments::parse(int argc, char **argv) {

	args::ArgumentParser parser("Compile PRUNE-applications.", "");
	args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});

	// Setup the positional arguments
	args::Positional<string> positional_args[POSITIONAL_COUNT] = {
		args::Positional<string>(parser, positional_names[0], "The input platform XML-file"),
		args::Positional<string>(parser, positional_names[1], "The input network XML-file"),
		args::Positional<string>(parser, positional_names[2], "The input mapping XML-file"),
		args::Positional<string>(parser, positional_names[3], "The output CMakeLists.txt"),
		args::Positional<string>(parser, positional_names[4], "The output toplevel C++-file"),
		args::Positional<string>(parser, positional_names[5], "The output ports header"),
		args::Positional<string>(parser, positional_names[6], "The output vertices header"),
		args::Positional<string>(parser, positional_names[7], "The output control function")};

	args::Flag use_dal_api(parser, "dal-api",
						   "Compile application using the "
						   "Distributed Application Layer (DAL) API",
						   {"dal-api"});

	args::Flag use_krn_fn(parser, "krn-fn",
						   "OpenCL kernel equal file name "
						   "(default: kernel name equals actor name)",
						   {"krn-fn"});
	// Do parse
	try {
		parser.ParseCLI(argc, argv);
	} catch (args::Help) {
		cout << parser;
		throw invalid_argument("user requested help");
	} catch (args::ParseError e) {
		cerr << e.what() << endl;
		cerr << parser;
		throw invalid_argument("parse-error");
	}

	CommandLineArguments clas = CommandLineArguments();
	clas.useDalApi = use_dal_api ? true : false;
	clas.useKrnFn = use_krn_fn ? true : false;
	clas.positionals = unordered_map<string, string>();
	// This will error out if some of the positional arguments is not in place
	for (int i = 0; i != POSITIONAL_COUNT; ++i) {
		if (!positional_args[i]) {
			cout << parser;
			throw invalid_argument("not all " + to_string(POSITIONAL_COUNT) +
								   " positional parameters were present");
		}
		clas.positionals.insert(
			pair<string, string>(positional_names[i], args::get(positional_args[i])));
	}
	return clas;
}

const char *CommandLineArguments::operator[](string const &key) {
	// Look through positional parameters
	auto fd = positionals.find(key);
	if (fd != positionals.end())
		return fd->second.c_str();
	return NULL;
}
